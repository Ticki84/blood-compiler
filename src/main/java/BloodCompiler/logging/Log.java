package BloodCompiler.logging;

import BloodCompiler.ast.nodes.Node;
import BloodCompiler.symbolTable.SymbolTable;
import org.apache.commons.lang3.StringUtils;

public final class Log {
    private static boolean debug = false;
    private static int fatal = 0;

    private Log() {}

    public static void initialize(boolean debug) {
        Log.debug = debug;
        Log.fatal = 0;
    }

    public static void fatalSemantic(Node node, String message) {
        System.err.printf("[%d:%d] Fatal semantic error: %s\n", node.getSourceLine(), node.getSourceCharPos(), message);
        fatal += 1;
    }

    public static void fatal(String message) {
        System.err.printf("Fatal error: %s\n", message);
        fatal += 1;
    }

    public static void debugSemantic(Node node, String message) {
        if (debug)
            System.err.printf("[%d:%d] Debug: %s\n", node.getSourceLine(), node.getSourceCharPos(), message);
    }

    public static void debug(String message) {
        if (debug)
            System.err.println("Debug: " + message);
    }

    public static boolean success() {
        if (fatal > 0) {
            System.err.printf("\nCompilation has failed with %s fatal errors\n", fatal);
        }
        else {
            System.err.print("\nCompilation successful\n");
        }
        return fatal == 0;
    }

    public static void printSymbolTable(SymbolTable root) {
        System.err.printf("\n%s\n", root.toString());
        for (SymbolTable st : root.getChildren()) {
            printSymbolTable(st);
        }
    }


    /* some generic nomenclature functions */
    public static String prettyMethod(String className, String methodName) {
        return String.format("%s::%s", className, methodName);
    }

    public static String prettyAttribute(String className, String attributeName) {
        return String.format("%s.%s", className, attributeName);
    }


    /* some generic semantic functions */
    public static void fatalWrongType(Node node, String variableName, String variableType, String expectedType) {
        fatalSemantic(node, String.format(
                "Expected '%s' of '%s' type to be of '%s' type", variableName, variableType, expectedType)
        );
    }

    public static void fatalMissing(Node node, String what, String expectedType) {
        fatalSemantic(node, String.format(
                "Missing %s of '%s' type", what, expectedType)
        );
    }

    public static void fatalTooMany(Node node, String what) {
        fatalSemantic(node, String.format(
                "Too many %s", what)
        );
    }

    public static void fatalRedefinition(Node node, String ofWhat, String name) {
        fatalSemantic(node, String.format(
                "Redefinition of existing %s '%s'", ofWhat, name)
        );
    }

    public static void fatalUndefined(Node node, String what, String name) {
        fatalSemantic(node, String.format(
                "Undefined %s '%s'", what, name)
        );
    }

    public static void fatalNullDivision(Node node) {
        fatalSemantic(node, "Division by 0");
    }

    public static void fatalStatic(Node node, String what, String name, boolean expected) {
        fatalSemantic(node, String.format(
                "%s '%s' should be '%s'", StringUtils.capitalize(what), name, expected ? "static" : "non-static")
        );
    }

    public static void fatalUnexpected(Node node, String what, String name, String where) {
        if (name != null)
            fatalSemantic(node, String.format(
                    "Unexpected %s '%s' in '%s'", StringUtils.capitalize(what), name, where)
            );
        else
            fatalSemantic(node, String.format(
                    "Unexpected %s in %s", StringUtils.capitalize(what), where)
            );
    }
}
