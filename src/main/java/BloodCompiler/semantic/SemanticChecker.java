package BloodCompiler.semantic;

import BloodCompiler.ast.ASTVisitor;
import BloodCompiler.ast.nodes.*;
import BloodCompiler.logging.Log;
import BloodCompiler.symbolTable.TableManager;
import BloodCompiler.symbolTable.records.AttributeRecord;
import BloodCompiler.symbolTable.records.ClassRecord;
import BloodCompiler.symbolTable.records.MethodRecord;
import BloodCompiler.symbolTable.records.VariableRecord;
import BloodCompiler.symbolTable.types.IntegerType;
import BloodCompiler.symbolTable.types.Type;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class SemanticChecker extends ASTVisitor {

    public SemanticChecker(TableManager tableManager) {
        super(tableManager);
    }

    void checkNodesTypes(String what, String plWhat, List<Node> nodes, List<Type> types) {
        if (nodes.size() < types.size()) {
            Log.fatalMissing(nodes.get(nodes.size()-1), what, types.get(nodes.size()).getName());
        }
        else {
            if (nodes.size() > types.size()) {
                Log.fatalTooMany(nodes.get(nodes.size()-1), plWhat);
            }
            for (int i=0; i<types.size(); i++) {
                Type nodeType = getExpressionType(nodes.get(i), true);
                if (nodeType != null) {
                    if (!isChildClass(nodes.get(i), nodeType, types.get(i))) {
                        Log.fatalWrongType(nodes.get(i), nodes.get(i).getText(), nodeType.getName(), types.get(i).getName());
                    }
                }
            }
        }
    }

    private boolean isChildClass(ClassRecord parent, ClassRecord child) {
        if (parent.getId().equals(child.getId())) {
            return true;
        } else if (child.getExtendedClass() != null) {
            ClassRecord extendedClass = tableManager.getClass(child.getExtendedClass());
            return isChildClass(parent, extendedClass);
        } else {
            return false;
        }
    }

    private boolean isChildClass(Node node, String parent, String child) {
        ClassRecord parentClass = tableManager.getClass(parent);
        ClassRecord childClass = tableManager.getClass(child);
        if (parentClass == null) {
            Log.fatalUndefined(node, "class", parent);
            return false;
        }
        else if (childClass == null) {
            Log.fatalUndefined(node, "class", child);
            return false;
        }
        return isChildClass(parentClass, childClass);
    }

    private boolean isChildClass(Node node, Type parent, Type child) {
        return isChildClass(node, parent.getName(), child.getName());
    }

    private Type getMessageType(ClassRecord targetClass, Node messageNode, boolean log) {
        if (messageNode instanceof Message) {
            Message message = (Message) messageNode;
            message.setBaseClass(targetClass.getId());

            AttributeRecord variable = tableManager.getClassAttribute(targetClass, message.getName());
            if (variable == null) {
                if (log) {
                    Log.fatalUndefined(messageNode, "attribute", Log.prettyAttribute(targetClass.getId(), message.getName()));
                }
                return null;
            }
            else {
                if (!(isAttributeVisible(currentClass, variable.getId())))
                    if (log)
                        Log.fatalSemantic(messageNode, "Unauthorized access to " + Log.prettyAttribute(targetClass.getId(), message.getName()));
                if (currentMethod != null && currentMethod.isStatic()){
                    if (!variable.isStatic()){
                        Log.fatalStatic(messageNode, "attribute", variable.getId(), true);
                    }
                }
            }

            Type targetType = variable.getType();

            /* the message ends with this variable */
            if (message.getMessage() == null) return targetType;

            /* the variable is used in the message */
            ClassRecord newClass = tableManager.getClass(targetType.getName());
            return getMessageType(newClass, message.getMessage(), log);
        } else if (messageNode instanceof MessageCall) {
            MessageCall call = (MessageCall) messageNode;
            call.setBaseClass(targetClass.getId());
            MethodRecord method = tableManager.getClassMethod(targetClass, call.getCalledMethod());
            if (method == null) {
                if (log) {
                    Log.fatalUndefined(messageNode, "method", Log.prettyMethod(targetClass.getId(), call.getCalledMethod()));
                }
                return null;
            }
            checkNodesTypes("parameter", "parameters", ((Args)call.getArgs()).getArgsList(), method.getParameterRecords().stream().map(VariableRecord::getType).collect(Collectors.toList()));
            Type returnType = method.getReturnType();

            /* the message ends with this method call */
            if (call.getMessage() == null) return returnType;

            /* the method's result is used in the message */
            ClassRecord newClass = tableManager.getClass(returnType.getName());
            return getMessageType(newClass, call.getMessage(), log);
        }

        return null;
    }

    private Type getMessageBaseClassType(MessageBaseClass messageBase, boolean log) {
        ClassRecord baseClass = tableManager.getClass(messageBase.getClassName());
        if (baseClass == null) {
            if (log) {
                Log.fatalUndefined(messageBase, "class", messageBase.getClassName());
            }
            return null;
        }

        return getMessageType(baseClass, messageBase.getMessage(), log);
    }

    private Type getExpressionType(Node expression, boolean log) {
        if (expression instanceof IntegerNode) {
            return tableManager.getType("Integer");
        } else if (expression instanceof StringNode) {
            return tableManager.getType("String");
        } else if (expression instanceof OPBinary) {
            OPBinary operation = ((OPBinary) expression);
            if (operation instanceof OPCat)
                return tableManager.getType("String");
            else
                return tableManager.getType("Integer");
        } else if (expression instanceof OPUnary) {
            return tableManager.getType("Integer");
        } else if (expression instanceof MessageBaseClass) {
            MessageBaseClass messageBase = (MessageBaseClass) expression;
            return getMessageBaseClassType(messageBase, log);
        } else if (expression instanceof MessageBaseExpr) {
            MessageBaseExpr messageExpr = (MessageBaseExpr) expression;
            Type exprType = getExpressionType(messageExpr.getBase(), log);
            if (exprType == null) return null;
            ClassRecord newClass = tableManager.getClass(exprType.getName());
            return getMessageType(newClass, messageExpr.getMessage(), log);
        } else if (expression instanceof ThisNode) {
            if (currentMethod != null && currentMethod.isStatic())
                if (log)
                    Log.fatalSemantic(expression, "Use of this in static method");
            return tableManager.getType(getCurrentClassName());
        } else if (expression instanceof SuperNode) {
            ClassRecord superClass = tableManager.getClass(currentClass.getExtendedClass());
            if (superClass == null) {
                if (log) {
                    Log.fatalSemantic(expression, String.format(
                            "Cannot use super keyword on class '%s' that doesn't extend another class", getCurrentClassName()
                            )
                    );
                }
                return null;
            }
            return tableManager.getType(superClass.getId());
        } else if (expression instanceof Identifier) {
            Identifier identifier = (Identifier) expression;
            VariableRecord record = tableManager.getVariable(identifier.getName());
            if (record == null) {
                if (log) {
                    Log.fatalUndefined(expression, "identifier", identifier.getName());
                }
                return null;
            }
            return record.getType();
        } else if (expression instanceof ResultNode) {
            if (currentMethod == null) {
                if (log) {
                    Log.fatalUnexpected(expression, "result", null, "non-method context");
                }
                return null;
            }
            return currentMethod.getReturnType();
        } else if (expression instanceof Cast) {
            Cast cast = (Cast) expression;
            ClassRecord targetClass = tableManager.getClass(cast.getTargetClass());
            if (targetClass == null) {
                if (log) {
                    Log.fatalUndefined(expression, "class", cast.getTargetClass());
                }
                return null;
            }
            return tableManager.getType(targetClass.getId());
        } else if (expression instanceof Instantiate) {
            Instantiate instantiation = (Instantiate) expression;
            return tableManager.getType(instantiation.getClassType());
        }

        if (log) {
            Log.fatalUndefined(expression, "expression type", expression.getClass().getName());
        }

        return null;
    }

    private String getTypeName(Type type) {
        return type != null ? type.getName() : "NULL";
    }

    private boolean isCircularDependency(ClassRecord targetClass) {
        HashSet<ClassRecord> usedClasses = new HashSet<>();
        ClassRecord currentClass = targetClass;
        while (currentClass != null) {
            if (usedClasses.contains(currentClass)) {
                return true;
            }
            usedClasses.add(currentClass);
            currentClass = tableManager.getClass(currentClass.getExtendedClass());
        }
        return false;
    }

    private boolean isAttributeVisible(ClassRecord classRecord, String attribute) {
        if (classRecord == null)
            return false;
        return tableManager.getClassAttribute(classRecord, attribute) != null;
    }

    
    /* Visitors */

    @Override
    public void process(Args node) {

    }

    @Override
    public void process(Assignment node) {
        if (node.getVar() instanceof ResultNode && currentMethod != null && currentMethod.getReturnType() == null)
            Log.fatalUnexpected(node.getAssignee(), "result", null, "method with no return type");
        Type varType = getExpressionType(node.getVar(), true);
        Type assigneeType = getExpressionType(node.getAssignee(), true);
        if (varType != null && assigneeType != null) {
            if (!isChildClass(node, varType, assigneeType)) {
                Log.fatalWrongType(node, node.getAssignee().getText(), getTypeName(assigneeType), getTypeName(varType));
            }
        }
    }

    @Override
    public void process(AttrProps node) {

    }

    @Override
    public void process(Block node) {
        for (Node declaration : node.getDeclarations()) {
            if (!(declaration instanceof DefLVal)) {
                Log.fatalSemantic(declaration, String.format(
                        "Expected '%s' to be a local variable declaration of the block", declaration.getText()
                ));
            }
        }
        for (Node instruction : node.getInstructions()) {
            if (instruction instanceof DefLVal) {
                DefLVal lVal = (DefLVal) instruction;
                Log.fatalSemantic(instruction, String.format(
                        "Attempt to declare local variable '%s' outside of local variables declaration of the block",
                        lVal.getLVal().getText()
                ));
            }
        }
    }

    @Override
    public void process(BodyClass node) {

    }

    @Override
    public void process(BodyConstructor node) {

    }

    @Override
    public void process(BodyMethodExpression node) {
        Type type = getExpressionType(node.getExpression(), true);
        if (type != null) {
            if (!isChildClass(node, currentMethod.getReturnType(), type)) {
                Log.fatalWrongType(node, node.getExpression().getText(), getTypeName(type), currentMethod.getReturnType().getName());
            }
        }
    }

    @Override
    public void process(Cast node) {
        Type type = getExpressionType(node.getCastee(), true);
        if (type != null) {
            if (!isChildClass(node, node.getTargetClass(), type.getName())) {
                Log.fatalSemantic(node, String.format(
                        "Impossible cast of '%s' of '%s' type into '%s' type",
                        node.getCastee().getText(), type.getName(), node.getTargetClass()
                ));
            }
        }
    }

    @Override
    public void process(ClassDef node) {
        if (node.getSuperClass() != null && tableManager.getClass(node.getSuperClass()) == null)
            Log.fatalUndefined(node, "class", node.getSuperClass());

        if (node.getSuperClass() != null && (node.getSuperClass().equals("Integer") || node.getSuperClass().equals("String")))
            Log.fatalSemantic(node, String.format("Attempt to extend builtin class %s", node.getSuperClass()));

        if (isCircularDependency(tableManager.getClass(node.getClassName())))
            Log.fatalSemantic(node, String.format("Circular dependency on class '%s'", node.getClassName()));

        /* CONSTRUCTOR ARGS MATCHING */
        for (Node constructorNode : ((BodyClass) node.getBodyClass()).getConstructors()) {
            DefConstructor constructor = (DefConstructor) constructorNode;
            if (node.getParametersClass().size() != constructor.getArgs().size()) {
                Log.fatalSemantic(constructor, String.format(
                        "Class and constructor arguments size mismatch for class '%s'", node.getClassName()
                ));
            }
            else {
                for (int i = 0; i < node.getParametersClass().size(); i++) {
                    ParamClassBase classParam = node.getParametersClass().get(i);
                    ParamClassBase constrParam = (ParamConstructor) constructor.getArgs().get(i);

                    if (!classParam.equals(constrParam)) {
                        Log.fatalSemantic(node, String.format(
                                "Class argument '%s' and constructor argument '%s' don't match for class '%s'",
                                classParam.getName(), constrParam.getName(), node.getClassName()
                        ));
                    }
                }
            }
        }
    }

    @Override
    public void process(DefAttribute node) {
        if (tableManager.getClass(node.getType()) == null) {
            Log.fatalUndefined(node, "type", node.getType());
        }
    }

    @Override
    public void process(DefConstructor node) {
        /* check the matching of class and constructor */
        if (!currentClass.getId().equals(node.getName())) {
            Log.fatalSemantic(node, String.format(
                    "Expected constructor '%s' name to be '%s'",
                    node.getName(), currentClass.getId()
            ));
        }

        ClassRecord extendedClass = tableManager.getClass(currentClass.getExtendedClass());
        if (extendedClass != null) {
            if (node.getSuperClassInit() == null || !(node.getSuperClassInit() instanceof SuperClassInit)) {
                Log.fatalSemantic(node, String.format(
                        "Missing call of superclass '%s' constructor in class '%s' constructor", extendedClass.getId(), getCurrentClassName())
                );
            }
        }
    }

    @Override
    public void process(DefLVal node) {
        ClassRecord typeRecord = tableManager.getClass(node.getLValClass());
        if (typeRecord == null) {
            Log.fatalUndefined(node, "type", node.getLValClass());
        }
        else if (node.getInitializer() != null) {
            Type valType = tableManager.getType(typeRecord.getId());
            Type initType = getExpressionType(node.getInitializer(), true);
            if (initType != null) {
                if (!isChildClass(node, valType, initType)) {
                    Log.fatalWrongType(node, node.getInitializer().getText(), initType.getName(), valType.getName());
                }
            }
        }
    }

    @Override
    public void process(DefLVals node) {

    }

    @Override
    public void process(DefMethod node) {
        /* check if return type exist */
        if (node.getReturnType() != null && tableManager.getClass(node.getReturnType()) == null) {
            Log.fatalUndefined(node, "return type", node.getReturnType());
        }

        /* check for method override */
        if (node.isOverride()) {
            if (currentClass.getExtendedClass() == null)
                Log.fatalSemantic(node, String.format(
                        "Cannot use override keyword in class '%s' that doesn't extend another class.", getCurrentClassName()
                        )
                );
            else {
                MethodRecord overriddenMethod = tableManager.getSuperclassMethod(currentClass, node.getName());
                if (overriddenMethod == null) {
                    /* override of undefined method */
                    Log.fatalUndefined(node, "method", Log.prettyMethod(getCurrentClassName(), node.getName()));
                } else {
                    /* check if methods are compatible */
                    if (node.isStatic() != overriddenMethod.isStatic()) {
                        Log.fatalStatic(node, "method", Log.prettyMethod(getCurrentClassName(), node.getName()), overriddenMethod.isStatic());
                    }

                    List<Type> newTypes = signatureFromMethodParams((ParamsMethod) node.getParamsMeth());
                    List<Type> oldTypes = overriddenMethod.getParameterRecords().stream().map(VariableRecord::getType).collect(Collectors.toList());
                    if (newTypes.size() < oldTypes.size()) {
                        Log.fatalMissing(node, "parameter", oldTypes.get(newTypes.size()).getName());
                    } else if (newTypes.size() > oldTypes.size()) {
                        Log.fatalUnexpected(node, "parameter", String.valueOf(oldTypes.size()), Log.prettyMethod(currentClass.getId(), node.getName()));
                    } else {
                        for (int i = 0; i < Math.min(newTypes.size(), oldTypes.size()); i++) {
                            if (!isChildClass(node, oldTypes.get(i), newTypes.get(i))) {
                                Log.fatalWrongType(node, ((ParamsMethod) node.getParamsMeth()).getParameterMethods().get(i).getName(), newTypes.get(i).getName(), oldTypes.get(i).getName());
                            }
                        }
                    }
                }

                Log.debugSemantic(node, String.format("New overridden method: '%s'", Log.prettyMethod(getCurrentClassName(), node.getName())));
            }
        }
        if (currentMethod != null && currentMethod.getReturnType() != null && (node.getBodyMeth() instanceof Block) && !currentMethod.ResultIsUsed()){
            Log.fatalSemantic(node.getBodyMeth(), String.format("Missing return value of type %s on 'result' for methodd %s::%s", currentMethod.getReturnType(), currentClass.getId(), currentMethod.getId()));
        }
    }

    @Override
    public void process(Identifier node) {

    }

    @Override
    public void process(IfNode node) {
        Type type = getExpressionType(node.getCondition(), true);
        if (type != null && !(type instanceof IntegerType)) {
            Log.fatalWrongType(node, node.getCondition().getText(), getTypeName(type), "Integer");
        }
    }

    @Override
    public void process(Instantiate node) {
        ClassRecord targetClass = tableManager.getClass(node.getClassType());
        if (targetClass == null) {
            Log.fatalUndefined(node, "class", node.getClassType());
        }
        else if (node.getClassType().equals("Integer")|| node.getClassType().equals("String")){
            Log.fatalSemantic(node, "Cannot instantiate " + node.getClassType() + " object");
        }
        else {
            checkNodesTypes("argument", "arguments", ((Args) node.getArgs()).getArgsList(), targetClass.getParametersTypes());
        }
    }

    @Override
    public void process(IntegerNode node) {

    }

    @Override
    public void process(MessageBaseClass node) {
        getExpressionType(node, true);
    }

    @Override
    public void process(MessageBaseExpr node) {
        getExpressionType(node, true);
    }

    @Override
    public void process(MessageCall node) {

    }

    @Override
    public void process(Message node) {

    }

    @Override
    public void process(OPBinary node) {
        for (Node operand : List.of(node.getLeftOperand(), node.getRightOperand())) {
            Type opType = getExpressionType(operand, true);
            if (opType != null && !node.getAllowedType().isInstance(opType)) {
                Log.fatalSemantic(operand, String.format(
                        "Unauthorized operation '%s %s %s' ('%s %s %s')", node.getLeftOperand().getText(), node.getOperator(), node.getRightOperand().getText(), getTypeName(getExpressionType(node.getLeftOperand(), false)), node.getOperator(), getTypeName(getExpressionType(node.getRightOperand(), false)))
                );
                break;
            }
        }
    }

    @Override
    public void process(OPAdd node) {

    }

    @Override
    public void process(OPAnd node) {

    }

    @Override
    public void process(OPCat node) {

    }

    @Override
    public void process(OPDiv node) {
        if (node.getRightOperand() instanceof IntegerNode) {
            IntegerNode integer = (IntegerNode) node.getRightOperand();
            if (integer.getValue() == 0) {
                Log.fatalNullDivision(node);
            }
        }
    }

    @Override
    public void process(OPEqu node) {

    }

    @Override
    public void process(OPGeq node) {

    }

    @Override
    public void process(OPGrt node) {

    }

    @Override
    public void process(OPLeq node) {

    }

    @Override
    public void process(OPLes node) {

    }

    @Override
    public void process(OPMod node) {
        if (node.getRightOperand() instanceof IntegerNode) {
            IntegerNode integer = (IntegerNode) node.getRightOperand();
            if (integer.getValue() == 0) {
                Log.fatalNullDivision(node);
            }
        }
    }

    @Override
    public void process(OPMul node) {

    }

    @Override
    public void process(OPNeg node) {

    }

    @Override
    public void process(OPNeq node) {

    }

    @Override
    public void process(OPNot node) {

    }

    @Override
    public void process(OPOr node) {

    }

    @Override
    public void process(OPShl node) {

    }

    @Override
    public void process(OPShr node) {

    }

    @Override
    public void process(OPSub node) {

    }

    @Override
    public void process(OPUnary node) {
        Node operand = node.getOperand();
        Type opType = getExpressionType(operand, true);
        if (opType != null && !node.getAllowedType().isInstance(opType)) {
            Log.fatalSemantic(operand, String.format(
                    "Unauthorized operation '%s %s' ('%s %s')", node.getOperator(), node.getOperand().getText(), node.getOperator(), getTypeName(getExpressionType(operand, false)))
            );
        }
    }

    @Override
    public void process(OPXor node) {

    }

    @Override
    public void process(ParamClass node) {

    }

    @Override
    public void process(ParamConstructor node) {

    }

    @Override
    public void process(ParamMethod node) {
        if (tableManager.getClass(node.getType()) == null) {
            Log.fatalUndefined(node, "type", node.getType());
        }
    }

    @Override
    public void process(ParamsMethod node) {

    }

    @Override
    public void process(ResultNode node) {
        if (currentMethod != null && currentMethod.getReturnType() != null) {
            String returnType = currentMethod.getReturnType().getName();
            if (returnType != null) {
                if (tableManager.getVariable("result").getType().getName() == null || !(tableManager.getVariable("result").getType().getName().equals(returnType))) {
                    Log.fatalSemantic(node, "Variable result need to be a " + returnType + " instead of " + tableManager.getVariable("result").getType().getName());
                }
            }
        }
    }

    @Override
    public void process(ReturnNode node) {

    }

    @Override
    public void process(Root node) {

    }

    @Override
    public void process(StringNode node) {

    }

    @Override
    public void process(SuperClassInit node) {
        if (!node.getClassName().equals(currentClass.getExtendedClass())) {
            Log.fatalSemantic(node, String.format(
                    "Expected call of superclass '%s' constructor instead of '%s' in class '%s' constructor", currentClass.getExtendedClass(), node.getClassName(), getCurrentClassName())
            );
        }

        ClassRecord extendedClass = tableManager.getClass(currentClass.getExtendedClass());
        checkNodesTypes("argument", "arguments", ((Args) node.getArgs()).getArgsList(), extendedClass.getParametersTypes());
    }

    @Override
    public void process(SuperClass node) {

    }

    @Override
    public void process(SuperNode node) {

    }

    @Override
    public void process(ThisNode node) {

    }

    @Override
    public void process(While node) {
        Type type = getExpressionType(node.getExpression(), true);
        if (type != null && !(type instanceof IntegerType)) {
            Log.fatalWrongType(node, node.getExpression().getText(), getTypeName(type), "Integer");
        }
    }
}
