package BloodCompiler.symbolTable.types;

public class ClassType extends Type {
    private final static int size = 2;

    public ClassType(String name) {
        super(name, size);
    }
}
