package BloodCompiler.symbolTable.types;

public abstract class Type {
    private final String name;
    private final int size;

    public Type(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }
}
