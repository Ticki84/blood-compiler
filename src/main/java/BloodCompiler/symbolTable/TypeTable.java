package BloodCompiler.symbolTable;

import BloodCompiler.symbolTable.types.IntegerType;
import BloodCompiler.symbolTable.types.StringType;
import BloodCompiler.symbolTable.types.Type;

import java.util.HashMap;

public class TypeTable {
    private final HashMap<String, Type> types = new HashMap<>();

    public TypeTable() {
        add("Integer", new IntegerType());
        add("String", new StringType());
    }

    public void add(String name, Type type) {
        types.put(name, type);
    }

    public Type get(String name) {
        return types.get(name);
    }
}
