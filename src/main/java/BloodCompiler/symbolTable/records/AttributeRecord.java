package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;
import BloodCompiler.symbolTable.types.Type;

public class AttributeRecord extends Record {

    private final Type type;
    private final boolean isStatic;
    private Integer offset;
    private final boolean isClassVar;

    public AttributeRecord(String id, Type type, int imbrication, boolean isStatic, boolean isClassVar) {
        super(id, Kind.ATTRIBUTE, imbrication);
        this.isStatic = isStatic;
        this.type = type;
        this.isClassVar = isClassVar;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public Type getType() {
        return type;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    public boolean isClassVar() {
        return isClassVar;
    }
}
