package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;

public abstract class Record {
    private final String id;
    private final Kind kind;
    private final int imbrication;

    public Record(String id, Kind kind, int imbrication) {
        this.id = id;
        this.kind = kind;
        this.imbrication = imbrication;
    }

    public String getId() {
        return id;
    }

    public Kind getKind() {
        return kind;
    }

    public int getImbrication() {
        return imbrication;
    }
}
