package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;
import BloodCompiler.symbolTable.SymbolTable;
import BloodCompiler.symbolTable.types.Type;

import java.util.ArrayList;

public class ClassRecord extends Record {

    private final ArrayList<Type> parametersTypes;
    private SymbolTable symbolTable;
    private String extendedClass;
    private Integer instanceSize;
    private Integer descriptorSize;
    private Integer descriptorOffset;

    public ClassRecord(String id, int imbrication, ArrayList<Type> parametersTypes) {
        super(id, Kind.CLASS, imbrication);
        this.parametersTypes = parametersTypes;
    }

    public ClassRecord(String id, int imbrication, ArrayList<Type> parametersTypes, String extendedClass) {
        this(id, imbrication, parametersTypes);
        this.extendedClass = extendedClass;
    }

    public String getExtendedClass() {
        return extendedClass;
    }

    public ArrayList<Type> getParametersTypes() {
        return parametersTypes;
    }

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public void setInstanceSize(int size) {
        instanceSize = size;
    }

    public Integer getInstanceSize() {
        return instanceSize;
    }

    public void setDescriptorSize(int size) {
        descriptorSize = size;
    }

    public Integer getDescriptorSize() {
        return descriptorSize;
    }

    public void setDescriptorOffset(int offset) {
        descriptorOffset = offset;
    }

    public Integer getDescriptorOffset() {
        return descriptorOffset;
    }
}
