package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;
import BloodCompiler.symbolTable.types.Type;

public class VariableRecord extends Record {

    private final Type type;

    private Integer offset;

    public VariableRecord(String id, Type type, int imbrication) {
        super(id, Kind.LOCAL_VARIABLE, imbrication);
        this.type = type;
    }

    public VariableRecord(String id, Type type, int imbrication, Kind kind) {
        /* for parameters */
        super(id, kind, imbrication);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }
}
