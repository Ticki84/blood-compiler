package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;
import BloodCompiler.symbolTable.types.Type;

import java.util.ArrayList;
import java.util.List;

public class ConstructorRecord extends Record {
    final List<ParameterRecord> parameterRecords = new ArrayList<>();
    private Integer offset;

    public ConstructorRecord(String id, int imbrication) {
        super(id, Kind.CONSTRUCTOR, imbrication);
    }

    public void addParameterRecord(ParameterRecord parameterRecord) {
        this.parameterRecords.add(parameterRecord);
    }

    public List<ParameterRecord> getParameterRecords() {
        return parameterRecords;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }
}
