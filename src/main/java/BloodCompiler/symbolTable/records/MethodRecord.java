package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;
import BloodCompiler.symbolTable.SymbolTable;
import BloodCompiler.symbolTable.types.Type;

import java.util.ArrayList;
import java.util.List;

public class MethodRecord extends Record {
    final boolean isStatic;
    final boolean isOverride;
    final List<ParameterRecord> parameterRecords = new ArrayList<>();
    final Type returnType;
    boolean result_used;
    private Integer offset;
    private SymbolTable symbolTable;
    private boolean isBlockMethod = false;

    public MethodRecord(String id, int imbrication, boolean isStatic, boolean isOverride, Type returnType) {
        super(id, Kind.METHOD, imbrication);
        this.isStatic = isStatic;
        this.isOverride = isOverride;
        this.returnType = returnType;
        this.result_used = false;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public boolean isOverride() {
        return isOverride;
    }

    public void addParameterRecord(ParameterRecord parameterRecord) {
        this.parameterRecords.add(parameterRecord);
    }

    public List<ParameterRecord> getParameterRecords() {
        return parameterRecords;
    }

    public Type getReturnType() {
        return returnType;
    }

    public void setResult_used(boolean result_used) {
        this.result_used = result_used;
    }

    public boolean ResultIsUsed() { return this. result_used; }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public void setBlockMethod() {
        this.isBlockMethod = true;
    }

    public boolean isBlockMethod() {
        return isBlockMethod;
    }
}
