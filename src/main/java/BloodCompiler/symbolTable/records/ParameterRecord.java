package BloodCompiler.symbolTable.records;

import BloodCompiler.symbolTable.Kind;
import BloodCompiler.symbolTable.types.Type;

public class ParameterRecord extends VariableRecord {

    public ParameterRecord(String id, Type type, int imbrication) {
        super(id, type, imbrication, Kind.PARAMETER);
    }
}
