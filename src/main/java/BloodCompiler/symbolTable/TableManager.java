package BloodCompiler.symbolTable;

import BloodCompiler.symbolTable.records.*;
import BloodCompiler.symbolTable.records.Record;
import BloodCompiler.symbolTable.types.ClassType;
import BloodCompiler.symbolTable.types.Type;

import java.util.ArrayList;
import java.util.List;

public class TableManager {
    private final SymbolTable rootTable = new SymbolTable(null, SymbolTableKind.ROOT);
    private SymbolTable currentTable = rootTable;
    private final TypeTable typeTable = new TypeTable();

    public TableManager() {
        /* Add builtin classes */

        ClassRecord stringRecord = new ClassRecord("String", getCurrentImbrication(), new ArrayList<>());
        MethodRecord println = new MethodRecord("println", getCurrentImbrication(), false, false, null);
        MethodRecord print = new MethodRecord("print", getCurrentImbrication(), false, false, null);
        SymbolTable stringTable = new SymbolTable(rootTable, SymbolTableKind.METHOD);
        stringTable.addSymbol("println", println);
        stringTable.addSymbol("print", print);
        stringRecord.setSymbolTable(stringTable);
        rootTable.addSymbol("String", stringRecord);

        ClassRecord integerRecord = new ClassRecord("Integer", getCurrentImbrication(), new ArrayList<>());
        SymbolTable integerTable = new SymbolTable(rootTable, SymbolTableKind.METHOD);
        MethodRecord toString = new MethodRecord("toString", getCurrentImbrication(), false, false, getType("String"));
        integerTable.addSymbol("toString", toString);
        integerRecord.setSymbolTable(integerTable);
        rootTable.addSymbol("Integer", integerRecord);
    }

    public SymbolTable getRootTable() {
        return rootTable;
    }

    public void addSymbol(String name, Record record) {
        currentTable.addSymbol(name, record);
        if (record instanceof ClassRecord) {
            ClassType type = new ClassType(name);
            typeTable.add(name, type);
        }
    }

    public List<Record> getSymbols(String name) {
        return currentTable.findSymbols(name);
    }

    public List<Record> getSymbolsInMethod(String name) {
        return currentTable.findSymbolsInMethod(name);
    }

    public List<Record> getSymbolsInCurrentScope(String name) {
        return currentTable.findSymbolsInCurrent(name);
    }

    public ClassRecord getClass(String name) {
        if (rootTable.findSymbols(name) == null) return null;
        for (Record record : rootTable.findSymbols(name)) {
            if (record instanceof ClassRecord) {
                return (ClassRecord) record;
            }
        }
        return null;
    }

    public VariableRecord getVariable(String name) {
        if (getSymbolsInMethod(name) == null) return null;
        for (Record record : getSymbolsInMethod(name)) {
            if (record instanceof VariableRecord) {
                return (VariableRecord) record;
            }
        }
        return null;
    }

    public VariableRecord getVariableInCurrentScope(String name) {
        if (getSymbolsInCurrentScope(name) == null) return null;
        for (Record record : getSymbolsInCurrentScope(name)) {
            if (record instanceof VariableRecord) {
                return (VariableRecord) record;
            }
        }
        return null;
    }

    public MethodRecord getClassMethod(ClassRecord targetClass, String methodName) {
        MethodRecord result = getClassMethodWithoutSuperclass(targetClass, methodName);
        if (result != null) return result;

        if (targetClass.getExtendedClass() != null) return getClassMethod(getClass(targetClass.getExtendedClass()), methodName);

        return null;
    }

    public MethodRecord getClassMethodWithoutSuperclass(ClassRecord targetClass, String methodName) {
        ArrayList<Record> results = targetClass.getSymbolTable().findSymbols(methodName);
        if (results != null) {
            for (Record record : results) {
                if (record instanceof MethodRecord) {
                    return (MethodRecord) record;
                }
            }
        }

        return null;
    }

    public ConstructorRecord getClassConstructor(ClassRecord targetClass) {
        List<ConstructorRecord> results = targetClass.getSymbolTable().getInstances(ConstructorRecord.class);

        if (results.size() > 0) return results.get(0);
        return null;
    }

    public MethodRecord getSuperclassMethod(ClassRecord baseClass, String methodName) {
        return getClassMethod(getClass(baseClass.getExtendedClass()), methodName);
    }

    public AttributeRecord getClassAttribute(ClassRecord targetClass, String variableName) {
        ArrayList<Record> results = targetClass.getSymbolTable().findSymbols(variableName);
        if (results != null) {
            for (Record record : results) {
                if (record instanceof AttributeRecord) {
                    return (AttributeRecord) record;
                }
            }
        }

        if (targetClass.getExtendedClass() != null) return getClassAttribute(getClass(targetClass.getExtendedClass()), variableName);

        return null;
    }

    public ParameterRecord getMethodParam(MethodRecord targetMethod, String paramName) {
        ArrayList<Record> results = targetMethod.getSymbolTable().findSymbols(paramName);
        if (results != null) {
            for (Record record : results) {
                if (record instanceof ParameterRecord) {
                    return (ParameterRecord) record;
                }
            }
        }

        // toDo heritated methods?

        return null;
    }

    public ParameterRecord getConstructorParam(ConstructorRecord constructor, String paramName) {
        for (ParameterRecord param : constructor.getParameterRecords()) {
            if (param.getId().equals(paramName)) {
                return param;
            }
        }
        // toDo heritated methods?

        return null;
    }

    public ArrayList<ClassRecord> getClasses() {
        return rootTable.getInstances(ClassRecord.class);
    }

    public void setSymbolTable(ClassRecord targetClass) {
        targetClass.setSymbolTable(currentTable);
    }

    public void setSymbolTable(MethodRecord targetMethod) {
        targetMethod.setSymbolTable(currentTable);
    }

    public Type getType(String name) {
        return typeTable.get(name);
    }

    public Type getOrCreateType(String name) {
        Type foundType = typeTable.get(name);
        if (foundType == null) {
            foundType = new ClassType(name);
            typeTable.add(name, foundType);
        }
        return foundType;
    }

    public void enterScope(SymbolTableKind kind) {
        currentTable = currentTable.getNextChild(kind);
    }

    public void enterScope(SymbolTableKind kind, String name) {
        currentTable = currentTable.getNextChild(kind, name);
    }

    public void exitScope() {
        currentTable = currentTable.getParent();
    }

    private void computeClassOffsets(ClassRecord record) {
        int instanceOffset, descriptorOffset;

        if (record.getExtendedClass() != null) {
            ClassRecord parent = getClass(record.getExtendedClass());
            if (parent.getInstanceSize() == null) {
                /* parent offsets not yet initialized, do so now */
                computeClassOffsets(parent);
            }
            instanceOffset = parent.getInstanceSize();
            descriptorOffset = parent.getDescriptorSize();

            /* handle the offsets first for inherited overriden parent methods */
            for (MethodRecord parentMethRecord : parent.getSymbolTable().getInstances(MethodRecord.class)) {
                MethodRecord curMethRecord = getClassMethodWithoutSuperclass(record, parentMethRecord.getId());
                if (curMethRecord == null) continue;
                curMethRecord.setOffset(parentMethRecord.getOffset());
            }
        } else {
            /* leave room for the address of the class descriptor */
            instanceOffset = 4;
            /* leave room for the instance size */
            descriptorOffset = 4;
        }

        for (AttributeRecord attributeRecord : record.getSymbolTable().getInstances(AttributeRecord.class)) {
            attributeRecord.setOffset(instanceOffset);
            instanceOffset += 4;
        }

        record.setInstanceSize(instanceOffset);

        if (!record.getId().equals("String") && !record.getId().equals("Integer")) {
            /* Builtin classes have no constructor */
            record.getSymbolTable().getInstances(ConstructorRecord.class).get(0).setOffset(descriptorOffset);
            descriptorOffset += 4;
        }

        for (MethodRecord methodRecord : record.getSymbolTable().getInstances(MethodRecord.class)) {
            if (methodRecord.getOffset() != null) continue;  /* overriden methods offsets are already computed */
            methodRecord.setOffset(descriptorOffset);
            descriptorOffset += 4;
        }

        /* static attributes are stored in the class descriptor */
        for (AttributeRecord attributeRecord : record.getSymbolTable().getInstances(AttributeRecord.class)) {
            if (!attributeRecord.isStatic()) continue;
            attributeRecord.setOffset(descriptorOffset);
            descriptorOffset += 4;
        }

        record.setDescriptorSize(descriptorOffset);
    }

    public void computeOffsets() {
        for (ClassRecord record : rootTable.getInstances(ClassRecord.class)) {
            if (record.getInstanceSize() == null) {
                /* class offsets may have been initialized by a child */
                computeClassOffsets(record);
            }
        }
    }

    public int getCurrentStackSize() {
        return currentTable.getStackSize();
    }

    public int getCurrentImbrication() {
        return currentTable.getImbrication();
    }

    public void reset() {
        currentTable = rootTable;
        rootTable.resetIndex();
    }
}
