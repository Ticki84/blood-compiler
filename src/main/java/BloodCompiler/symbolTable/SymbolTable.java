package BloodCompiler.symbolTable;

import BloodCompiler.symbolTable.records.*;
import BloodCompiler.symbolTable.records.Record;
import BloodCompiler.symbolTable.types.Type;
import de.vandermeer.asciitable.AsciiTable;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class SymbolTable {
    private final SymbolTable parent;
    private final ArrayList<SymbolTable> children = new ArrayList<>();
    private int currentIndex;
    private final HashMap<String, ArrayList<Record>> table = new HashMap<>();
    private final SymbolTableKind kind;
    private final Integer imbrication;
    private final EnumMap<Kind, Integer> offsets = new EnumMap<>(Kind.class);
    private String name;
    private int stackSize = 0;

    public SymbolTable(SymbolTable parent, SymbolTableKind kind) {
        this.parent = parent;
        this.kind = kind;
        if (parent == null) imbrication = 0;
        else imbrication = parent.imbrication + 1;

        for (Kind kindVal : Kind.values()) {
            if (kindVal == Kind.LOCAL_VARIABLE)
                offsets.put(kindVal, 0);
            else
                offsets.put(kindVal, -4);
        }
    }

    public SymbolTable(SymbolTable parent, SymbolTableKind kind, String name) {
        this(parent, kind);
        this.name = name;
    }


    public void addSymbol(String name, Record record) {
        if (table.containsKey(name)) {
            table.get(name).add(record);
        }
        else {
            ArrayList<Record> arr = new ArrayList<>();
            arr.add(record);
            table.put(name, arr);
        }
        if (record instanceof ParameterRecord) {
            /* ParameterRecord inherits VariableRecord */
            ((ParameterRecord) record).setOffset(offsets.get(Kind.PARAMETER));
            offsets.put(Kind.PARAMETER, offsets.get(Kind.PARAMETER) - 4);
        } else if (record instanceof VariableRecord) {
            ((VariableRecord) record).setOffset(offsets.get(Kind.LOCAL_VARIABLE));
            offsets.put(Kind.LOCAL_VARIABLE, offsets.get(Kind.LOCAL_VARIABLE) + 4);
            stackSize += 4;
        }
    }

    public ArrayList<Record> findSymbols(String name) {
        if (table.containsKey(name))
            return table.get(name);
        if (parent == null)
            return null;
        return parent.findSymbols(name);
    }

    public ArrayList<Record> findSymbolsInCurrent(String name) {
        return table.get(name);
    }

    public ArrayList<Record> findSymbolsInMethod(String name) {
        if (kind == SymbolTableKind.CLASS || kind == SymbolTableKind.ROOT) return null;

        if (table.containsKey(name))
            return table.get(name);
        if (parent == null)
            return null;
        return parent.findSymbolsInMethod(name);
    }

    public SymbolTable getNextChild(SymbolTableKind kind) {
        SymbolTable child;
        if (currentIndex >= children.size()) {
            child = new SymbolTable(this, kind);
            children.add(child);
        } else {
            child = children.get(currentIndex);
        }
        currentIndex++;
        return child;
    }

    public SymbolTable getNextChild(SymbolTableKind kind, String name) {
        SymbolTable child;
        if (currentIndex >= children.size()) {
            child = new SymbolTable(this, kind, name);
            children.add(child);
        } else {
            child = children.get(currentIndex);
        }
        currentIndex++;
        return child;
    }


    public SymbolTable getParent() {
        return this.parent;
    }

    public ArrayList<SymbolTable> getChildren() {
        return this.children;
    }

    public void resetIndex() {
        currentIndex = 0;
        for (SymbolTable st : getChildren()) {
            st.resetIndex();
        }
    }

    public SymbolTableKind getKind() {
        return kind;
    }

    public Integer getImbrication() {
        return imbrication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public <T extends Record> ArrayList<T> getInstances(Class<T> type) {
        ArrayList<T> results = new ArrayList<>();

        for (ArrayList<Record> records : table.values()) {
            for (Record record : records) {
                if (type.isInstance(record))
                    results.add(type.cast(record));
            }
        }

        return results;
    }

    public Integer getStackSize() {
        return stackSize;
    }

    @Override
    public String toString() {
        AsciiTable at = new AsciiTable();
        at.addRule();
        at.addRow("Type", "Name", "Extends", "Return Type" ,"Properties","Arguments", "offset");
        at.addRule();

        for (List<Record> result : table.values()) {
            for (Record record : result) {
                at.addRule();
                if (record instanceof AttributeRecord) {
                    boolean staticBool = ((AttributeRecord) record).isStatic();
                    at.addRow("" + record.getKind() + " : " + ((AttributeRecord) record).getType().getName(), "" + record.getId(), "", "", staticBool ? "Static" : "", "", Objects.toString(((AttributeRecord) record).getOffset(), "null"));
                } else if (record instanceof VariableRecord) {
                    at.addRow("" + record.getKind() + " : " + ((VariableRecord) record).getType().getName(), "" + record.getId(), "", "", "", "", Objects.toString(((VariableRecord) record).getOffset(), "null"));
                }
                else if (record instanceof ClassRecord) {
                    if (((ClassRecord) record).getExtendedClass() != null) {
                        at.addRow("" + record.getKind(), "" + record.getId(), "" + ((ClassRecord) record).getExtendedClass() , "", "", "", String.format("%d/%d", ((ClassRecord) record).getInstanceSize(), ((ClassRecord) record).getDescriptorSize()));
                    }
                    else {
                        at.addRow("" + record.getKind(), "" + record.getId(), "", "", "", "", String.format("%d/%d", ((ClassRecord) record).getInstanceSize(), ((ClassRecord) record).getDescriptorSize()));
                    }
                    for (Type type : ((ClassRecord) record).getParametersTypes()) {
                        at.addRow("", "", "", "", "", ""+type.getName(), "");
                    }
                }
                else if (record instanceof ConstructorRecord) {
                    at.addRow("" + record.getKind(), "", "", "", "", "", Objects.toString(((ConstructorRecord) record).getOffset(), "null"));
                    for (ParameterRecord parameterRecord : ((ConstructorRecord) record).getParameterRecords()) {
                        at.addRow("", "", "", "", "", ""+parameterRecord.getType().getName(), "");
                    }
                }
                else if (record instanceof MethodRecord) {
                    boolean isOver = ((MethodRecord) record).isOverride();
                    boolean isStat = ((MethodRecord) record).isStatic();
                    String isSmthg = "";
                    if (isOver) {isSmthg = "Override";}
                    else if (isStat) {isSmthg = "Static";}

                    Type retType = ((MethodRecord) record).getReturnType();
                    at.addRow("" + record.getKind(), "" + record.getId(), "", retType != null ? retType.getName() : "void", isSmthg , "", Objects.toString(((MethodRecord) record).getOffset(), "null"));

                    for (ParameterRecord parameterRecord : ((MethodRecord) record).getParameterRecords()) {
                        at.addRow("", "", "", "", "", "" + parameterRecord.getType().getName(), "");
                    }
                }





            }
        }


        at.addRule();

        String printedName = this.getName() != null ? " : " + this.getName() : "";

        String rend = at.render();
        return "Type : " + this.getKind() + printedName + "  (" + this.getImbrication() + ")" + "\n" + rend;
    }
}
