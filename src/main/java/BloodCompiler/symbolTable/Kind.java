package BloodCompiler.symbolTable;

public enum Kind {
    ATTRIBUTE, LOCAL_VARIABLE, CLASS, METHOD, CONSTRUCTOR, PARAMETER
}
