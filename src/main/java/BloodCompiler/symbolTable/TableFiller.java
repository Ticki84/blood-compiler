package BloodCompiler.symbolTable;

import BloodCompiler.ast.ASTVisitor;
import BloodCompiler.ast.nodes.*;
import BloodCompiler.logging.Log;
import BloodCompiler.symbolTable.records.*;
import BloodCompiler.symbolTable.records.Record;
import BloodCompiler.symbolTable.types.Type;

import java.util.ArrayList;
import java.util.List;

public class TableFiller extends ASTVisitor {

    public TableFiller(TableManager tableManager) {
        super(tableManager);
    }

    @Override
    public void process(Args node) {

    }

    @Override
    public void process(Assignment node) {
        if (currentMethod != null && node.getVar().getText().equals("RESULT")){
            currentMethod.setResult_used(true);
        }
    }

    @Override
    public void process(AttrProps node) {

    }

    @Override
    public void process(Block node) {
        if (currentMethod != null) currentMethod.setBlockMethod();
    }

    @Override
    public void process(BodyClass node) {
        tableManager.setSymbolTable(currentClass);
        ArrayList<Node> constructors = node.getConstructors();
        if (constructors.size() == 0) {
            Log.fatalSemantic(node, String.format(
                    "Missing class '%s' constructor", currentClass.getId()
            ));
        }
        for (int i=1; i<constructors.size(); i++) {
            Log.fatalRedefinition(constructors.get(i), "constructor", currentClass.getId());
        }
    }

    @Override
    public void process(BodyConstructor node) {

    }

    @Override
    public void process(BodyMethodExpression node) {

    }

    @Override
    public void process(Cast node) {

    }

    @Override
    public void process(ClassDef node) {

        if (tableManager.getType(node.getClassName()) != null) {
            Log.fatalRedefinition(node, "class", node.getClassName());
        }
        else {
            Log.debugSemantic(node, String.format(
                    "New class: '%s'", node.getClassName()
            ));

            ArrayList<Type> signature = signatureFromClassParams(node.getParametersClass());
            if (node.getSuperClass() != null) {
                currentClass = new ClassRecord(node.getClassName(), tableManager.getCurrentImbrication(), signature, node.getSuperClass());
            } else {
                currentClass = new ClassRecord(node.getClassName(), tableManager.getCurrentImbrication(), signature);
            }
            tableManager.addSymbol(node.getClassName(), currentClass);
        }
    }

    @Override
    public void process(DefAttribute node) {
        if (tableManager.getVariableInCurrentScope(node.getName()) != null) {
            Log.fatalRedefinition(node, "variable", node.getName());
        }
        else {
            Type type = tableManager.getOrCreateType(node.getType());
            AttributeRecord attributeRecord = new AttributeRecord(node.getName(), type, tableManager.getCurrentImbrication(), node.isStatic(), false);
            tableManager.addSymbol(node.getName(), attributeRecord);

            Log.debugSemantic(node, String.format(
                    "New %s attribute: '%s' of '%s'", node.isStatic() ? "class" : "instance", node.getName(), getCurrentClassName())
            );
        }
    }

    @Override
    public void process(DefConstructor node) {
        Record record = new ConstructorRecord(node.getName(), tableManager.getCurrentImbrication());
        tableManager.addSymbol(node.getName(), record);
    }

    @Override
    public void process(DefLVal node) {
        if (node.getLVal() instanceof Identifier) {
            Identifier lVal = (Identifier)node.getLVal();
            if (tableManager.getVariableInCurrentScope(lVal.getName()) != null) {
                Log.fatalRedefinition(node, "variable", lVal.getName());
            }
            else {
                Type type = tableManager.getOrCreateType(node.getLValClass());
                Record record = new VariableRecord(lVal.getName(), type, tableManager.getCurrentImbrication());
                tableManager.addSymbol(lVal.getName(), record);

                Log.debugSemantic(node, String.format(
                        "New local variable: '%s' of '%s' type in method '%s::%s'", lVal.getName(), node.getLValClass(), getCurrentClassName(), getCurrentMethodName())
                );
            }
        }
    }

    @Override
    public void process(DefLVals node) {

    }

    @Override
    public void process(DefMethod node) {
        /* check if the method name is unique */
        if (currentMethod != null) {
            Log.fatalRedefinition(node, "method", Log.prettyMethod(getCurrentClassName(), node.getName()));
        }
        else {
            Log.debugSemantic(node, String.format("New method: '%s'", Log.prettyMethod(getCurrentClassName(), node.getName())));

            Type returnType = tableManager.getType(node.getReturnType());
            currentMethod = new MethodRecord(node.getName(), tableManager.getCurrentImbrication(), node.isStatic(), node.isOverride(), returnType);
            tableManager.addSymbol(node.getName(), currentMethod);
        }
    }

    @Override
    public void process(Identifier node) {

    }

    @Override
    public void process(IfNode node) {

    }

    @Override
    public void process(Instantiate node) {

    }

    @Override
    public void process(IntegerNode node) {

    }

    @Override
    public void process(MessageBaseClass node) {

    }

    @Override
    public void process(MessageBaseExpr node) {

    }

    @Override
    public void process(MessageCall node) {

    }

    @Override
    public void process(Message node) {

    }

    @Override
    public void process(OPBinary node) {

    }

    @Override
    public void process(OPAdd node) {

    }

    @Override
    public void process(OPAnd node) {

    }

    @Override
    public void process(OPCat node) {

    }

    @Override
    public void process(OPDiv node) {

    }

    @Override
    public void process(OPEqu node) {

    }

    @Override
    public void process(OPGeq node) {

    }

    @Override
    public void process(OPGrt node) {

    }

    @Override
    public void process(OPLeq node) {

    }

    @Override
    public void process(OPLes node) {

    }

    @Override
    public void process(OPMod node) {

    }

    @Override
    public void process(OPMul node) {

    }

    @Override
    public void process(OPNeg node) {

    }

    @Override
    public void process(OPNeq node) {

    }

    @Override
    public void process(OPNot node) {

    }

    @Override
    public void process(OPOr node) {

    }

    @Override
    public void process(OPShl node) {

    }

    @Override
    public void process(OPShr node) {

    }

    @Override
    public void process(OPSub node) {

    }

    @Override
    public void process(OPUnary node) {

    }

    @Override
    public void process(OPXor node) {

    }

    @Override
    public void process(ParamClass node) {
        if (node.isNewAttribute()) {
            /* check that name isn't already taken IN CURRENT TABLE (because you can hide attribute) */
            if (tableManager.getVariableInCurrentScope(node.getName()) != null) {
                Log.fatalRedefinition(node, "attribute", node.getName());
            } else {
                /* adding the new class parameter to symbol table */
                Type type = tableManager.getType(node.getType());
                Record paramRecord = new AttributeRecord(node.getName(), type, tableManager.getCurrentImbrication(), false, node.isNewAttribute());
                tableManager.addSymbol(node.getName(), paramRecord);
                Log.debugSemantic(node, String.format(
                        "New class parameter: '%s' of '%s' class", node.getName(), getCurrentClassName())
                );
            }
        }
    }

    @Override
    public void process(ParamConstructor node) {
        if (tableManager.getVariableInCurrentScope(node.getName()) != null) {
            Log.fatalRedefinition(node, "variable", node.getName());
        } else {
            /* adding the new constructor parameter to symbol table */
            Type type = tableManager.getType(node.getType());
            ParameterRecord paramRecord = new ParameterRecord(node.getName(), type, tableManager.getCurrentImbrication());
            tableManager.addSymbol(node.getName(), paramRecord);
            ConstructorRecord constructorRecord = tableManager.getClassConstructor(currentClass);
            constructorRecord.addParameterRecord(paramRecord);
            Log.debugSemantic(node, String.format(
                    "New constructor parameter: '%s' of '%s' class", node.getName(), getCurrentClassName())
            );
        }
    }

    @Override
    public void process(ParamMethod node) {
        if (tableManager.getVariableInCurrentScope(node.getName()) != null) {
            Log.fatalRedefinition(node, "variable", node.getName());
        } else {
            Type type = tableManager.getOrCreateType(node.getType());
            ParameterRecord record = new ParameterRecord(node.getName(), type, tableManager.getCurrentImbrication());
            tableManager.addSymbol(node.getName(), record);
            currentMethod.addParameterRecord(record);
            Log.debugSemantic(node, String.format(
                    "New method parameter: '%s' of '%s' type in '%s'", node.getName(), node.getType(), Log.prettyMethod(getCurrentClassName(), getCurrentMethodName()))
            );
        }
    }

    @Override
    public void process(ParamsMethod node) {
        /* we set the method's symbol table here because we're in the method's scope */
        tableManager.setSymbolTable(currentMethod);

        /* we define result and this here because it's in correct scope */
        if (!currentMethod.isStatic()) {
            ParameterRecord thisRecord = new ParameterRecord("this", tableManager.getType(currentClass.getId()), tableManager.getCurrentImbrication());
            tableManager.addSymbol("this", thisRecord);
        }

        if (currentMethod.getReturnType() != null) {
            VariableRecord resultRecord = new VariableRecord("result", currentMethod.getReturnType(), tableManager.getCurrentImbrication());
            tableManager.addSymbol("result", resultRecord);
        }
    }

    @Override
    public void process(ResultNode node) {

    }

    @Override
    public void process(ReturnNode node) {

    }

    @Override
    public void process(Root node) {

    }

    @Override
    public void process(StringNode node) {

    }

    @Override
    public void process(SuperClassInit node) {

    }

    @Override
    public void process(SuperClass node) {

    }

    @Override
    public void process(SuperNode node) {

    }

    @Override
    public void process(ThisNode node) {

    }

    @Override
    public void process(While node) {

    }
}
