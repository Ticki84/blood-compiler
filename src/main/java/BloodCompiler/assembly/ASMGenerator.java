package BloodCompiler.assembly;

import BloodCompiler.assembly.enums.AddrMode;
import BloodCompiler.assembly.enums.Cond;
import BloodCompiler.assembly.structs.EffectiveAddress;
import BloodCompiler.assembly.structs.Literal;
import BloodCompiler.assembly.structs.Register;
import BloodCompiler.ast.Visitor;
import BloodCompiler.ast.nodes.*;
import BloodCompiler.logging.Log;
import BloodCompiler.symbolTable.SymbolTableKind;
import BloodCompiler.symbolTable.TableManager;
import BloodCompiler.symbolTable.records.*;

import java.util.List;

import static BloodCompiler.assembly.InstructionWriter.*;
import static BloodCompiler.assembly.RegisterAllocator.*;

public class ASMGenerator implements Visitor {

    private final TableManager tableManager;
    private ClassRecord currentClass;
    private MethodRecord currentMethod;
    /* allows to prevent useless instructions when we don't need the result of an expression */
    private boolean isComputing = false;
    private int loop = 0;

    public ASMGenerator(TableManager tableManager) {
        this.tableManager = tableManager;
    }

    protected String getCurrentClassName() {
        if (currentClass != null) return currentClass.getId();
        return "<main body>";
    }

    protected String getCurrentMethodName() {
        if (currentMethod != null) return currentMethod.getId();
        return "<main body>";
    }

    private String getMethodLabel(ClassRecord classRecord, ConstructorRecord constructorRecord) {
        return String.format("%s_%s", classRecord.getId(), constructorRecord.getId());
    }

    private String getMethodLabel(ClassRecord classRecord, MethodRecord methodRecord) {
        return String.format("%s_%s", classRecord.getId(), methodRecord.getId());
    }

    private void initializeDescriptors(ClassRecord classRecord, Register descriptorBase) {
        ConstructorRecord constructorRecord = classRecord.getSymbolTable().getInstances(ConstructorRecord.class).get(0);
        Register methodAddress = getUnusedRegister();

        String label = getMethodLabel(classRecord, constructorRecord);
        ldr(methodAddress, label);
        str(methodAddress, new EffectiveAddress(descriptorBase, new Literal(constructorRecord.getOffset())));

        List<MethodRecord> methodRecords = classRecord.getSymbolTable().getInstances(MethodRecord.class);
        for (MethodRecord methodRecord : methodRecords) {
            label = getMethodLabel(classRecord, methodRecord);
            ldr(methodAddress, label);
            str(methodAddress, new EffectiveAddress(descriptorBase, new Literal(methodRecord.getOffset())));
        }

        releaseRegister(methodAddress);
    }

    public void initializeHeap() {
        Register heapAddress = getUnusedRegister();
        int currentOffset = 0;

        for (ClassRecord record : tableManager.getClasses()) {
            if (record.getId().equals("String") || record.getId().equals("Integer")) continue;
            ldr(heapAddress, "_heap");
            add(heapAddress, heapAddress, HeapOffset);
            Register instanceSize = getUnusedRegister();
            mov(instanceSize, new Literal(record.getInstanceSize()));
            comment(String.format("instanceSize(%s) := %d", record.getId(), record.getInstanceSize()));
            str(instanceSize, new EffectiveAddress(heapAddress));
            releaseRegister(instanceSize);

            initializeDescriptors(record, heapAddress);

            comment(String.format("HeapOffset += descriptorSize(%s)", record.getId()));
            add(HeapOffset, HeapOffset, new Literal(record.getDescriptorSize()));

            record.setDescriptorOffset(currentOffset);
            currentOffset += record.getDescriptorSize();
        }

        releaseRegister(heapAddress);
        b("_main");
    }

    private void computeVariableAddress(VariableRecord record) {
        int scopeOffset = tableManager.getCurrentImbrication() - record.getImbrication();
        if (scopeOffset == 0) {
            comment("R0 := @" + record.getId());
            if (record instanceof ParameterRecord)
                add(R0, BasePointer, new Literal(record.getOffset() - 8));
            else
                add(R0, BasePointer, new Literal(record.getOffset()));
        } else {
            Register currentBase = getUnusedRegister();
            Register offsetCount = getUnusedRegister();
            comment(currentBase + " := BP");
            mov(currentBase, BasePointer);
            mov(offsetCount, new Literal(scopeOffset));

            String lbl = "_identifier_loop" + loop++;

            comment(currentBase + " := BP_old");
            label(lbl);
            ldr(currentBase, new EffectiveAddress(currentBase, new Literal(-4)));

            comment("offsetCount--");
            sub(offsetCount, offsetCount, new Literal(1), null, true);
            b(lbl, Cond.NE);

            releaseRegister(offsetCount);

            comment("R0 := @" + record.getId());
            if (record instanceof ParameterRecord)
                add(R0, currentBase, new Literal(record.getOffset() - 8));
            else
                add(R0, currentBase, new Literal(record.getOffset()));

            releaseRegister(currentBase);
        }
    }

    private void computeAttributeAddress(ClassRecord classRecord, AttributeRecord attributeRecord, Register Rd) {
        if (attributeRecord.isStatic()) {
            ldr(Rd, "_heap");
            comment(String.format("%s := @%s.%s", Rd, classRecord.getId(), attributeRecord.getId()));
            add(Rd, Rd, new Literal(classRecord.getDescriptorOffset() + attributeRecord.getOffset()));
        } else {
            add(Rd, R0, new Literal(attributeRecord.getOffset()));
        }
    }

    @Override
    public void visit(Args node) {
        for (Node arg : node.getArgsList()) {
            arg.accept(this);
        }
    }

    @Override
    public void visit(Assignment node) {
        Register value = getUnusedRegister();
        boolean oldComputing = isComputing;
        isComputing = true;
        node.getAssignee().accept(this);
        isComputing = oldComputing;
        comment(value + " := result");
        mov(value, R0);

        if (node.getVar() instanceof Identifier) {
            Identifier identifier = (Identifier) node.getVar();
            VariableRecord record = tableManager.getVariable(identifier.getName());

            Register varAddr = getUnusedRegister();
            computeVariableAddress(record);
            comment(varAddr + " := @" + record.getId());
            mov(varAddr, R0);

            comment(record.getId() + " := result");
            str(value, new EffectiveAddress(varAddr));

            releaseRegister(varAddr);

            node.getVar().accept(this);
        } else if (node.getVar() instanceof ResultNode) {
            VariableRecord record = tableManager.getVariable("result");

            Register varAddr = getUnusedRegister();
            computeVariableAddress(record);
            comment(varAddr + " := @" + record.getId());
            mov(varAddr, R0);

            comment(record.getId() + " := result");
            str(value, new EffectiveAddress(varAddr));

            releaseRegister(varAddr);

            node.getVar().accept(this);
        } else if (node.getVar() instanceof MessageBaseClass) {
            node.getVar().accept(this);

            MessageBaseClass baseClass = (MessageBaseClass) node.getVar();
            ClassRecord baseRecord = tableManager.getClass(baseClass.getClassName());

            if (baseClass.getMessage() instanceof Message) {
                Message target = (Message) baseClass.getMessage();
                AttributeRecord targetRecord = tableManager.getClassAttribute(baseRecord, target.getName());

                Register attributeAddress = getUnusedRegister();
                computeAttributeAddress(baseRecord, targetRecord, attributeAddress);
                str(value, new EffectiveAddress(attributeAddress));
                releaseRegister(attributeAddress);
            }
        } else if (node.getVar() instanceof MessageBaseExpr) {
            MessageBaseExpr baseExpr = (MessageBaseExpr) node.getVar();
            if (!(baseExpr.getBase() instanceof ThisNode)) {
                throw new RuntimeException("Encountered MessageBaseExpr assignment with unhandled base type");
            }
            if (!(baseExpr.getMessage() instanceof Message)) {
                throw new RuntimeException("Encountered MessageBaseExpr assignment with unhandled message type");
            }
            Message message = (Message) baseExpr.getMessage();
            ClassRecord baseClass = tableManager.getClass(message.getBaseClass());
            AttributeRecord attributeRecord = tableManager.getClassAttribute(baseClass, message.getName());

            baseExpr.getBase().accept(this);  /* R0 now contains @this */
            Register attributeAddress = getUnusedRegister();
            computeAttributeAddress(baseClass, attributeRecord, attributeAddress);

            str(value, new EffectiveAddress(attributeAddress));
            releaseRegister(attributeAddress);
        }

        else {
            throw new RuntimeException("Unhandled assignment var");
        }

        releaseRegister(value);
    }

    @Override
    public void visit(AttrProps node) {
    }

    @Override
    public void visit(Block node) {
        if (node.getDeclarations().size() == 0 && node.getInstructions().size() == 0) return;
        Log.debugSemantic(node, "Entering scope for block");
        tableManager.enterScope(SymbolTableKind.BLOCK);

        comment("push old BP");
        push(BasePointer);
        comment("BP := SP");
        mov(BasePointer, StackPointer);
        if (tableManager.getCurrentStackSize() > 0) {
            comment("increment SP to leave room for local variables");
            add(StackPointer, StackPointer, new Literal(tableManager.getCurrentStackSize()));
        }

        for (Node declaration : node.getDeclarations()) {
            if (declaration instanceof DefLVal) {
                declaration.accept(this);
            }
        }
        for (Node instruction : node.getInstructions()) {
            if (!(instruction instanceof DefLVal)) {
                instruction.accept(this);
            }
        }

        if (currentMethod != null && currentMethod.getReturnType() != null) {
            VariableRecord variableRecord = tableManager.getVariable("result");
            if (tableManager.getCurrentImbrication() == 3) {
                label(getMethodLabel(currentClass, currentMethod)+"_end");
                computeVariableAddress(variableRecord);
                ldr(R0, new EffectiveAddress(R0));
            }
        }

        comment("SP := BP");
        mov(StackPointer, BasePointer);
        comment("restore old BP");
        pop(BasePointer);

        tableManager.exitScope();
        Log.debugSemantic(node, "Exiting scope for block");
    }

    @Override
    public void visit(BodyClass node) {
        for (Node attribute : node.getAttributes()) {
            attribute.accept(this);
        }
        for (Node constructor : node.getConstructors()) {
            constructor.accept(this);
        }
        for (Node method : node.getMethods()) {
            method.accept(this);
        }
    }

    @Override
    public void visit(BodyConstructor node) {
        for (Node instruction : node.getInstructions()) {
            instruction.accept(this);
        }
    }

    @Override
    public void visit(BodyMethodExpression node) {
        boolean oldComputing = isComputing;
        isComputing = true;
        node.getExpression().accept(this);
        isComputing = oldComputing;
    }

    @Override
    public void visit(Cast node) {
        node.getCastee().accept(this);
    }

    @Override
    public void visit(ClassDef node) {
        currentClass = tableManager.getClass(node.getClassName());
        Log.debugSemantic(node, String.format("Entering scope for class '%s' body.", getCurrentClassName()));
        tableManager.enterScope(SymbolTableKind.CLASS, node.getClassName());
        for (ParamClass param : node.getParametersClass()) {
            param.accept(this);
        }
        node.getBodyClass().accept(this);
        tableManager.exitScope();
        Log.debugSemantic(node, String.format("Exiting scope for class '%s' body.", getCurrentClassName()));
        currentClass = null;
    }

    @Override
    public void visit(DefAttribute node) {
        if (node.getExpression() != null)
            node.getExpression().accept(this);
    }

    @Override
    public void visit(DefConstructor node) {
        ConstructorRecord record = tableManager.getClassConstructor(currentClass);
        Log.debugSemantic(node, String.format("Entering scope for constructor '%s' body.", Log.prettyMethod(getCurrentClassName(), node.getName())));
        tableManager.enterScope(SymbolTableKind.CONSTRUCTOR);

        for (Node arg : node.getArgs()) {
            arg.accept(this);
        }
        if (node.getSuperClassInit() != null)
            node.getSuperClassInit().accept(this);

        label(getMethodLabel(currentClass, record));
        comment("push old BP");
        push(BasePointer);
        comment("BP := SP");
        mov(BasePointer, StackPointer);
        if (tableManager.getCurrentStackSize() > 0) {
            comment("increment SP to leave room for method variables (result)");
            add(StackPointer, StackPointer, new Literal(tableManager.getCurrentStackSize()));
        }
        node.getBloc().accept(this);
        comment("SP := BP");
        mov(StackPointer, BasePointer);
        comment("restore old BP");
        pop(BasePointer);
        tableManager.exitScope();
        Register returnAddr = getUnusedRegister();
        pop(returnAddr);
        mov(ProgramCounter, returnAddr);
        releaseRegister(returnAddr);
    }

    @Override
    public void visit(DefLVal node) {
        if (node.getInitializer() != null) {
            Identifier lVal = (Identifier) node.getLVal();
            VariableRecord record = tableManager.getVariableInCurrentScope(lVal.getName());
            int offset = record.getOffset();

            boolean oldComputing = isComputing;
            isComputing = true;
            node.getInitializer().accept(this);
            isComputing = oldComputing;
            comment(lVal.getName() + " := result");
            str(R0, new EffectiveAddress(BasePointer, new Literal(offset)));
        }

        node.getLVal().accept(this);
    }

    @Override
    public void visit(DefLVals node) {
        for (Node lVal : node.getLVarDefs()) {
            lVal.accept(this);
        }
    }

    @Override
    public void visit(DefMethod node) {
        currentMethod = tableManager.getClassMethodWithoutSuperclass(currentClass, node.getName());
        Log.debugSemantic(node, String.format("Entering scope for method '%s' body.", Log.prettyMethod(getCurrentClassName(), node.getName())));
        tableManager.enterScope(SymbolTableKind.METHOD, node.getName());
        node.getParamsMeth().accept(this);
        label(getMethodLabel(currentClass, currentMethod));
        comment("push old BP");
        push(BasePointer);
        comment("BP := SP");
        mov(BasePointer, StackPointer);
        if (tableManager.getCurrentStackSize() > 0) {
            comment("increment SP to leave room for method variables (result)");
            add(StackPointer, StackPointer, new Literal(tableManager.getCurrentStackSize()));
        }
        node.getBodyMeth().accept(this);
        comment("SP := BP");
        mov(StackPointer, BasePointer);
        comment("restore old BP");
        pop(BasePointer);
        tableManager.exitScope();
        Log.debugSemantic(node, String.format("Exiting scope for method '%s' body.", Log.prettyMethod(getCurrentClassName(), node.getName())));
        currentMethod = null;
        Register returnAddr = getUnusedRegister();
        pop(returnAddr);
        mov(ProgramCounter, returnAddr);
        releaseRegister(returnAddr);
    }

    @Override
    public void visit(Identifier node) {
        if (!isComputing) return;

        VariableRecord record = tableManager.getVariable(node.getName());
        computeVariableAddress(record);
        comment(R0 + " := " + record.getId());
        ldr(R0, new EffectiveAddress(R0));
    }

    @Override
    public void visit(IfNode node) {
        int l = loop;
        loop += 1;
        node.getCondition().accept(this);
        cmp(R0, new Literal(0));
        b("loop"+Integer.toString(l), Cond.EQ);
        if (node.getThenNode() != null)
            node.getThenNode().accept(this);
        bl("loop"+Integer.toString(l)+"_end");
        label("loop"+Integer.toString(l));
        if (node.getElseNode() != null)
            node.getElseNode().accept(this);
        label("loop"+Integer.toString(l)+"_end");
    }

    @Override
    public void visit(Instantiate node) {
        boolean oldComputing = isComputing;
        isComputing = true;

        node.getArgs().accept(this);

        ClassRecord record = tableManager.getClass(node.getClassType());

        Register descriptorAddress = getUnusedRegister();

        comment(String.format(descriptorAddress + " := @descriptor(%s)", record.getId()));
        ldr(descriptorAddress, "_heap");
        if (record.getDescriptorOffset() > 0)
            add(descriptorAddress, descriptorAddress, new Literal(record.getDescriptorOffset()));

        Register instanceAddress = getUnusedRegister();

        ldr(instanceAddress, "_heap");
        add(instanceAddress, instanceAddress, HeapOffset);
        str(descriptorAddress, new EffectiveAddress(instanceAddress));

        comment(String.format("%s := @instance(%s)", R0, record.getId()));

        comment(String.format("HeapOffset += instanceSize(%s)", record.getId()));
        add(HeapOffset, HeapOffset, new Literal(record.getInstanceSize()));

        releaseRegister(descriptorAddress);


        ConstructorRecord constructorRecord = tableManager.getClassConstructor(record);

        Register descriptorAddr = getUnusedRegister();
        ldr(descriptorAddr, "_heap");
        if (record.getDescriptorOffset() > 0)
            add(descriptorAddr, descriptorAddr, new Literal(record.getDescriptorOffset()));
        Register constructorAddr = getUnusedRegister();
        ldr(constructorAddr, new EffectiveAddress(descriptorAddr, new Literal(constructorRecord.getOffset())));
        releaseRegister(descriptorAddr);

        stm(StackPointer, getUsedRegisters(), AddrMode.FA, null, true);

        comment("increment SP to leave room for 'this' and method parameters");
        add(StackPointer, StackPointer, new Literal(4 * (constructorRecord.getParameterRecords().size() + 1)));

        comment("TestClass::param(this)");
        str(instanceAddress, new EffectiveAddress(StackPointer, new Literal(tableManager.getConstructorParam(constructorRecord, "this").getOffset())));
        int idx = 0;
        for (Node arg : ((Args) node.getArgs()).getArgsList()) {
            arg.accept(this);
            ParameterRecord currentRecord = constructorRecord.getParameterRecords().get(idx++ + 1);
            comment(String.format("%s::param(%s)", constructorRecord.getId(), currentRecord.getId()));
            str(R0, new EffectiveAddress(StackPointer, new Literal(currentRecord.getOffset())));

            AttributeRecord currentAttribute = tableManager.getClassAttribute(record, currentRecord.getId());
            if (currentAttribute == null || !currentAttribute.isClassVar()) continue;

            Register value = getUnusedRegister();
            Register attributeAddress = getUnusedRegister();
            mov(value, R0);
            mov(R0, instanceAddress);
            computeAttributeAddress(record, currentAttribute, attributeAddress);
            str(value, new EffectiveAddress(attributeAddress));
            releaseRegister(attributeAddress);
            releaseRegister(value);
        }

        push(ProgramCounter);


        comment(String.format("%s::%s()", record.getId(), constructorRecord.getId()));

        mov(ProgramCounter, constructorAddr);
        sub(StackPointer, StackPointer, new Literal(4 * (constructorRecord.getParameterRecords().size() + 1)));
        ldm(StackPointer, getUsedRegisters(), AddrMode.FA, null, true);
        releaseRegister(constructorAddr);

        isComputing = oldComputing;

        mov(R0, instanceAddress);
        releaseRegister(instanceAddress);
    }

    @Override
    public void visit(IntegerNode node) {
        ldr(R0, Integer.toString(node.getValue()));
    }

    @Override
    public void visit(MessageBaseClass node) {
        node.getMessage().accept(this);
    }

    @Override
    public void visit(MessageBaseExpr node) {
        boolean oldComputing = isComputing;
        isComputing = true;
        /* node will store expression result in R0 (either an Integer, String or a class instance) */
        node.getBase().accept(this);

        /* always a MessageCall */
        node.getMessage().accept(this);
        isComputing = oldComputing;
    }

    @Override
    public void visit(MessageCall node) {
        /* R0 contains the instance address for non-static method calls */

        if (node.getBaseClass().equals("String")) {
            switch (node.getCalledMethod()) {
                case "print":
                    print(R0);
                    break;
                case "println":
                    println(R0);
                    break;
            }
        } else if (node.getBaseClass().equals("Integer") && node.getCalledMethod().equals("toString")) {
            Register value = getUnusedRegister();
            mov(value, R0);
            its(R0, value);
            releaseRegister(value);
        } else {
            ClassRecord baseClass = tableManager.getClass(node.getBaseClass());
            MethodRecord calledMethod = tableManager.getClassMethod(baseClass, node.getCalledMethod());

            Register descriptorAddr = getUnusedRegister();
            ldr(descriptorAddr, "_heap");
            if (baseClass.getDescriptorOffset() > 0)
                add(descriptorAddr, descriptorAddr, new Literal(baseClass.getDescriptorOffset()));
            Register methodAddr = getUnusedRegister();
            ldr(methodAddr, new EffectiveAddress(descriptorAddr, new Literal(calledMethod.getOffset())));
            releaseRegister(descriptorAddr);

            stm(StackPointer, getUsedRegisters(), AddrMode.FA, null, true);

            if (calledMethod.isStatic()) {
                comment("increment SP to leave room for method parameters");
                add(StackPointer, StackPointer, new Literal(4 * (calledMethod.getParameterRecords().size())));
            } else {
                comment("increment SP to leave room for 'this' and method parameters");
                add(StackPointer, StackPointer, new Literal(4 * (calledMethod.getParameterRecords().size() + 1))); // this is not included in parametersRecords

                str(R0, new EffectiveAddress(StackPointer, new Literal(tableManager.getMethodParam(calledMethod, "this").getOffset())));
            }
            int idx = 0;
            for (Node arg : ((Args) node.getArgs()).getArgsList()) {
                arg.accept(this);
                comment(String.format("%s::param(%s)", calledMethod.getId(), calledMethod.getParameterRecords().get(idx).getId()));
                str(R0, new EffectiveAddress(StackPointer, new Literal(calledMethod.getParameterRecords().get(idx++).getOffset())));
            }

            push(ProgramCounter);

            if (calledMethod.isStatic()) {
                comment(String.format("%s.%s()", node.getBaseClass(), node.getCalledMethod()));
            } else {
                comment(String.format("%s::%s()", node.getBaseClass(), node.getCalledMethod()));
            }

            mov(ProgramCounter, methodAddr);
            if (calledMethod.isStatic())
                sub(StackPointer, StackPointer, new Literal(4 * (calledMethod.getParameterRecords().size())));
            else
                sub(StackPointer, StackPointer, new Literal(4 * (calledMethod.getParameterRecords().size() + 1)));
            ldm(StackPointer, getUsedRegisters(), AddrMode.FA, null, true);
            releaseRegister(methodAddr);
        }

        if (node.getMessage() != null) {
            /* always a MessageCall */
            node.getMessage().accept(this);
        }
    }

    @Override
    public void visit(Message node) {
        ClassRecord baseClass = tableManager.getClass(node.getBaseClass());

        AttributeRecord attributeRecord = tableManager.getClassAttribute(baseClass, node.getName());

        if (isComputing) {
            Register attributeAddr = getUnusedRegister();
            computeAttributeAddress(baseClass, attributeRecord, attributeAddr);
            comment(String.format("R0 := %s.%s", baseClass.getId(), attributeRecord.getId()));
            ldr(R0, new EffectiveAddress(attributeAddr));
            releaseRegister(attributeAddr);
        }

        if (node.getMessage() != null) {
            /* Always a MessageCall */
            node.getMessage().accept(this);
        }
    }

    @Override
    public void visit(OPBinary node) {
        boolean oldComputing = isComputing;
        isComputing = true;

        node.getLeftOperand().accept(this);
        Register leftRegister = getUnusedRegister();
        mov(leftRegister, R0);

        node.getRightOperand().accept(this);
        Register rightRegister = getUnusedRegister();
        mov(rightRegister, R0);

        isComputing = oldComputing;

        if (node instanceof OPAdd) {
            add(R0, leftRegister, rightRegister);
        } else if (node instanceof OPAnd) {
            and(R0, leftRegister, rightRegister);
        } else if (node instanceof OPCat) {
            concatstr(R0, leftRegister, rightRegister);
        } else if (node instanceof OPDiv) {
            cmp(rightRegister, new Literal(0));
            b("_except_div_zero", Cond.EQ);
            div(R0, leftRegister, rightRegister);
        } else if (node instanceof OPEqu) {
            comment(leftRegister + " =? " + rightRegister);
            cmp(leftRegister, rightRegister);
            mov(R0, new Literal(0));
            mov(R0, new Literal(1), Cond.EQ);
        } else if (node instanceof OPGeq) {
            cmp(leftRegister, rightRegister);
            mov(R0, new Literal(0));
            mov(R0, new Literal(1), Cond.GE);
        } else if (node instanceof OPGrt) {
            comment(leftRegister + " <? " + rightRegister);
            cmp(leftRegister, rightRegister);
            mov(R0, new Literal(0));
            mov(R0, new Literal(1), Cond.GT);
        } else if (node instanceof OPLeq) {
            cmp(leftRegister, rightRegister);
            mov(R0, new Literal(0));
            mov(R0, new Literal(1), Cond.LE);
        } else if (node instanceof OPLes) {
            cmp(leftRegister, rightRegister);
            mov(R0, new Literal(0));
            mov(R0, new Literal(1), Cond.LT);
        } else if (node instanceof OPMod) {
            mod(R0, leftRegister, rightRegister);
        } else if (node instanceof OPMul) {
            mul(R0, leftRegister, rightRegister);
        } else if (node instanceof OPNeq) {
            cmp(leftRegister, rightRegister);
            mov(R0, new Literal(0));
            mov(R0, new Literal(1), Cond.NE);
        } else if (node instanceof OPOr) {
            orr(R0, leftRegister, rightRegister);
        } else if (node instanceof OPShl) {
            lsl(R0, leftRegister, rightRegister);
        } else if (node instanceof OPShr) {
            lsr(R0, leftRegister, rightRegister);
        } else if (node instanceof OPSub) {
            sub(R0, leftRegister, rightRegister);
        } else if (node instanceof OPXor) {
            eor(R0, leftRegister, rightRegister);
        } else {
            throw new RuntimeException("Unimplemented operation node " + node);
        }

        releaseRegister(leftRegister);
        releaseRegister(rightRegister);
    }

    @Override
    public void visit(OPAdd node) {
    }

    @Override
    public void visit(OPAnd node) {
        if (node.getLeftOperand() instanceof Identifier && node.getRightOperand() instanceof Identifier) {
        }
    }

    @Override
    public void visit(OPCat node) {
    }

    @Override
    public void visit(OPDiv node) {
    }

    @Override
    public void visit(OPEqu node) {
    }

    @Override
    public void visit(OPGeq node) {
    }

    @Override
    public void visit(OPGrt node) {
    }

    @Override
    public void visit(OPLeq node) {
    }

    @Override
    public void visit(OPLes node) {
    }

    @Override
    public void visit(OPMod node) {
    }

    @Override
    public void visit(OPMul node) {
    }

    @Override
    public void visit(OPNeg node) {
    }

    @Override
    public void visit(OPNeq node) {
    }

    @Override
    public void visit(OPNot node) {
    }

    @Override
    public void visit(OPOr node) {
    }

    @Override
    public void visit(OPShl node) {
    }

    @Override
    public void visit(OPShr node) {
    }

    @Override
    public void visit(OPSub node) {
    }

    @Override
    public void visit(OPUnary node) {
        Register register = getUnusedRegister();
        boolean oldComputing = isComputing;
        isComputing = true;
        node.getOperand().accept(this);
        mov(register, R0);
        isComputing = oldComputing;

        if (node instanceof OPNeg){
            rsb(R0, register, new Literal(0));
        } else if (node instanceof OPNot) {
            mvn(R0, register);
        } else {
            throw new RuntimeException("Unimplemented operation node " + node);
        }

        releaseRegister(register);
    }

    @Override
    public void visit(OPXor node) {
    }

    @Override
    public void visit(ParamClass node) {
    }

    @Override
    public void visit(ParamConstructor node) {
    }

    @Override
    public void visit(ParamMethod node) {
    }

    @Override
    public void visit(ParamsMethod node) {
        for (Node param : node.getParameterMethods()){
            param.accept(this);
        }
    }

    @Override
    public void visit(ResultNode node) {
        VariableRecord thisRecord = tableManager.getVariable("result");
        computeVariableAddress(thisRecord);
        ldr(R0, new EffectiveAddress(R0));
    }

    @Override
    public void visit(ReturnNode node) {
        int scopeOffset = tableManager.getCurrentImbrication() - currentMethod.getImbrication()-2;
        if (scopeOffset !=0){
            Register offsetCount = getUnusedRegister();
            mov(offsetCount, new Literal(scopeOffset));

            String lbl = "_identifier_loop" + loop++;

            comment(BasePointer + " := BP_old");
            label(lbl);
            ldr(BasePointer, new EffectiveAddress(BasePointer, new Literal(-4)));

            comment("offsetCount--");
            sub(offsetCount, offsetCount, new Literal(1), null, true);
            b(lbl, Cond.NE);

            releaseRegister(offsetCount);
        }

        b(getMethodLabel(currentClass, currentMethod)+"_end");
    }

    @Override
    public void visit(Root node) {
        for (Node currentClass : node.getClasses()) {
            currentClass.accept(this);
        }
        label("_main");
        for (Node currentInstruction : node.getMainProgram()) {
            currentInstruction.accept(this);
        }
    }

    @Override
    public void visit(StringNode node) {
        String label = dcd(node.getStringContent());
        ldr(R0, label);
    }

    @Override
    public void visit(SuperClassInit node) {
        node.getArgs().accept(this);
    }

    @Override
    public void visit(SuperClass node) {
    }

    @Override
    public void visit(SuperNode node) {
    }

    @Override
    public void visit(ThisNode node) {
        VariableRecord record = tableManager.getVariable("this");
        computeVariableAddress(record);
        comment(R0 + " := this");
        ldr(R0, new EffectiveAddress(R0));
    }

    @Override
    public void visit(While node) {
        int l = loop;
        loop += 1;
        label("loop"+Integer.toString(l));
        node.getExpression().accept(this);
        cmp(R0, new Literal(0));
        b("end"+Integer.toString(l), Cond.EQ);
        node.getInstruction().accept(this);
        b("loop"+Integer.toString(l));
        label("end"+Integer.toString(l));
    }
}
