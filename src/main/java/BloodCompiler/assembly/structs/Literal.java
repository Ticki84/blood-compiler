package BloodCompiler.assembly.structs;

/**
 * #N
 */
public class Literal implements FlexibleOperator2, Offset {
    private final int N;

    public Literal(int N) {
        assert N > -256 && N < 256 ;
        this.N = N;
    }


    @Override
    public String toString() {
        return "#" + N;
    }
}
