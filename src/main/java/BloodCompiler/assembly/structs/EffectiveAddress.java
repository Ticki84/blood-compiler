package BloodCompiler.assembly.structs;

import BloodCompiler.assembly.enums.Indexed;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class EffectiveAddress {
    private final Register register;
    private final Offset offset;
    private final Indexed indexed;

    public EffectiveAddress(@NotNull Register Rb) {
        this(Rb, null, null);
    }

    public EffectiveAddress(@NotNull Register Rb, @Nullable Offset offset) {
        this(Rb, offset, null);
    }

    public EffectiveAddress(@NotNull Register Rb, @Nullable Offset offset, @Nullable Indexed indexed) {
        this.register = Rb;
        this.offset = offset;
        this.indexed = indexed;
    }

    public EffectiveAddress(@NotNull Register Rb, @NotNull Register Rx, int shift) {
        this(Rb, Rx, shift, null);
    }

    public EffectiveAddress(@NotNull Register Rb, @NotNull Register Rx, int shift, @Nullable Indexed indexed) {
        this(Rb, new ScaledRegister(Rx, shift), indexed);
        assert shift > 0 && shift < 32;
    }

    @Override
    public String toString() {
        return String.format("[%s%s]%s",
                register,
                offset != null && indexed != Indexed.Post ? ", " + offset.toString() : "",
                indexed == Indexed.Post ? ", " + offset.toString() : indexed == Indexed.Pre ? "!" : "");
    }
}
