package BloodCompiler.assembly.structs;

import org.jetbrains.annotations.NotNull;

/**
 * Rn
 */
public class Register implements Comparable<Register>, FlexibleOperator2, Offset {
    private int n = 0;

    public Register(int n) {
        assert n >= 0 && n < 16;
        this.n = n;
    }

    public int index() {
        return n;
    }

    public static Register BasePointer() {
        return new Register(12);
    }

    public static Register StackPointer() {
        return new Register(13);
    }

    public static Register LinkRegister() {
        return new Register(14);
    }

    public static Register ProgramCounter() {
        return new Register(15);
    }

    public int getNumber() {
        return n;
    }

    @Override
    public String toString() {
        switch (n) {
            case 13:
                return "SP";
            case 14:
                return "LR";
            case 15:
                return "PC";
            default:
                return "R" + n;
        }
    }

    @Override
    public int compareTo(@NotNull Register compared) {
        return Integer.compare(this.n, compared.n);
    }
}
