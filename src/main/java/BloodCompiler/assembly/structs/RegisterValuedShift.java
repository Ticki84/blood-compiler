package BloodCompiler.assembly.structs;

/**
 * Rn, ASR Rm
 */
public class RegisterValuedShift implements FlexibleOperator2 {
    private final Register n;
    private final Register m;

    public RegisterValuedShift(Register n, Register m) {
        this.n = n;
        this.m = m;
    }

    @Override
    public String toString() {
        return n.toString()+", ASR "+m.toString();
    }
}
