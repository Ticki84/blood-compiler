package BloodCompiler.assembly.structs;

public final class ASMConstants {
    private ASMConstants() {}

    public static final String mul = "mul\t\t\t\tSTMFA\tSP!, {R1,R2}\n" +
            "\t\t\t\tMOV\t\tR0, #0\n" +
            "_mul_loop\t\t\tLSRS\t\tR2, R2, #1\n" +
            "\t\t\t\tADDCS\tR0, R0, R1\n" +
            "\t\t\t\tLSL\t\tR1, R1, #1\n" +
            "\t\t\t\tTST\t\tR2, R2\n" +
            "\t\t\t\tBNE\t\t_mul_loop\n" +
            "\t\t\t\tLDMFA\tSP!, {R1,R2}\n" +
            "\t\t\t\tLDR\t\tPC, [R13, #-4]!";

    public static final String div = "div\t\tSTMFA\tSP!, {R2-R5}\n" +
            "\t\tMOV\t\tR0, #0\n" +
            "\t\tMOV\t\tR3, #0\n" +
            "\t\tCMP\t\tR1, #0\n" +
            "\t\tRSBLT\tR1, R1, #0\n" +
            "\t\tEORLT\tR3, R3, #1\n" +
            "\t\tCMP\t\tR2, #0\n" +
            "\t\tRSBLT\tR2, R2, #0\n" +
            "\t\tEORLT\tR3, R3, #1\n" +
            "\t\tMOV\t\tR4, R2\n" +
            "\t\tMOV\t\tR5, #1\n" +
            "_div_max\tLSL\t\tR4, R4, #1\n" +
            "\t\tLSL\t\tR5, R5, #1\n" +
            "\t\tCMP\t\tR4, R1\n" +
            "\t\tBLE\t\t_div_max\n" +
            "_div_loop\tLSR\t\tR4, R4, #1\n" +
            "\t\tLSR\t\tR5, R5, #1\n" +
            "\t\tCMP\t\tR4,R1\n" +
            "\t\tBGT\t\t_div_loop\n" +
            "\t\tADD\t\tR0, R0, R5\n" +
            "\t\tSUB\t\tR1, R1, R4\n" +
            "\t\tCMP\t\tR1, R2\n" +
            "\t\tBGE\t\t_div_loop\n" +
            "\t\tCMP\t\tR3, #1\n" +
            "\t\tBNE\t\t_div_exit\n" +
            "\t\tCMP\t\tR1, #0\n" +
            "\t\tADDNE\tR0, R0, #1\n" +
            "\t\tRSB\t\tR0, R0, #0\n" +
            "\t\tRSB\t\tR1, R1, #0\n" +
            "\t\tADDNE\tR1, R1, R2\n" +
            "_div_exit\tLDMFA\tSP!, {R2-R5}\n" +
            "\t\tLDR\t\tPC, [R13, #-4]!";

    public static final String print = "\t\t\t;\t\tR0 contains the address of the null-terminated string to print\n" +
            "print\t\tSTMFA\tSP!, {R0-R2}\n" +
            "\t\t\tLDR\t\tR1, =_str_out  ; address of the output buffer\n" +
            "_print_loop\tLDRB\t\tR2, [R0], #1\n" +
            "\t\t\tSTRB\t\tR2, [R1], #1\n" +
            "\t\t\tTST\t\tR2, R2\n" +
            "\t\t\tBNE\t\t_print_loop\n" +
            "\t\t\tLDMFA\tSP!, {R0-R2}\n" +
            "\t\t\tLDR\t\tPC, [R13, #-4]!\n" +
            "\t\t\t\n" +
            "\t\t\t;\t\tR0 contains the address of the null-terminated string to print\n" +
            "println\t\tSTMFA\tSP!, {R0-R2}\n" +
            "\t\t\tLDR\t\tR1, =_str_out  ; address of the output buffer\n" +
            "_println_loop\tLDRB\t\tR2, [R0], #1\n" +
            "\t\t\tSTRB\t\tR2, [R1], #1\n" +
            "\t\t\tTST\t\tR2, R2\n" +
            "\t\t\tBNE\t\t_println_loop\n" +
            "\t\t\tMOV\t\tR2, #10\n" +
            "\t\t\tSTRB\t\tR2, [R1, #-1]\n" +
            "\t\t\tMOV\t\tR2, #0\n" +
            "\t\t\tSTRB\t\tR2, [R1]\n" +
            "\t\t\tLDMFA\tSP!, {R0-R2}\n" +
            "\t\t\tLDR\t\tPC, [R13, #-4]!";

    public static final String int_to_string = "its\t\tSTMFA\tSP!, {R0-R10}\n" +
            "        MOV     R9, R2\n" +
            "\t\tMOV\t\tR3, R1\n" +
            "\t\tMOV\t\tR4, #10\n" +
            "\t\tMOV\t\tR8, #0\n" +
            "\t\tMOV\t\tR5, #0\n" +
            "\t\tMOV\t\tR6, #0\n" +
            "\t\tMOV\t\tR7, #0\n" +
            "\t\tCMP\t\tR3, #0\n" +
            "\t\tMOV\t\tR10, #0\n" +
            "\t\tRSBLT\tR3, R3, #0\n" +
            "\t\tMOVLT\tR5, #0x2D\n" +
            "\t\tADDLT\tR8, R8, #8\n" +
            "\t\tADDLT\tR10, R10, #1\n" +
            "_its_init\tCMP\t\tR3, R4\n" +
            "\t\tBLT\t\t_its_loop\n" +
            "\t\tMOV\t\tR1, R4\n" +
            "\t\tMOV\t\tR2, #10\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tmul\n" +
            "\t\tMOV\t\tR4, R0\n" +
            "\t\tB\t\t_its_init\n" +
            "\n" +
            "_its_loop\n" +
            "\t\tMOV\t\tR1, R4\n" +
            "\t\tMOV\t\tR2, #10\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tdiv\n" +
            "\t\tMOV\t\tR4, R0\n" +
            "\t\tCMP\t\tR4, #0\n" +
            "\t\tBEQ\t\t_its_exit\n" +
            "\t\tMOV\t\tR2, R0\n" +
            "\t\tMOV\t\tR1, R3\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tdiv\n" +
            "\t\tMOV\t\tR1, R0\n" +
            "\t\tADD\t\tR0, R0, #0x30\n" +
            "\t\tLSL\t\tR0, R0, R8\n" +
            "\t\tADD\t\tR5, R5, R0\n" +
            "\t\tMOV\t\tR2, R4\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tmul\n" +
            "\t\tSUB\t\tR3, R3, R0\n" +
            "\t\tADD\t\tR8, R8, #8\n" +
            "\t\tADD\t\tR10, R10, #1\n" +
            "\t\tCMP\t\tR8, #32\n" +
            "\t\tMOVEQ\tR8, #0\n" +
            "\t\tBEQ\t\t_its2\n" +
            "\t\tB\t\t_its_loop\n" +
            "\n" +
            "_its2\n" +
            "\t\tMOV\t\tR1, R4\n" +
            "\t\tMOV\t\tR2, #10\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tdiv\n" +
            "\t\tMOV\t\tR4, R0\n" +
            "\t\tCMP\t\tR4, #0\n" +
            "\t\tBEQ\t\t_its_exit\n" +
            "\t\tMOV\t\tR2, R0\n" +
            "\t\tMOV\t\tR1, R3\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tdiv\n" +
            "\t\tMOV\t\tR1, R0\n" +
            "\t\tADD\t\tR0, R0, #0x30\n" +
            "\t\tLSL\t\tR0, R0, R8\n" +
            "\t\tADD\t\tR6, R6, R0\n" +
            "\t\tMOV\t\tR2, R4\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tmul\n" +
            "\t\tSUB\t\tR3, R3, R0\n" +
            "\t\tADD\t\tR8, R8, #8\n" +
            "\t\tADD\t\tR10, R10, #1\n" +
            "\t\tCMP\t\tR8, #32\n" +
            "\t\tMOVEQ\tR8, #0\n" +
            "\t\tBEQ\t\t_its3\n" +
            "\t\tB\t\t_its2\n" +
            "\n" +
            "_its3\n" +
            "\t\tMOV\t\tR1, R4\n" +
            "\t\tMOV\t\tR2, #10\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tdiv\n" +
            "\t\tMOV\t\tR4, R0\n" +
            "\t\tCMP\t\tR4, #0\n" +
            "\t\tBEQ\t\t_its_exit\n" +
            "\t\tMOV\t\tR2, R0\n" +
            "\t\tMOV\t\tR1, R3\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tdiv\n" +
            "\t\tMOV\t\tR1, R0\n" +
            "\t\tADD\t\tR0, R0, #0x30\n" +
            "\t\tLSL\t\tR0, R0, R8\n" +
            "\t\tADD\t\tR7, R7, R0\n" +
            "\t\tADD\t\tR10, R10, #1\n" +
            "\t\tMOV\t\tR2, R4\n" +
            "\t\tSTR\t\tPC, [SP], #4\n" +
            "\t\tB\t\tmul\n" +
            "\t\tSUB\t\tR3, R3, R0\n" +
            "\t\tADD\t\tR8, R8, #8\n" +
            "\t\tB\t\t_its3\n" +
            "\n" +
            "_its_exit\tSTMIA\tR9, {R5-R7}\n" +
            "\t\tLDMFA\tSP!, {R0-R10}\n" +
            "\t\tLDR\t\tPC, [SP, #-4]!";
}
