package BloodCompiler.assembly.structs;

/**
 * Rn, LSL #S
 */
public class ScaledRegister implements FlexibleOperator2, Offset {
    private final Register n;
    private final int S;

    public ScaledRegister(Register n, int S) {
        this.n = n;
        this.S = S;
    }

    @Override
    public String toString() {
        return n.toString() + ", LSL #" + S;
    }
}
