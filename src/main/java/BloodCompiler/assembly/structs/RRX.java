package BloodCompiler.assembly.structs;

/**
 * Rn, RXX
 */
public class RRX implements FlexibleOperator2 {
    private final Register n;

    public RRX(Register n) {
        this.n = n;
    }

    @Override
    public String toString() {
        return n.toString() + ", RXX";
    }
}
