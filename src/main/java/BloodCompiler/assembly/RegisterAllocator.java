package BloodCompiler.assembly;

import BloodCompiler.assembly.exceptions.RegistersExhaustedException;
import BloodCompiler.assembly.structs.Register;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RegisterAllocator {
    /* allocate R3 through R12 as general-purpose registers */
    private static final int firstAvailable = 4;
    private static final int lastAvailable = 11;
    private static final TreeSet<Register> available = Stream.iterate(firstAvailable, n -> n + 1)
            .limit(lastAvailable - firstAvailable + 1)
            .map(Register::new)
            .collect(Collectors.toCollection(TreeSet::new));
    private static final Set<Register> all = new HashSet<>(available);

    public static final Register BasePointer = Register.BasePointer();
    public static final Register StackPointer = Register.StackPointer();
    public static final Register LinkRegister = Register.LinkRegister();
    public static final Register ProgramCounter = Register.ProgramCounter();
    public static final Register R0 = new Register(0);
    public static final Register R1 = new Register(1);
    public static final Register R2 = new Register(2);
    public static final Register HeapOffset = new Register(3);

    public static Register getUnusedRegister() throws RegistersExhaustedException {
        if (available.isEmpty()) {
            throw new RegistersExhaustedException();
        }

        return available.pollFirst();
    }

    public static boolean isGeneralPurpose(Register register) {
        return register.getNumber() >= firstAvailable && register.getNumber() <= lastAvailable;
    }

    public static void releaseRegister(Register register) {
        if (!isGeneralPurpose(register)) {
            throw new IllegalArgumentException(String.format("Register %s is not a general-purpose register.", register.toString()));
        } else if (available.contains(register)) {
            throw new IllegalArgumentException(String.format("Register %s is not currently allocated.", register.toString()));
        }
        available.add(register);
    }

    public static boolean isAllocated(Register register) {
        return !(isGeneralPurpose(register) && available.contains(register));
    }

    public static Set<Register> getUsedRegisters() {
        Set<Register> allRegisters = new HashSet<>(all);
        allRegisters.removeAll(available);
        return allRegisters;
    }
}
