package BloodCompiler.assembly.enums;

public enum Cond {
    EQ, NE, CS, HS, CC, LO, MI, PL, VS, VC, HI, LS, GE, LT, GT, LE
}
