package BloodCompiler.assembly.enums;

public enum AddrMode {
    FD, FA, ED, EA, IA, ID, DA, DB
}
