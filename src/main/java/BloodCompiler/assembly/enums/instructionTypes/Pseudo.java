package BloodCompiler.assembly.enums.instructionTypes;

public enum Pseudo {
    DCD, DCB, FILL, ADR, EQU
}
