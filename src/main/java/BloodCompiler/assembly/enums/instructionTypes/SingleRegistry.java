package BloodCompiler.assembly.enums.instructionTypes;

public enum SingleRegistry {
    LDR, LDRB, STR, STRB
}
