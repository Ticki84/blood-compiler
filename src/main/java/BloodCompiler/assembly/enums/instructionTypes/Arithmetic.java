package BloodCompiler.assembly.enums.instructionTypes;

public enum Arithmetic {
    ADD, SUB, RSB, ADC, SBC, RSC
}
