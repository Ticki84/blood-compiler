package BloodCompiler.assembly.enums.instructionTypes;

public enum Rotate {
    ROR, RRX
}
