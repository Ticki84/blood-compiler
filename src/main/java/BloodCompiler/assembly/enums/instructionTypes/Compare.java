package BloodCompiler.assembly.enums.instructionTypes;

public enum Compare {
    CMP, CMN, TST, TEQ
}
