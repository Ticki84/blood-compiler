package BloodCompiler.assembly.enums.instructionTypes;

public enum Shift {
    LSR, ASR, LSL
}
