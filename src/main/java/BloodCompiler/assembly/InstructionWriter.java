package BloodCompiler.assembly;

import BloodCompiler.assembly.enums.AddrMode;
import BloodCompiler.assembly.enums.Cond;
import BloodCompiler.assembly.enums.Indexed;
import BloodCompiler.assembly.structs.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static BloodCompiler.assembly.RegisterAllocator.*;

public final class InstructionWriter {
    private static PrintWriter writer;

    // Options
    private static final int pad = 15;
    private static final boolean canPadOverflow = false;
    public static final int outputBufferLength = 0x1000;
    private static final int heapSize = 0x100000;

    // Default values
    private static final boolean s = false;
    private static final boolean updateAddress = false;

    // Global values
    private static String label = StringUtils.rightPad("", pad, " ");
    private static String comment = "";
    private static int strCounter = 0;

    public static void initialize(PrintWriter writer) {
        InstructionWriter.writer = writer;

        label("_str_out");
        fill(outputBufferLength);

        label("_heap");
        fill(heapSize);

        label("_start");
        b("_init");
        label("_exit");
        end();

        writer.println(ASMConstants.print);
        writer.println(ASMConstants.mul);
        writer.println(ASMConstants.div);
        writer.println(ASMConstants.int_to_string);
        concatsubroutine();

        label("_except_div_zero");
        String lbl = dcd("RuntimeException: division by zero.");
        ldr(R0, lbl);
        println(R0);
        b("_exit");

        label("_init");
        mov(RegisterAllocator.BasePointer, RegisterAllocator.StackPointer);
        mov(RegisterAllocator.HeapOffset, new Literal(0));
    }

    public static void finish() {
        b("_exit");
        writer.close();
    }

    // Arithmetic
    public static void add(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("ADD", Rd, Rn, op2, cond, s));
    }
    public static void add(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("ADD", Rd, Rn, op2, cond, s));
    }
    public static void add(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("ADD", Rd, Rn, op2, null, s));
    }
    public static void sub(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("SUB", Rd, Rn, op2, cond, s));
    }
    public static void sub(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("SUB", Rd, Rn, op2, cond, s));
    }
    public static void sub(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("SUB", Rd, Rn, op2, null, s));
    }
    public static void rsb(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("RSB", Rd, Rn, op2, cond, s));
    }
    public static void rsb(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("RSB", Rd, Rn, op2, cond, s));
    }
    public static void rsb(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("RSB", Rd, Rn, op2, null, s));
    }
    public static void adc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("ADC", Rd, Rn, op2, cond, s));
    }
    public static void adc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("ADC", Rd, Rn, op2, cond, s));
    }
    public static void adc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("ADC", Rd, Rn, op2, null, s));
    }
    public static void sbc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("SBC", Rd, Rn, op2, cond, s));
    }
    public static void sbc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("SBC", Rd, Rn, op2, cond, s));
    }
    public static void sbc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("SBC", Rd, Rn, op2, null, s));
    }
    public static void rsc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("RSC", Rd, Rn, op2, cond, s));
    }
    public static void rsc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("RSC", Rd, Rn, op2, cond, s));
    }
    public static void rsc(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("RSC", Rd, Rn, op2, null, s));
    }

    // Logical
    public static void and(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("AND", Rd, Rn, op2, cond, s));
    }
    public static void and(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("AND", Rd, Rn, op2, cond, s));
    }
    public static void and(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("AND", Rd, Rn, op2, null, s));
    }
    public static void orr(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("ORR", Rd, Rn, op2, cond, s));
    }
    public static void orr(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("ORR", Rd, Rn, op2, cond, s));
    }
    public static void orr(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("ORR", Rd, Rn, op2, null, s));
    }
    public static void eor(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("EOR", Rd, Rn, op2, cond, s));
    }
    public static void eor(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("EOR", Rd, Rn, op2, cond, s));
    }
    public static void eor(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("EOR", Rd, Rn, op2, null, s));
    }
    public static void bic(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("BIC", Rd, Rn, op2, cond, s));
    }
    public static void bic(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("BIC", Rd, Rn, op2, cond, s));
    }
    public static void bic(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("BIC", Rd, Rn, op2, null, s));
    }

    // Move
    public static void mov(@NotNull Register Rd, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("MOV", Rd, null, op2, cond, s));
    }
    public static void mov(@NotNull Register Rd, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("MOV", Rd, null, op2, cond, s));
    }
    public static void mov(@NotNull Register Rd, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("MOV", Rd, null, op2, null, s));
    }
    public static void mvn(@NotNull Register Rd, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("MVN", Rd, null, op2, cond, s));
    }
    public static void mvn(@NotNull Register Rd, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("MVN", Rd, null, op2, cond, s));
    }
    public static void mvn(@NotNull Register Rd, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("MVN", Rd, null, op2, null, s));
    }

    // Shift/Rotate
    public static void lsr(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("LSR", Rd, Rm, op2, cond, s));
    }
    public static void lsr(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("LSR", Rd, Rm, op2, cond, s));
    }
    public static void lsr(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("LSR", Rd, Rm, op2, null, s));
    }
    public static void asr(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("ASR", Rd, Rm, op2, cond, s));
    }
    public static void asr(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("ASR", Rd, Rm, op2, cond, s));
    }
    public static void asr(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("ASR", Rd, Rm, op2, null, s));
    }
    public static void lsl(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("LSL", Rd, Rm, op2, cond, s));
    }
    public static void lsl(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("LSL", Rd, Rm, op2, cond, s));
    }
    public static void lsl(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("LSL", Rd, Rm, op2, null, s));
    }
    public static void ror(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("ROR", Rd, Rm, op2, cond, s));
    }
    public static void ror(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("ROR", Rd, Rm, op2, cond, s));
    }
    public static void ror(@NotNull Register Rd, @NotNull Register Rm, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("ROR", Rd, Rm, op2, null, s));
    }
    public static void rrx(@NotNull Register Rd, @NotNull Register Rm, @Nullable Cond cond, boolean s) {
        writer.println(dataProcessing("RRX", Rd, Rm, null, cond, s));
    }
    public static void rrx(@NotNull Register Rd, @NotNull Register Rm, @Nullable Cond cond) {
        writer.println(dataProcessing("RRX", Rd, Rm, null, cond, s));
    }
    public static void rrx(@NotNull Register Rd, @NotNull Register Rm) {
        writer.println(dataProcessing("RRX", Rd, Rm, null, null, s));
    }

    // Compare
    public static void cmp(@NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("CMP", null, Rn, op2, cond, s));
    }
    public static void cmp(@NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("CMP", null, Rn, op2, null, s));
    }
    public static void cmn(@NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("CMN", null, Rn, op2, cond, s));
    }
    public static void cmn(@NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("CMN", null, Rn, op2, null, s));
    }
    public static void tst(@NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("TST", null, Rn, op2, cond, s));
    }
    public static void tst(@NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("TST", null, Rn, op2, null, s));
    }
    public static void teq(@NotNull Register Rn, @NotNull FlexibleOperator2 op2, @Nullable Cond cond) {
        writer.println(dataProcessing("TEQ", null, Rn, op2, cond, s));
    }
    public static void teq(@NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        writer.println(dataProcessing("TEQ", null, Rn, op2, null, s));
    }

    // Single Registry
    public static void ldr(@NotNull Register Rd, @NotNull EffectiveAddress EA, @Nullable Cond cond) {
        writer.println(singleRegistry("LDR", Rd, EA.toString(), cond));
    }
    public static void ldr(@NotNull Register Rd, @NotNull EffectiveAddress EA) {
        writer.println(singleRegistry("LDR", Rd, EA.toString(), null));
    }
    public static void ldr(@NotNull Register Rd, @NotNull String Literal, @Nullable Cond cond) {
        writer.println(singleRegistry("LDR", Rd, "=" + Literal, cond));
    }
    public static void ldr(@NotNull Register Rd, @NotNull String Literal) {
        writer.println(singleRegistry("LDR", Rd, "=" + Literal, null));
    }
    public static void ldrb(@NotNull Register Rd, @NotNull EffectiveAddress EA, @Nullable Cond cond) {
        writer.println(singleRegistry("LDRB", Rd, EA.toString(), cond));
    }
    public static void ldrb(@NotNull Register Rd, @NotNull EffectiveAddress EA) {
        writer.println(singleRegistry("LDRB", Rd, EA.toString(), null));
    }
    public static void str(@NotNull Register Rs, @NotNull EffectiveAddress EA, @Nullable Cond cond) {
        writer.println(singleRegistry("STR", Rs, EA.toString(), cond));
    }
    public static void str(@NotNull Register Rs, @NotNull EffectiveAddress EA) {
        writer.println(singleRegistry("STR", Rs, EA.toString(), null));
    }
    public static void strb(@NotNull Register Rs, @NotNull EffectiveAddress EA, @Nullable Cond cond) {
        writer.println(singleRegistry("STRB", Rs, EA.toString(), cond));
    }
    public static void strb(@NotNull Register Rs, @NotNull EffectiveAddress EA) {
        writer.println(singleRegistry("STRB", Rs, EA.toString(), null));
    }

    // Multiple Registry
    public static void ldm(@NotNull Register Rn, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode, @Nullable Cond cond, boolean updateAddress) {
        writer.println(multipleRegistry("LDM", Rn, RegList, addrMode, cond, updateAddress));
    }
    public static void ldm(@NotNull Register Rn, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode, @Nullable Cond cond) {
        writer.println(multipleRegistry("LDM", Rn, RegList, addrMode, cond, updateAddress));
    }
    public static void ldm(@NotNull Register Rn, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode) {
        writer.println(multipleRegistry("LDM", Rn, RegList, addrMode, null, updateAddress));
    }
    public static void ldm(@NotNull Register Rn, @NotNull Set<Register> RegList) {
        writer.println(multipleRegistry("LDM", Rn, RegList, null, null, updateAddress));
    }
    public static void stm(@NotNull Register Rn, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode, @Nullable Cond cond, boolean updateAddress) {
        writer.println(multipleRegistry("STM", Rn, RegList, addrMode, cond, updateAddress));
    }
    public static void stm(@NotNull Register Rn, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode, @Nullable Cond cond) {
        writer.println(multipleRegistry("STM", Rn, RegList, addrMode, cond, updateAddress));
    }
    public static void stm(@NotNull Register Rn, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode) {
        writer.println(multipleRegistry("STM", Rn, RegList, addrMode, null, updateAddress));
    }
    public static void stm(@NotNull Register Rn, @NotNull Set<Register> RegList) {
        writer.println(multipleRegistry("STM", Rn, RegList, null, null, updateAddress));
    }

    // Branch
    public static void b(@NotNull String target, @Nullable Cond cond) {
        String lbl = consumeLabel();
        writer.println(branching(false, target, cond));
    }

    public static void b(@NotNull String target) {
        String lbl = consumeLabel();
        writer.println(branching(false, target, null));
    }

    public static void bl(@NotNull String target, @Nullable Cond cond) {
        String lbl = consumeLabel();
        writer.println(branching(true, target, cond));
    }

    public static void bl(@NotNull String target) {
        String lbl = consumeLabel();
        writer.println(branching(true, target, null));
    }

    // Pseudo and directives
    public static void dcd(int... dn) {
        String lbl = consumeLabel();
        writer.println(String.format("%s DCD %s;", lbl, Arrays.stream(dn).mapToObj(String::valueOf).collect(Collectors.joining(", "))));
    }

    public static String dcd(String text) {
        String lbl = "_str_" + strCounter++;
        byte[] bytes = ArrayUtils.add(text.getBytes(StandardCharsets.UTF_8), (byte) 0);
        String[] values = Utils.bytesToLittleEndianHex(bytes);
        writer.println(String.format("%s DCD %s ;", lbl, String.join(", ", values)));
        return lbl;
    }

    public static void dcb(int... dn) {
        String lbl = consumeLabel();
        writer.println(String.format("%s DCB %s ;", lbl, Arrays.stream(dn).mapToObj(String::valueOf).collect(Collectors.joining(", "))));
    }

    public static void end() {
        String lbl = consumeLabel();
        writer.println(String.format("%s END ;", lbl));
    }

    public static void fill(int n) {
        String lbl = consumeLabel();
        writer.println(String.format("%s FILL %s", lbl, n));
    }

    public static void adr(Register Ra, String label) {
        String lbl = consumeLabel();
        writer.println(String.format("%s ADR %s, %s ;", lbl, Ra.toString(), label));
    }

    public static void equ(String label, String expr) {
        String lbl = consumeLabel();
        writer.println(String.format("%s %s EQU %s ;", lbl, label, expr));
    }

    public static void label(@Nullable String lbl) {
        if (!label.matches("[ ]+")) {
            /* a label is already set */
            writer.println(label);
        }
        String tmp = Objects.requireNonNullElse(lbl, "");
        assert canPadOverflow || tmp.length() <= pad;
        label = StringUtils.rightPad(tmp, pad, " ");
    }

    // Custom pseudo-instructions
    public static void push(@NotNull Register Rs, @Nullable Cond cond) {
        EffectiveAddress EA = new EffectiveAddress(
                RegisterAllocator.StackPointer,
                new Literal(4),
                Indexed.Post
        );
        str(Rs, EA, cond);
    }
    public static void push(@NotNull Register Rs) {
        EffectiveAddress EA = new EffectiveAddress(
                RegisterAllocator.StackPointer,
                new Literal(4),
                Indexed.Post
        );
        str(Rs, EA);
    }

    public static void pop(@NotNull Register Rd, @Nullable Cond cond) {
        EffectiveAddress EA = new EffectiveAddress(
                RegisterAllocator.StackPointer,
                new Literal(-4),
                Indexed.Pre
        );
        ldr(Rd, EA, cond);
    }
    public static void pop(@NotNull Register Rd) {
        EffectiveAddress EA = new EffectiveAddress(
                RegisterAllocator.StackPointer,
                new Literal(-4),
                Indexed.Pre
        );
        ldr(Rd, EA);
    }

    public static void mul(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, op2);
        push(RegisterAllocator.ProgramCounter);
        b("mul");
        mov(Rd, RegisterAllocator.R0);
    }

    public static void mul(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, boolean s) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, op2);
        push(RegisterAllocator.ProgramCounter);
        b("mul");
        mov(Rd, RegisterAllocator.R0, null, s);
    }

    public static void div(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, op2);
        push(RegisterAllocator.ProgramCounter);
        b("div");
        mov(Rd, RegisterAllocator.R0);
    }

    public static void div(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, boolean s) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, op2);
        push(RegisterAllocator.ProgramCounter);
        b("div");
        mov(Rd, RegisterAllocator.R0, null, s);
    }

    public static void mod(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, Rd);
        push(RegisterAllocator.ProgramCounter);
        b("div");
        mov(Rd, RegisterAllocator.R1);
    }

    public static void mod(@NotNull Register Rd, @NotNull Register Rn, @NotNull FlexibleOperator2 op2, boolean s) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, Rd);
        push(RegisterAllocator.ProgramCounter);
        b("div");
        mov(Rd, RegisterAllocator.R1, null, s);
    }

    public static void concatsubroutine() {

        label("concat");
        ldr(RegisterAllocator.R0, "_heap");
        add(RegisterAllocator.R0, RegisterAllocator.R0, HeapOffset);

        Register strOffset = RegisterAllocator.getUnusedRegister();
        Register currentChar = RegisterAllocator.getUnusedRegister();
        Register str2Offset = RegisterAllocator.getUnusedRegister();
        Set<Register> regSet = new HashSet<>(Arrays.asList(RegisterAllocator.R1, RegisterAllocator.R2));


        stm(StackPointer, regSet, AddrMode.FA, null, true);
        mov(strOffset,new Literal(-1));

        label("_end_of_str");
        add(strOffset,strOffset,new Literal(1));
        comment("Load into the currentChar the strOffset char in R1");
        ldrb(currentChar, new EffectiveAddress(RegisterAllocator.R1, strOffset));
        strb(currentChar, new EffectiveAddress(RegisterAllocator.R0, strOffset));
        comment("Compare to null-byte to check if it is the end of the first string");
        cmp(currentChar, new Literal(0x00));
        b("_end_of_str", Cond.NE);
        mov(str2Offset, new Literal(0));
        b("_concat_loop");

        label("_concat_loop");
        ldrb(currentChar, new EffectiveAddress(RegisterAllocator.R2, str2Offset));
        strb(currentChar, new EffectiveAddress(RegisterAllocator.R0, strOffset));
        add(strOffset, strOffset, new Literal(1));
        add(str2Offset, str2Offset, new Literal(1));
        cmp(currentChar, new Literal(0x00));
        b("_concat_loop", Cond.NE);

        add(HeapOffset, HeapOffset, strOffset);
        add(HeapOffset, HeapOffset, str2Offset);

        Register tempRegister = getUnusedRegister();
        comment("Re-align the heap");
        ldr(tempRegister, "0xFFFFFFFC");
        and(HeapOffset, HeapOffset, tempRegister);
        add(HeapOffset, HeapOffset, new Literal(4));
        releaseRegister(tempRegister);

        ldm(StackPointer, regSet, AddrMode.FA, null, true);

        pop(ProgramCounter);


    }

    public static void concatstr(@NotNull Register Rd, @NotNull Register Rn, @NotNull Register Rm) {
        mov(RegisterAllocator.R1, Rn);
        mov(RegisterAllocator.R2, Rm);
        push(RegisterAllocator.ProgramCounter);
        b("concat");
        mov(Rd, RegisterAllocator.R0);
    }



    public static void print(@NotNull Register Rn) {
        mov(RegisterAllocator.R0, Rn);
        push(RegisterAllocator.ProgramCounter);
        b("print");
    }

    public static void println(@NotNull Register Rn) {
        mov(RegisterAllocator.R0, Rn);
        push(RegisterAllocator.ProgramCounter);
        b("println");
    }

    public static void its(@NotNull Register Rd, @NotNull Register Rn) {
        ldr(Rd, "_heap");
        add(Rd, Rd, HeapOffset);
        add(HeapOffset, HeapOffset, new Literal(12));
        mov(R1, Rn);
        mov(R2, Rd);
        push(ProgramCounter);
        b("its");
    }

    @NotNull
    private static String consumeLabel() {
        String lbl = label;
        label = StringUtils.rightPad("", pad, " ");
        lbl = StringUtils.rightPad(lbl, canPadOverflow ? label.length() : pad, " ");
        return lbl;
    }

    public static void comment(@Nullable String text) {
        comment = Objects.requireNonNullElse(text, "");
    }

    @NotNull
    private static String consumeComment() {
        String txt = comment;
        comment = "";
        return txt;
    }

    @NotNull
    private static String dataProcessing(@NotNull String opCode, @Nullable Register dest, @Nullable Register op1, @Nullable FlexibleOperator2 op2, @Nullable Cond cond, boolean s) {
        if (op1 != null && !RegisterAllocator.isAllocated(op1)) {
            throw new IllegalArgumentException(String.format("Register %s is not allocated.", op1.toString()));
        }
        if (op2 instanceof Register && !RegisterAllocator.isAllocated((Register) op2)) {
            throw new IllegalArgumentException(String.format("Register %s is not allocated.", op2.toString()));
        }
        String lbl = consumeLabel();
        String cmt = consumeComment();
        return String.format("%s %s%s%s %s%s%s ;%s",
                lbl,
                opCode,
                s ? "S" : "",
                cond != null ? cond.name() : "",
                Objects.toString(dest, ""),
                (dest != null && op1 != null ? ", " : "") + Objects.toString(op1, ""),
                op2 != null ? ", " + op2.toString() : "",
                !cmt.equals("") ? " "+cmt : "");
    }

    @NotNull
    private static String singleRegistry(@NotNull String opCode, @NotNull Register Rn, @NotNull String EA, @Nullable Cond cond) {
        if (!RegisterAllocator.isAllocated(Rn)) {
            throw new IllegalArgumentException(String.format("Register %s is not allocated.", Rn.toString()));
        }
        String lbl = consumeLabel();
        String cmt = consumeComment();
        return String.format("%s %s%s %s, %s ;%s",
                lbl,
                opCode,
                cond != null ? cond.name() : "",
                Rn.toString(),
                EA,
                !cmt.equals("") ? " "+cmt : "");
    }

    @NotNull
    private static String multipleRegistry(@NotNull String opCode, @NotNull Register StackPointer, @NotNull Set<Register> RegList, @Nullable AddrMode addrMode, @Nullable Cond cond, boolean updateAddress) {
        RegList.forEach(register -> {
            if (!RegisterAllocator.isAllocated(register)) {
                throw new IllegalArgumentException(String.format("Register %s is not allocated.", register.toString()));
            }
        });
        String lbl = consumeLabel();
        String cmt = consumeComment();
        return String.format("%s %s%s%s %s%s, {%s} ;%s",
                lbl,
                opCode,
                addrMode != null ? addrMode.name() : null,
                cond != null ? cond.name() : "",
                StackPointer,
                updateAddress ? "!" : "",
                RegList.stream().map(Register::toString).collect(Collectors.joining(",")),
                !cmt.equals("") ? " "+cmt : "");
    }

    @NotNull
    private static String branching(@NotNull boolean link, @NotNull String target, @Nullable Cond cond) {
        String lbl = consumeLabel();
        String cmt = consumeComment();
        return String.format("%s %s%s%s %s ;%s",
                lbl,
                "B",
                link ? "L" : "",
                cond != null ? cond.name() : "",
                target,
                !cmt.equals("") ? " "+cmt : "");
    }

    private static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
