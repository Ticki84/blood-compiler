package BloodCompiler.assembly.visual;

import java.util.List;
import java.util.stream.Stream;

import BloodCompiler.assembly.InstructionWriter;
import BloodCompiler.assembly.visual.security.ExitTrappedException;
import BloodCompiler.assembly.visual.security.ExitTrapper;

import visual.EmulatorLogFile;
import visual.HeadlessController;

public class Launcher {
    private static final int instMemSize = 0x10000;
    /* we make sure that the output buffer is always the first symbol in memory */
    private static final int outputBufferAddress = instMemSize;

    /* VisUAL offsets line numbers by one for some reason */
    private static final List<Integer> breakpoints = List.of(11 - 1, 25 - 1);

    private Launcher() {}

    /* array of all word addresses in the output buffer */
    private static String[] getOutputRange() {
        return Stream.iterate(outputBufferAddress, n -> n + 4)
                .limit(InstructionWriter.outputBufferLength / 4)
                .map(n -> String.format("0x%X", n))
                .toArray(String[]::new);
    }

    public static List<String> executeAndParseOutput(String assemblyFile) {
        EmulatorLogFile.configureLogging("", true, false, false, false, false, false, true, false, getOutputRange());
        HeadlessController.setLogMode(EmulatorLogFile.LogMode.BREAKPOINT);
        HeadlessController.setBreakpoints(breakpoints);
        HeadlessController.setInstMemSize(instMemSize);
        String logFile = String.format("%s_log.xml", assemblyFile);

        ExitTrapper.forbidSystemExitCall();
        int code = -1;
        try {
            HeadlessController.runFile(assemblyFile, logFile);
        } catch (ExitTrappedException e) {
            code = e.getCode();
        } finally {
            ExitTrapper.enableSystemExitCall();
            if (code != 0) {
                System.err.println("VisUAL emulator exited with error code " + code);
                System.exit(code);
            }
        }

        return OutputParser.parseOutput(logFile);
    }

    public static void run(String assemblyFile) {
        List<String> output = executeAndParseOutput(assemblyFile);

        System.out.println("---- PROGRAM OUTPUT ----");
        output.forEach(System.out::print);
        System.out.println("---- END PROGRAM OUTPUT ----");
    }
}
