// $ANTLR null D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g 2021-04-15 00:11:58

package BloodCompiler.antlr;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class BloodParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ARGS", "ASSIGNMENT", "ATTR_PROPS", 
		"BLOCK", "BODY_CLASS", "BODY_CONSTRUCTOR", "BODY_METHOD", "CAST", "CLASSDEF", 
		"COMMENT", "DEF_ATTRIBUTE", "DEF_CONSTRUCTOR", "DEF_LVAR", "DEF_LVARS", 
		"DEF_METHOD", "ELSE", "EXPRESSION", "EXPRESSIONS", "ID", "IDCLASS", "IF", 
		"INSTANTIATE", "INT", "MESSAGE", "MESSAGE_BASE", "MESSAGE_CALL", "METHOD_PROPS", 
		"NEWLINE", "OP_ADD", "OP_AND", "OP_CAT", "OP_DIV", "OP_EQU", "OP_GEQ", 
		"OP_GRT", "OP_LEQ", "OP_LES", "OP_MOD", "OP_MUL", "OP_NEG", "OP_NEQ", 
		"OP_NOT", "OP_OR", "OP_SHL", "OP_SHR", "OP_SUB", "OP_XOR", "OVERRIDE", 
		"PARAMS_CLASS", "PARAMS_CONSTRUCTOR", "PARAMS_METHOD", "PARAM_CLASS", 
		"PARAM_CONSTRUCTOR", "PARAM_METHOD", "PROGRAM", "RESULT", "RETURN", "STATIC", 
		"STRING", "SUPER", "SUPERCLASS", "SUPERCLASS_INIT", "THEN", "THIS", "VAR", 
		"WHILE", "WS", "'%'", "'&'", "'('", "')'", "'*'", "'+'", "','", "'-'", 
		"'.'", "'/'", "':'", "':='", "';'", "'<'", "'<<'", "'<='", "'<>'", "'='", 
		"'>'", "'>='", "'>>'", "'and'", "'as'", "'class'", "'def'", "'do'", "'else'", 
		"'extends'", "'if'", "'is'", "'new'", "'or'", "'override'", "'result'", 
		"'return'", "'static'", "'super'", "'then'", "'this'", "'var'", "'while'", 
		"'xor'", "'{'", "'}'", "'~'"
	};
	public static final int EOF=-1;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int T__89=89;
	public static final int T__90=90;
	public static final int T__91=91;
	public static final int T__92=92;
	public static final int T__93=93;
	public static final int T__94=94;
	public static final int T__95=95;
	public static final int T__96=96;
	public static final int T__97=97;
	public static final int T__98=98;
	public static final int T__99=99;
	public static final int T__100=100;
	public static final int T__101=101;
	public static final int T__102=102;
	public static final int T__103=103;
	public static final int T__104=104;
	public static final int T__105=105;
	public static final int T__106=106;
	public static final int T__107=107;
	public static final int T__108=108;
	public static final int T__109=109;
	public static final int T__110=110;
	public static final int T__111=111;
	public static final int T__112=112;
	public static final int T__113=113;
	public static final int T__114=114;
	public static final int T__115=115;
	public static final int ARGS=4;
	public static final int ASSIGNMENT=5;
	public static final int ATTR_PROPS=6;
	public static final int BLOCK=7;
	public static final int BODY_CLASS=8;
	public static final int BODY_CONSTRUCTOR=9;
	public static final int BODY_METHOD=10;
	public static final int CAST=11;
	public static final int CLASSDEF=12;
	public static final int COMMENT=13;
	public static final int DEF_ATTRIBUTE=14;
	public static final int DEF_CONSTRUCTOR=15;
	public static final int DEF_LVAR=16;
	public static final int DEF_LVARS=17;
	public static final int DEF_METHOD=18;
	public static final int ELSE=19;
	public static final int EXPRESSION=20;
	public static final int EXPRESSIONS=21;
	public static final int ID=22;
	public static final int IDCLASS=23;
	public static final int IF=24;
	public static final int INSTANTIATE=25;
	public static final int INT=26;
	public static final int MESSAGE=27;
	public static final int MESSAGE_BASE=28;
	public static final int MESSAGE_CALL=29;
	public static final int METHOD_PROPS=30;
	public static final int NEWLINE=31;
	public static final int OP_ADD=32;
	public static final int OP_AND=33;
	public static final int OP_CAT=34;
	public static final int OP_DIV=35;
	public static final int OP_EQU=36;
	public static final int OP_GEQ=37;
	public static final int OP_GRT=38;
	public static final int OP_LEQ=39;
	public static final int OP_LES=40;
	public static final int OP_MOD=41;
	public static final int OP_MUL=42;
	public static final int OP_NEG=43;
	public static final int OP_NEQ=44;
	public static final int OP_NOT=45;
	public static final int OP_OR=46;
	public static final int OP_SHL=47;
	public static final int OP_SHR=48;
	public static final int OP_SUB=49;
	public static final int OP_XOR=50;
	public static final int OVERRIDE=51;
	public static final int PARAMS_CLASS=52;
	public static final int PARAMS_CONSTRUCTOR=53;
	public static final int PARAMS_METHOD=54;
	public static final int PARAM_CLASS=55;
	public static final int PARAM_CONSTRUCTOR=56;
	public static final int PARAM_METHOD=57;
	public static final int PROGRAM=58;
	public static final int RESULT=59;
	public static final int RETURN=60;
	public static final int STATIC=61;
	public static final int STRING=62;
	public static final int SUPER=63;
	public static final int SUPERCLASS=64;
	public static final int SUPERCLASS_INIT=65;
	public static final int THEN=66;
	public static final int THIS=67;
	public static final int VAR=68;
	public static final int WHILE=69;
	public static final int WS=70;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public BloodParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public BloodParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return BloodParser.tokenNames; }
	@Override public String getGrammarFileName() { return "D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g"; }


	  @Override
	  public void reportError(RecognitionException e) {
	    throw new RuntimeException(e);
	  }


	public static class prog_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "prog"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:1: prog : ( defClasse )* ( instruction )* EOF -> ^( PROGRAM ( defClasse )* ( instruction )* ) ;
	public final BloodParser.prog_return prog() throws RecognitionException {
		BloodParser.prog_return retval = new BloodParser.prog_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token EOF3=null;
		ParserRuleReturnScope defClasse1 =null;
		ParserRuleReturnScope instruction2 =null;

		Object EOF3_tree=null;
		RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
		RewriteRuleSubtreeStream stream_defClasse=new RewriteRuleSubtreeStream(adaptor,"rule defClasse");
		RewriteRuleSubtreeStream stream_instruction=new RewriteRuleSubtreeStream(adaptor,"rule instruction");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:6: ( ( defClasse )* ( instruction )* EOF -> ^( PROGRAM ( defClasse )* ( instruction )* ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:8: ( defClasse )* ( instruction )* EOF
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:8: ( defClasse )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==94) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:8: defClasse
					{
					pushFollow(FOLLOW_defClasse_in_prog313);
					defClasse1=defClasse();
					state._fsp--;

					stream_defClasse.add(defClasse1.getTree());
					}
					break;

				default :
					break loop1;
				}
			}

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:19: ( instruction )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= ID && LA2_0 <= IDCLASS)||LA2_0==INT||LA2_0==STRING||LA2_0==73||LA2_0==76||LA2_0==78||LA2_0==99||LA2_0==101||(LA2_0 >= 104 && LA2_0 <= 105)||LA2_0==107||LA2_0==109||LA2_0==111||LA2_0==113||LA2_0==115) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:90:19: instruction
					{
					pushFollow(FOLLOW_instruction_in_prog316);
					instruction2=instruction();
					state._fsp--;

					stream_instruction.add(instruction2.getTree());
					}
					break;

				default :
					break loop2;
				}
			}

			EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_prog319);  
			stream_EOF.add(EOF3);

			// AST REWRITE
			// elements: instruction, defClasse
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 91:3: -> ^( PROGRAM ( defClasse )* ( instruction )* )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:91:6: ^( PROGRAM ( defClasse )* ( instruction )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PROGRAM, "PROGRAM"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:91:16: ( defClasse )*
				while ( stream_defClasse.hasNext() ) {
					adaptor.addChild(root_1, stream_defClasse.nextTree());
				}
				stream_defClasse.reset();

				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:91:27: ( instruction )*
				while ( stream_instruction.hasNext() ) {
					adaptor.addChild(root_1, stream_instruction.nextTree());
				}
				stream_instruction.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "prog"


	public static class instruction_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "instruction"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:95:1: instruction : (a= expression ( ':' IDCLASS ( ':=' b= expression -> ^( DEF_LVAR $a IDCLASS $b) | -> ^( DEF_LVAR $a IDCLASS ) ) | ( ':=' b= expression -> ^( ASSIGNMENT $a $b) | -> $a) ) ';' | 'return' ';' -> ^( RETURN ) | 'if' expression 'then' a= instruction 'else' b= instruction -> ^( IF expression ^( THEN ( $a)? ) ^( ELSE ( $b)? ) ) | 'while' expression 'do' instruction -> ^( WHILE expression instruction ) | bloc -> ( ^( BLOCK bloc ) )? );
	public final BloodParser.instruction_return instruction() throws RecognitionException {
		BloodParser.instruction_return retval = new BloodParser.instruction_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal4=null;
		Token IDCLASS5=null;
		Token string_literal6=null;
		Token string_literal7=null;
		Token char_literal8=null;
		Token string_literal9=null;
		Token char_literal10=null;
		Token string_literal11=null;
		Token string_literal13=null;
		Token string_literal14=null;
		Token string_literal15=null;
		Token string_literal17=null;
		ParserRuleReturnScope a =null;
		ParserRuleReturnScope b =null;
		ParserRuleReturnScope expression12 =null;
		ParserRuleReturnScope expression16 =null;
		ParserRuleReturnScope instruction18 =null;
		ParserRuleReturnScope bloc19 =null;

		Object char_literal4_tree=null;
		Object IDCLASS5_tree=null;
		Object string_literal6_tree=null;
		Object string_literal7_tree=null;
		Object char_literal8_tree=null;
		Object string_literal9_tree=null;
		Object char_literal10_tree=null;
		Object string_literal11_tree=null;
		Object string_literal13_tree=null;
		Object string_literal14_tree=null;
		Object string_literal15_tree=null;
		Object string_literal17_tree=null;
		RewriteRuleTokenStream stream_99=new RewriteRuleTokenStream(adaptor,"token 99");
		RewriteRuleTokenStream stream_111=new RewriteRuleTokenStream(adaptor,"token 111");
		RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_108=new RewriteRuleTokenStream(adaptor,"token 108");
		RewriteRuleTokenStream stream_82=new RewriteRuleTokenStream(adaptor,"token 82");
		RewriteRuleTokenStream stream_83=new RewriteRuleTokenStream(adaptor,"token 83");
		RewriteRuleTokenStream stream_96=new RewriteRuleTokenStream(adaptor,"token 96");
		RewriteRuleTokenStream stream_97=new RewriteRuleTokenStream(adaptor,"token 97");
		RewriteRuleSubtreeStream stream_bloc=new RewriteRuleSubtreeStream(adaptor,"rule bloc");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_instruction=new RewriteRuleSubtreeStream(adaptor,"rule instruction");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:96:2: (a= expression ( ':' IDCLASS ( ':=' b= expression -> ^( DEF_LVAR $a IDCLASS $b) | -> ^( DEF_LVAR $a IDCLASS ) ) | ( ':=' b= expression -> ^( ASSIGNMENT $a $b) | -> $a) ) ';' | 'return' ';' -> ^( RETURN ) | 'if' expression 'then' a= instruction 'else' b= instruction -> ^( IF expression ^( THEN ( $a)? ) ^( ELSE ( $b)? ) ) | 'while' expression 'do' instruction -> ^( WHILE expression instruction ) | bloc -> ( ^( BLOCK bloc ) )? )
			int alt6=5;
			switch ( input.LA(1) ) {
			case ID:
			case IDCLASS:
			case INT:
			case STRING:
			case 73:
			case 76:
			case 78:
			case 101:
			case 104:
			case 107:
			case 109:
			case 115:
				{
				alt6=1;
				}
				break;
			case 105:
				{
				alt6=2;
				}
				break;
			case 99:
				{
				alt6=3;
				}
				break;
			case 111:
				{
				alt6=4;
				}
				break;
			case 113:
				{
				alt6=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			switch (alt6) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:96:4: a= expression ( ':' IDCLASS ( ':=' b= expression -> ^( DEF_LVAR $a IDCLASS $b) | -> ^( DEF_LVAR $a IDCLASS ) ) | ( ':=' b= expression -> ^( ASSIGNMENT $a $b) | -> $a) ) ';'
					{
					pushFollow(FOLLOW_expression_in_instruction349);
					a=expression();
					state._fsp--;

					stream_expression.add(a.getTree());
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:97:3: ( ':' IDCLASS ( ':=' b= expression -> ^( DEF_LVAR $a IDCLASS $b) | -> ^( DEF_LVAR $a IDCLASS ) ) | ( ':=' b= expression -> ^( ASSIGNMENT $a $b) | -> $a) )
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==81) ) {
						alt5=1;
					}
					else if ( ((LA5_0 >= 82 && LA5_0 <= 83)) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:97:5: ':' IDCLASS ( ':=' b= expression -> ^( DEF_LVAR $a IDCLASS $b) | -> ^( DEF_LVAR $a IDCLASS ) )
							{
							char_literal4=(Token)match(input,81,FOLLOW_81_in_instruction355);  
							stream_81.add(char_literal4);

							IDCLASS5=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_instruction357);  
							stream_IDCLASS.add(IDCLASS5);

							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:98:4: ( ':=' b= expression -> ^( DEF_LVAR $a IDCLASS $b) | -> ^( DEF_LVAR $a IDCLASS ) )
							int alt3=2;
							int LA3_0 = input.LA(1);
							if ( (LA3_0==82) ) {
								alt3=1;
							}
							else if ( (LA3_0==83) ) {
								alt3=2;
							}

							else {
								NoViableAltException nvae =
									new NoViableAltException("", 3, 0, input);
								throw nvae;
							}

							switch (alt3) {
								case 1 :
									// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:98:6: ':=' b= expression
									{
									string_literal6=(Token)match(input,82,FOLLOW_82_in_instruction364);  
									stream_82.add(string_literal6);

									pushFollow(FOLLOW_expression_in_instruction368);
									b=expression();
									state._fsp--;

									stream_expression.add(b.getTree());
									// AST REWRITE
									// elements: b, IDCLASS, a
									// token labels: 
									// rule labels: a, b, retval
									// token list labels: 
									// rule list labels: 
									// wildcard labels: 
									retval.tree = root_0;
									RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);
									RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
									RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

									root_0 = (Object)adaptor.nil();
									// 99:5: -> ^( DEF_LVAR $a IDCLASS $b)
									{
										// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:99:8: ^( DEF_LVAR $a IDCLASS $b)
										{
										Object root_1 = (Object)adaptor.nil();
										root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DEF_LVAR, "DEF_LVAR"), root_1);
										adaptor.addChild(root_1, stream_a.nextTree());
										adaptor.addChild(root_1, stream_IDCLASS.nextNode());
										adaptor.addChild(root_1, stream_b.nextTree());
										adaptor.addChild(root_0, root_1);
										}

									}


									retval.tree = root_0;

									}
									break;
								case 2 :
									// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:100:6: 
									{
									// AST REWRITE
									// elements: IDCLASS, a
									// token labels: 
									// rule labels: a, retval
									// token list labels: 
									// rule list labels: 
									// wildcard labels: 
									retval.tree = root_0;
									RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);
									RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

									root_0 = (Object)adaptor.nil();
									// 100:6: -> ^( DEF_LVAR $a IDCLASS )
									{
										// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:100:9: ^( DEF_LVAR $a IDCLASS )
										{
										Object root_1 = (Object)adaptor.nil();
										root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DEF_LVAR, "DEF_LVAR"), root_1);
										adaptor.addChild(root_1, stream_a.nextTree());
										adaptor.addChild(root_1, stream_IDCLASS.nextNode());
										adaptor.addChild(root_0, root_1);
										}

									}


									retval.tree = root_0;

									}
									break;

							}

							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:102:5: ( ':=' b= expression -> ^( ASSIGNMENT $a $b) | -> $a)
							{
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:102:5: ( ':=' b= expression -> ^( ASSIGNMENT $a $b) | -> $a)
							int alt4=2;
							int LA4_0 = input.LA(1);
							if ( (LA4_0==82) ) {
								alt4=1;
							}
							else if ( (LA4_0==83) ) {
								alt4=2;
							}

							else {
								NoViableAltException nvae =
									new NoViableAltException("", 4, 0, input);
								throw nvae;
							}

							switch (alt4) {
								case 1 :
									// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:102:7: ':=' b= expression
									{
									string_literal7=(Token)match(input,82,FOLLOW_82_in_instruction415);  
									stream_82.add(string_literal7);

									pushFollow(FOLLOW_expression_in_instruction419);
									b=expression();
									state._fsp--;

									stream_expression.add(b.getTree());
									// AST REWRITE
									// elements: a, b
									// token labels: 
									// rule labels: a, b, retval
									// token list labels: 
									// rule list labels: 
									// wildcard labels: 
									retval.tree = root_0;
									RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);
									RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
									RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

									root_0 = (Object)adaptor.nil();
									// 103:5: -> ^( ASSIGNMENT $a $b)
									{
										// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:103:8: ^( ASSIGNMENT $a $b)
										{
										Object root_1 = (Object)adaptor.nil();
										root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ASSIGNMENT, "ASSIGNMENT"), root_1);
										adaptor.addChild(root_1, stream_a.nextTree());
										adaptor.addChild(root_1, stream_b.nextTree());
										adaptor.addChild(root_0, root_1);
										}

									}


									retval.tree = root_0;

									}
									break;
								case 2 :
									// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:104:6: 
									{
									// AST REWRITE
									// elements: a
									// token labels: 
									// rule labels: a, retval
									// token list labels: 
									// rule list labels: 
									// wildcard labels: 
									retval.tree = root_0;
									RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);
									RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

									root_0 = (Object)adaptor.nil();
									// 104:6: -> $a
									{
										adaptor.addChild(root_0, stream_a.nextTree());
									}


									retval.tree = root_0;

									}
									break;

							}

							}
							break;

					}

					char_literal8=(Token)match(input,83,FOLLOW_83_in_instruction458);  
					stream_83.add(char_literal8);

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:109:4: 'return' ';'
					{
					string_literal9=(Token)match(input,105,FOLLOW_105_in_instruction465);  
					stream_105.add(string_literal9);

					char_literal10=(Token)match(input,83,FOLLOW_83_in_instruction467);  
					stream_83.add(char_literal10);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 110:3: -> ^( RETURN )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:110:6: ^( RETURN )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RETURN, "RETURN"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:111:4: 'if' expression 'then' a= instruction 'else' b= instruction
					{
					string_literal11=(Token)match(input,99,FOLLOW_99_in_instruction481);  
					stream_99.add(string_literal11);

					pushFollow(FOLLOW_expression_in_instruction483);
					expression12=expression();
					state._fsp--;

					stream_expression.add(expression12.getTree());
					string_literal13=(Token)match(input,108,FOLLOW_108_in_instruction485);  
					stream_108.add(string_literal13);

					pushFollow(FOLLOW_instruction_in_instruction489);
					a=instruction();
					state._fsp--;

					stream_instruction.add(a.getTree());
					string_literal14=(Token)match(input,97,FOLLOW_97_in_instruction491);  
					stream_97.add(string_literal14);

					pushFollow(FOLLOW_instruction_in_instruction495);
					b=instruction();
					state._fsp--;

					stream_instruction.add(b.getTree());
					// AST REWRITE
					// elements: expression, a, b
					// token labels: 
					// rule labels: a, b, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.getTree():null);
					RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 112:3: -> ^( IF expression ^( THEN ( $a)? ) ^( ELSE ( $b)? ) )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:112:6: ^( IF expression ^( THEN ( $a)? ) ^( ELSE ( $b)? ) )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IF, "IF"), root_1);
						adaptor.addChild(root_1, stream_expression.nextTree());
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:112:22: ^( THEN ( $a)? )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(THEN, "THEN"), root_2);
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:112:30: ( $a)?
						if ( stream_a.hasNext() ) {
							adaptor.addChild(root_2, stream_a.nextTree());
						}
						stream_a.reset();

						adaptor.addChild(root_1, root_2);
						}

						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:112:34: ^( ELSE ( $b)? )
						{
						Object root_2 = (Object)adaptor.nil();
						root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ELSE, "ELSE"), root_2);
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:112:42: ( $b)?
						if ( stream_b.hasNext() ) {
							adaptor.addChild(root_2, stream_b.nextTree());
						}
						stream_b.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:113:4: 'while' expression 'do' instruction
					{
					string_literal15=(Token)match(input,111,FOLLOW_111_in_instruction527);  
					stream_111.add(string_literal15);

					pushFollow(FOLLOW_expression_in_instruction529);
					expression16=expression();
					state._fsp--;

					stream_expression.add(expression16.getTree());
					string_literal17=(Token)match(input,96,FOLLOW_96_in_instruction531);  
					stream_96.add(string_literal17);

					pushFollow(FOLLOW_instruction_in_instruction533);
					instruction18=instruction();
					state._fsp--;

					stream_instruction.add(instruction18.getTree());
					// AST REWRITE
					// elements: expression, instruction
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 114:3: -> ^( WHILE expression instruction )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:114:6: ^( WHILE expression instruction )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(WHILE, "WHILE"), root_1);
						adaptor.addChild(root_1, stream_expression.nextTree());
						adaptor.addChild(root_1, stream_instruction.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:115:4: bloc
					{
					pushFollow(FOLLOW_bloc_in_instruction550);
					bloc19=bloc();
					state._fsp--;

					stream_bloc.add(bloc19.getTree());
					// AST REWRITE
					// elements: bloc
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 116:3: -> ( ^( BLOCK bloc ) )?
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:116:6: ( ^( BLOCK bloc ) )?
						if ( stream_bloc.hasNext() ) {
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:116:6: ^( BLOCK bloc )
							{
							Object root_1 = (Object)adaptor.nil();
							root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(BLOCK, "BLOCK"), root_1);
							adaptor.addChild(root_1, stream_bloc.nextTree());
							adaptor.addChild(root_0, root_1);
							}

						}
						stream_bloc.reset();

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "instruction"


	public static class bloc_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "bloc"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:119:1: bloc : '{' ( (a+= instruction )+ ( 'is' (b+= instruction )+ -> ^( DEF_LVARS ( $a)+ ) ( $b)+ | -> ( $a)+ ) | ->) '}' ;
	public final BloodParser.bloc_return bloc() throws RecognitionException {
		BloodParser.bloc_return retval = new BloodParser.bloc_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal20=null;
		Token string_literal21=null;
		Token char_literal22=null;
		List<Object> list_a=null;
		List<Object> list_b=null;
		RuleReturnScope a = null;
		RuleReturnScope b = null;
		Object char_literal20_tree=null;
		Object string_literal21_tree=null;
		Object char_literal22_tree=null;
		RewriteRuleTokenStream stream_100=new RewriteRuleTokenStream(adaptor,"token 100");
		RewriteRuleTokenStream stream_113=new RewriteRuleTokenStream(adaptor,"token 113");
		RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
		RewriteRuleSubtreeStream stream_instruction=new RewriteRuleSubtreeStream(adaptor,"rule instruction");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:119:6: ( '{' ( (a+= instruction )+ ( 'is' (b+= instruction )+ -> ^( DEF_LVARS ( $a)+ ) ( $b)+ | -> ( $a)+ ) | ->) '}' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:119:8: '{' ( (a+= instruction )+ ( 'is' (b+= instruction )+ -> ^( DEF_LVARS ( $a)+ ) ( $b)+ | -> ( $a)+ ) | ->) '}'
			{
			char_literal20=(Token)match(input,113,FOLLOW_113_in_bloc573);  
			stream_113.add(char_literal20);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:120:3: ( (a+= instruction )+ ( 'is' (b+= instruction )+ -> ^( DEF_LVARS ( $a)+ ) ( $b)+ | -> ( $a)+ ) | ->)
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( ((LA10_0 >= ID && LA10_0 <= IDCLASS)||LA10_0==INT||LA10_0==STRING||LA10_0==73||LA10_0==76||LA10_0==78||LA10_0==99||LA10_0==101||(LA10_0 >= 104 && LA10_0 <= 105)||LA10_0==107||LA10_0==109||LA10_0==111||LA10_0==113||LA10_0==115) ) {
				alt10=1;
			}
			else if ( (LA10_0==114) ) {
				alt10=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				throw nvae;
			}

			switch (alt10) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:120:5: (a+= instruction )+ ( 'is' (b+= instruction )+ -> ^( DEF_LVARS ( $a)+ ) ( $b)+ | -> ( $a)+ )
					{
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:120:5: (a+= instruction )+
					int cnt7=0;
					loop7:
					while (true) {
						int alt7=2;
						int LA7_0 = input.LA(1);
						if ( ((LA7_0 >= ID && LA7_0 <= IDCLASS)||LA7_0==INT||LA7_0==STRING||LA7_0==73||LA7_0==76||LA7_0==78||LA7_0==99||LA7_0==101||(LA7_0 >= 104 && LA7_0 <= 105)||LA7_0==107||LA7_0==109||LA7_0==111||LA7_0==113||LA7_0==115) ) {
							alt7=1;
						}

						switch (alt7) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:120:6: a+= instruction
							{
							pushFollow(FOLLOW_instruction_in_bloc583);
							a=instruction();
							state._fsp--;

							stream_instruction.add(a.getTree());
							if (list_a==null) list_a=new ArrayList<Object>();
							list_a.add(a.getTree());
							}
							break;

						default :
							if ( cnt7 >= 1 ) break loop7;
							EarlyExitException eee = new EarlyExitException(7, input);
							throw eee;
						}
						cnt7++;
					}

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:121:4: ( 'is' (b+= instruction )+ -> ^( DEF_LVARS ( $a)+ ) ( $b)+ | -> ( $a)+ )
					int alt9=2;
					int LA9_0 = input.LA(1);
					if ( (LA9_0==100) ) {
						alt9=1;
					}
					else if ( (LA9_0==114) ) {
						alt9=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 9, 0, input);
						throw nvae;
					}

					switch (alt9) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:121:6: 'is' (b+= instruction )+
							{
							string_literal21=(Token)match(input,100,FOLLOW_100_in_bloc592);  
							stream_100.add(string_literal21);

							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:121:11: (b+= instruction )+
							int cnt8=0;
							loop8:
							while (true) {
								int alt8=2;
								int LA8_0 = input.LA(1);
								if ( ((LA8_0 >= ID && LA8_0 <= IDCLASS)||LA8_0==INT||LA8_0==STRING||LA8_0==73||LA8_0==76||LA8_0==78||LA8_0==99||LA8_0==101||(LA8_0 >= 104 && LA8_0 <= 105)||LA8_0==107||LA8_0==109||LA8_0==111||LA8_0==113||LA8_0==115) ) {
									alt8=1;
								}

								switch (alt8) {
								case 1 :
									// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:121:12: b+= instruction
									{
									pushFollow(FOLLOW_instruction_in_bloc597);
									b=instruction();
									state._fsp--;

									stream_instruction.add(b.getTree());
									if (list_b==null) list_b=new ArrayList<Object>();
									list_b.add(b.getTree());
									}
									break;

								default :
									if ( cnt8 >= 1 ) break loop8;
									EarlyExitException eee = new EarlyExitException(8, input);
									throw eee;
								}
								cnt8++;
							}

							// AST REWRITE
							// elements: b, a
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: a, b
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
							RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"token a",list_a);
							RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"token b",list_b);
							root_0 = (Object)adaptor.nil();
							// 122:5: -> ^( DEF_LVARS ( $a)+ ) ( $b)+
							{
								// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:122:8: ^( DEF_LVARS ( $a)+ )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DEF_LVARS, "DEF_LVARS"), root_1);
								if ( !(stream_a.hasNext()) ) {
									throw new RewriteEarlyExitException();
								}
								while ( stream_a.hasNext() ) {
									adaptor.addChild(root_1, stream_a.nextTree());
								}
								stream_a.reset();

								adaptor.addChild(root_0, root_1);
								}

								if ( !(stream_b.hasNext()) ) {
									throw new RewriteEarlyExitException();
								}
								while ( stream_b.hasNext() ) {
									adaptor.addChild(root_0, stream_b.nextTree());
								}
								stream_b.reset();

							}


							retval.tree = root_0;

							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:123:6: 
							{
							// AST REWRITE
							// elements: a
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: a
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);
							RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"token a",list_a);
							root_0 = (Object)adaptor.nil();
							// 123:6: -> ( $a)+
							{
								if ( !(stream_a.hasNext()) ) {
									throw new RewriteEarlyExitException();
								}
								while ( stream_a.hasNext() ) {
									adaptor.addChild(root_0, stream_a.nextTree());
								}
								stream_a.reset();

							}


							retval.tree = root_0;

							}
							break;

					}

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:125:5: 
					{
					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 125:5: ->
					{
						root_0 = null;
					}


					retval.tree = root_0;

					}
					break;

			}

			char_literal22=(Token)match(input,114,FOLLOW_114_in_bloc647);  
			stream_114.add(char_literal22);

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "bloc"


	public static class constante_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "constante"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:130:1: constante : ( INT ^| STRING ^);
	public final BloodParser.constante_return constante() throws RecognitionException {
		BloodParser.constante_return retval = new BloodParser.constante_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token INT23=null;
		Token STRING24=null;

		Object INT23_tree=null;
		Object STRING24_tree=null;

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:131:2: ( INT ^| STRING ^)
			int alt11=2;
			int LA11_0 = input.LA(1);
			if ( (LA11_0==INT) ) {
				alt11=1;
			}
			else if ( (LA11_0==STRING) ) {
				alt11=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:131:4: INT ^
					{
					root_0 = (Object)adaptor.nil();


					INT23=(Token)match(input,INT,FOLLOW_INT_in_constante659); 
					INT23_tree = (Object)adaptor.create(INT23);
					root_0 = (Object)adaptor.becomeRoot(INT23_tree, root_0);

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:132:4: STRING ^
					{
					root_0 = (Object)adaptor.nil();


					STRING24=(Token)match(input,STRING,FOLLOW_STRING_in_constante665); 
					STRING24_tree = (Object)adaptor.create(STRING24);
					root_0 = (Object)adaptor.becomeRoot(STRING24_tree, root_0);

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "constante"


	public static class instanciation_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "instanciation"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:135:1: instanciation : 'new' IDCLASS '(' args ')' -> ^( INSTANTIATE IDCLASS args ) ;
	public final BloodParser.instanciation_return instanciation() throws RecognitionException {
		BloodParser.instanciation_return retval = new BloodParser.instanciation_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal25=null;
		Token IDCLASS26=null;
		Token char_literal27=null;
		Token char_literal29=null;
		ParserRuleReturnScope args28 =null;

		Object string_literal25_tree=null;
		Object IDCLASS26_tree=null;
		Object char_literal27_tree=null;
		Object char_literal29_tree=null;
		RewriteRuleTokenStream stream_101=new RewriteRuleTokenStream(adaptor,"token 101");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleSubtreeStream stream_args=new RewriteRuleSubtreeStream(adaptor,"rule args");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:136:2: ( 'new' IDCLASS '(' args ')' -> ^( INSTANTIATE IDCLASS args ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:136:4: 'new' IDCLASS '(' args ')'
			{
			string_literal25=(Token)match(input,101,FOLLOW_101_in_instanciation677);  
			stream_101.add(string_literal25);

			IDCLASS26=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_instanciation679);  
			stream_IDCLASS.add(IDCLASS26);

			char_literal27=(Token)match(input,73,FOLLOW_73_in_instanciation681);  
			stream_73.add(char_literal27);

			pushFollow(FOLLOW_args_in_instanciation683);
			args28=args();
			state._fsp--;

			stream_args.add(args28.getTree());
			char_literal29=(Token)match(input,74,FOLLOW_74_in_instanciation685);  
			stream_74.add(char_literal29);

			// AST REWRITE
			// elements: IDCLASS, args
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 137:3: -> ^( INSTANTIATE IDCLASS args )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:137:6: ^( INSTANTIATE IDCLASS args )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(INSTANTIATE, "INSTANTIATE"), root_1);
				adaptor.addChild(root_1, stream_IDCLASS.nextNode());
				adaptor.addChild(root_1, stream_args.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "instanciation"


	public static class identificateur_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "identificateur"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:140:1: identificateur : ( 'this' -> ^( THIS ) | 'super' -> ^( SUPER ) | 'result' -> ^( RESULT ) | ID ^);
	public final BloodParser.identificateur_return identificateur() throws RecognitionException {
		BloodParser.identificateur_return retval = new BloodParser.identificateur_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal30=null;
		Token string_literal31=null;
		Token string_literal32=null;
		Token ID33=null;

		Object string_literal30_tree=null;
		Object string_literal31_tree=null;
		Object string_literal32_tree=null;
		Object ID33_tree=null;
		RewriteRuleTokenStream stream_104=new RewriteRuleTokenStream(adaptor,"token 104");
		RewriteRuleTokenStream stream_107=new RewriteRuleTokenStream(adaptor,"token 107");
		RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:141:2: ( 'this' -> ^( THIS ) | 'super' -> ^( SUPER ) | 'result' -> ^( RESULT ) | ID ^)
			int alt12=4;
			switch ( input.LA(1) ) {
			case 109:
				{
				alt12=1;
				}
				break;
			case 107:
				{
				alt12=2;
				}
				break;
			case 104:
				{
				alt12=3;
				}
				break;
			case ID:
				{
				alt12=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 12, 0, input);
				throw nvae;
			}
			switch (alt12) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:141:4: 'this'
					{
					string_literal30=(Token)match(input,109,FOLLOW_109_in_identificateur709);  
					stream_109.add(string_literal30);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 141:11: -> ^( THIS )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:141:14: ^( THIS )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(THIS, "THIS"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:142:4: 'super'
					{
					string_literal31=(Token)match(input,107,FOLLOW_107_in_identificateur720);  
					stream_107.add(string_literal31);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 142:12: -> ^( SUPER )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:142:15: ^( SUPER )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SUPER, "SUPER"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:143:4: 'result'
					{
					string_literal32=(Token)match(input,104,FOLLOW_104_in_identificateur731);  
					stream_104.add(string_literal32);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 143:13: -> ^( RESULT )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:143:16: ^( RESULT )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RESULT, "RESULT"), root_1);
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:144:4: ID ^
					{
					root_0 = (Object)adaptor.nil();


					ID33=(Token)match(input,ID,FOLLOW_ID_in_identificateur742); 
					ID33_tree = (Object)adaptor.create(ID33);
					root_0 = (Object)adaptor.becomeRoot(ID33_tree, root_0);

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "identificateur"


	public static class defClasse_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "defClasse"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:147:1: defClasse : 'class' IDCLASS '(' paramsClasse ')' ( 'extends' IDCLASS )? 'is' '{' ( defClasse2 )+ '}' -> ^( CLASSDEF IDCLASS paramsClasse ( ^( SUPERCLASS IDCLASS ) )? ^( BODY_CLASS ( defClasse2 )+ ) ) ;
	public final BloodParser.defClasse_return defClasse() throws RecognitionException {
		BloodParser.defClasse_return retval = new BloodParser.defClasse_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal34=null;
		Token IDCLASS35=null;
		Token char_literal36=null;
		Token char_literal38=null;
		Token string_literal39=null;
		Token IDCLASS40=null;
		Token string_literal41=null;
		Token char_literal42=null;
		Token char_literal44=null;
		ParserRuleReturnScope paramsClasse37 =null;
		ParserRuleReturnScope defClasse243 =null;

		Object string_literal34_tree=null;
		Object IDCLASS35_tree=null;
		Object char_literal36_tree=null;
		Object char_literal38_tree=null;
		Object string_literal39_tree=null;
		Object IDCLASS40_tree=null;
		Object string_literal41_tree=null;
		Object char_literal42_tree=null;
		Object char_literal44_tree=null;
		RewriteRuleTokenStream stream_100=new RewriteRuleTokenStream(adaptor,"token 100");
		RewriteRuleTokenStream stream_113=new RewriteRuleTokenStream(adaptor,"token 113");
		RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_94=new RewriteRuleTokenStream(adaptor,"token 94");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleTokenStream stream_98=new RewriteRuleTokenStream(adaptor,"token 98");
		RewriteRuleSubtreeStream stream_paramsClasse=new RewriteRuleSubtreeStream(adaptor,"rule paramsClasse");
		RewriteRuleSubtreeStream stream_defClasse2=new RewriteRuleSubtreeStream(adaptor,"rule defClasse2");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:148:2: ( 'class' IDCLASS '(' paramsClasse ')' ( 'extends' IDCLASS )? 'is' '{' ( defClasse2 )+ '}' -> ^( CLASSDEF IDCLASS paramsClasse ( ^( SUPERCLASS IDCLASS ) )? ^( BODY_CLASS ( defClasse2 )+ ) ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:148:4: 'class' IDCLASS '(' paramsClasse ')' ( 'extends' IDCLASS )? 'is' '{' ( defClasse2 )+ '}'
			{
			string_literal34=(Token)match(input,94,FOLLOW_94_in_defClasse754);  
			stream_94.add(string_literal34);

			IDCLASS35=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_defClasse756);  
			stream_IDCLASS.add(IDCLASS35);

			char_literal36=(Token)match(input,73,FOLLOW_73_in_defClasse758);  
			stream_73.add(char_literal36);

			pushFollow(FOLLOW_paramsClasse_in_defClasse760);
			paramsClasse37=paramsClasse();
			state._fsp--;

			stream_paramsClasse.add(paramsClasse37.getTree());
			char_literal38=(Token)match(input,74,FOLLOW_74_in_defClasse762);  
			stream_74.add(char_literal38);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:148:41: ( 'extends' IDCLASS )?
			int alt13=2;
			int LA13_0 = input.LA(1);
			if ( (LA13_0==98) ) {
				alt13=1;
			}
			switch (alt13) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:148:42: 'extends' IDCLASS
					{
					string_literal39=(Token)match(input,98,FOLLOW_98_in_defClasse765);  
					stream_98.add(string_literal39);

					IDCLASS40=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_defClasse767);  
					stream_IDCLASS.add(IDCLASS40);

					}
					break;

			}

			string_literal41=(Token)match(input,100,FOLLOW_100_in_defClasse771);  
			stream_100.add(string_literal41);

			char_literal42=(Token)match(input,113,FOLLOW_113_in_defClasse773);  
			stream_113.add(char_literal42);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:148:71: ( defClasse2 )+
			int cnt14=0;
			loop14:
			while (true) {
				int alt14=2;
				int LA14_0 = input.LA(1);
				if ( (LA14_0==95||LA14_0==110) ) {
					alt14=1;
				}

				switch (alt14) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:148:71: defClasse2
					{
					pushFollow(FOLLOW_defClasse2_in_defClasse775);
					defClasse243=defClasse2();
					state._fsp--;

					stream_defClasse2.add(defClasse243.getTree());
					}
					break;

				default :
					if ( cnt14 >= 1 ) break loop14;
					EarlyExitException eee = new EarlyExitException(14, input);
					throw eee;
				}
				cnt14++;
			}

			char_literal44=(Token)match(input,114,FOLLOW_114_in_defClasse778);  
			stream_114.add(char_literal44);

			// AST REWRITE
			// elements: IDCLASS, defClasse2, paramsClasse, IDCLASS
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 149:3: -> ^( CLASSDEF IDCLASS paramsClasse ( ^( SUPERCLASS IDCLASS ) )? ^( BODY_CLASS ( defClasse2 )+ ) )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:149:6: ^( CLASSDEF IDCLASS paramsClasse ( ^( SUPERCLASS IDCLASS ) )? ^( BODY_CLASS ( defClasse2 )+ ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLASSDEF, "CLASSDEF"), root_1);
				adaptor.addChild(root_1, stream_IDCLASS.nextNode());
				adaptor.addChild(root_1, stream_paramsClasse.nextTree());
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:149:38: ( ^( SUPERCLASS IDCLASS ) )?
				if ( stream_IDCLASS.hasNext() ) {
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:149:38: ^( SUPERCLASS IDCLASS )
					{
					Object root_2 = (Object)adaptor.nil();
					root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(SUPERCLASS, "SUPERCLASS"), root_2);
					adaptor.addChild(root_2, stream_IDCLASS.nextNode());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_IDCLASS.reset();

				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:149:61: ^( BODY_CLASS ( defClasse2 )+ )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(BODY_CLASS, "BODY_CLASS"), root_2);
				if ( !(stream_defClasse2.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_defClasse2.hasNext() ) {
					adaptor.addChild(root_2, stream_defClasse2.nextTree());
				}
				stream_defClasse2.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "defClasse"


	public static class defClasse2_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "defClasse2"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:152:1: defClasse2 : ( defAttr ^| 'def' ! ( defMethode ^| defConstructeur ^) );
	public final BloodParser.defClasse2_return defClasse2() throws RecognitionException {
		BloodParser.defClasse2_return retval = new BloodParser.defClasse2_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal46=null;
		ParserRuleReturnScope defAttr45 =null;
		ParserRuleReturnScope defMethode47 =null;
		ParserRuleReturnScope defConstructeur48 =null;

		Object string_literal46_tree=null;

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:153:2: ( defAttr ^| 'def' ! ( defMethode ^| defConstructeur ^) )
			int alt16=2;
			int LA16_0 = input.LA(1);
			if ( (LA16_0==110) ) {
				alt16=1;
			}
			else if ( (LA16_0==95) ) {
				alt16=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 16, 0, input);
				throw nvae;
			}

			switch (alt16) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:153:4: defAttr ^
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_defAttr_in_defClasse2815);
					defAttr45=defAttr();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(defAttr45.getTree(), root_0);
					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:154:4: 'def' ! ( defMethode ^| defConstructeur ^)
					{
					root_0 = (Object)adaptor.nil();


					string_literal46=(Token)match(input,95,FOLLOW_95_in_defClasse2821); 
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:154:11: ( defMethode ^| defConstructeur ^)
					int alt15=2;
					int LA15_0 = input.LA(1);
					if ( (LA15_0==ID||LA15_0==103||LA15_0==106) ) {
						alt15=1;
					}
					else if ( (LA15_0==IDCLASS) ) {
						alt15=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 15, 0, input);
						throw nvae;
					}

					switch (alt15) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:154:12: defMethode ^
							{
							pushFollow(FOLLOW_defMethode_in_defClasse2825);
							defMethode47=defMethode();
							state._fsp--;

							root_0 = (Object)adaptor.becomeRoot(defMethode47.getTree(), root_0);
							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:154:26: defConstructeur ^
							{
							pushFollow(FOLLOW_defConstructeur_in_defClasse2830);
							defConstructeur48=defConstructeur();
							state._fsp--;

							root_0 = (Object)adaptor.becomeRoot(defConstructeur48.getTree(), root_0);
							}
							break;

					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "defClasse2"


	public static class defConstructeur_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "defConstructeur"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:157:1: defConstructeur : a= IDCLASS '(' paramsConstructor ')' ( ':' b= IDCLASS '(' args ')' )? 'is' bloc -> ^( DEF_CONSTRUCTOR $a paramsConstructor ( ^( SUPERCLASS_INIT $b args ) )? ^( BODY_CONSTRUCTOR ( bloc )? ) ) ;
	public final BloodParser.defConstructeur_return defConstructeur() throws RecognitionException {
		BloodParser.defConstructeur_return retval = new BloodParser.defConstructeur_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token a=null;
		Token b=null;
		Token char_literal49=null;
		Token char_literal51=null;
		Token char_literal52=null;
		Token char_literal53=null;
		Token char_literal55=null;
		Token string_literal56=null;
		ParserRuleReturnScope paramsConstructor50 =null;
		ParserRuleReturnScope args54 =null;
		ParserRuleReturnScope bloc57 =null;

		Object a_tree=null;
		Object b_tree=null;
		Object char_literal49_tree=null;
		Object char_literal51_tree=null;
		Object char_literal52_tree=null;
		Object char_literal53_tree=null;
		Object char_literal55_tree=null;
		Object string_literal56_tree=null;
		RewriteRuleTokenStream stream_100=new RewriteRuleTokenStream(adaptor,"token 100");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleSubtreeStream stream_args=new RewriteRuleSubtreeStream(adaptor,"rule args");
		RewriteRuleSubtreeStream stream_bloc=new RewriteRuleSubtreeStream(adaptor,"rule bloc");
		RewriteRuleSubtreeStream stream_paramsConstructor=new RewriteRuleSubtreeStream(adaptor,"rule paramsConstructor");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:158:2: (a= IDCLASS '(' paramsConstructor ')' ( ':' b= IDCLASS '(' args ')' )? 'is' bloc -> ^( DEF_CONSTRUCTOR $a paramsConstructor ( ^( SUPERCLASS_INIT $b args ) )? ^( BODY_CONSTRUCTOR ( bloc )? ) ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:158:4: a= IDCLASS '(' paramsConstructor ')' ( ':' b= IDCLASS '(' args ')' )? 'is' bloc
			{
			a=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_defConstructeur845);  
			stream_IDCLASS.add(a);

			char_literal49=(Token)match(input,73,FOLLOW_73_in_defConstructeur847);  
			stream_73.add(char_literal49);

			pushFollow(FOLLOW_paramsConstructor_in_defConstructeur849);
			paramsConstructor50=paramsConstructor();
			state._fsp--;

			stream_paramsConstructor.add(paramsConstructor50.getTree());
			char_literal51=(Token)match(input,74,FOLLOW_74_in_defConstructeur851);  
			stream_74.add(char_literal51);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:158:40: ( ':' b= IDCLASS '(' args ')' )?
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0==81) ) {
				alt17=1;
			}
			switch (alt17) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:158:41: ':' b= IDCLASS '(' args ')'
					{
					char_literal52=(Token)match(input,81,FOLLOW_81_in_defConstructeur854);  
					stream_81.add(char_literal52);

					b=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_defConstructeur858);  
					stream_IDCLASS.add(b);

					char_literal53=(Token)match(input,73,FOLLOW_73_in_defConstructeur860);  
					stream_73.add(char_literal53);

					pushFollow(FOLLOW_args_in_defConstructeur862);
					args54=args();
					state._fsp--;

					stream_args.add(args54.getTree());
					char_literal55=(Token)match(input,74,FOLLOW_74_in_defConstructeur864);  
					stream_74.add(char_literal55);

					}
					break;

			}

			string_literal56=(Token)match(input,100,FOLLOW_100_in_defConstructeur868);  
			stream_100.add(string_literal56);

			pushFollow(FOLLOW_bloc_in_defConstructeur870);
			bloc57=bloc();
			state._fsp--;

			stream_bloc.add(bloc57.getTree());
			// AST REWRITE
			// elements: args, b, bloc, a, paramsConstructor
			// token labels: a, b
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleTokenStream stream_a=new RewriteRuleTokenStream(adaptor,"token a",a);
			RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 159:3: -> ^( DEF_CONSTRUCTOR $a paramsConstructor ( ^( SUPERCLASS_INIT $b args ) )? ^( BODY_CONSTRUCTOR ( bloc )? ) )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:159:6: ^( DEF_CONSTRUCTOR $a paramsConstructor ( ^( SUPERCLASS_INIT $b args ) )? ^( BODY_CONSTRUCTOR ( bloc )? ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DEF_CONSTRUCTOR, "DEF_CONSTRUCTOR"), root_1);
				adaptor.addChild(root_1, stream_a.nextNode());
				adaptor.addChild(root_1, stream_paramsConstructor.nextTree());
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:159:45: ( ^( SUPERCLASS_INIT $b args ) )?
				if ( stream_args.hasNext()||stream_b.hasNext() ) {
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:159:45: ^( SUPERCLASS_INIT $b args )
					{
					Object root_2 = (Object)adaptor.nil();
					root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(SUPERCLASS_INIT, "SUPERCLASS_INIT"), root_2);
					adaptor.addChild(root_2, stream_b.nextNode());
					adaptor.addChild(root_2, stream_args.nextTree());
					adaptor.addChild(root_1, root_2);
					}

				}
				stream_args.reset();
				stream_b.reset();

				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:159:73: ^( BODY_CONSTRUCTOR ( bloc )? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(BODY_CONSTRUCTOR, "BODY_CONSTRUCTOR"), root_2);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:159:92: ( bloc )?
				if ( stream_bloc.hasNext() ) {
					adaptor.addChild(root_2, stream_bloc.nextTree());
				}
				stream_bloc.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "defConstructeur"


	public static class defMethode_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "defMethode"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:162:1: defMethode : ( methodeProps )? ID '(' paramsMethode ')' ( ( ':' IDCLASS ( ':=' expression | 'is' bloc ) ) | 'is' bloc ) -> ^( DEF_METHOD ^( METHOD_PROPS ( methodeProps )? ) ID paramsMethode ( IDCLASS )? ^( BODY_METHOD ( expression )? ( ^( BLOCK bloc ) )? ) ) ;
	public final BloodParser.defMethode_return defMethode() throws RecognitionException {
		BloodParser.defMethode_return retval = new BloodParser.defMethode_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID59=null;
		Token char_literal60=null;
		Token char_literal62=null;
		Token char_literal63=null;
		Token IDCLASS64=null;
		Token string_literal65=null;
		Token string_literal67=null;
		Token string_literal69=null;
		ParserRuleReturnScope methodeProps58 =null;
		ParserRuleReturnScope paramsMethode61 =null;
		ParserRuleReturnScope expression66 =null;
		ParserRuleReturnScope bloc68 =null;
		ParserRuleReturnScope bloc70 =null;

		Object ID59_tree=null;
		Object char_literal60_tree=null;
		Object char_literal62_tree=null;
		Object char_literal63_tree=null;
		Object IDCLASS64_tree=null;
		Object string_literal65_tree=null;
		Object string_literal67_tree=null;
		Object string_literal69_tree=null;
		RewriteRuleTokenStream stream_100=new RewriteRuleTokenStream(adaptor,"token 100");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_82=new RewriteRuleTokenStream(adaptor,"token 82");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleSubtreeStream stream_bloc=new RewriteRuleSubtreeStream(adaptor,"rule bloc");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_methodeProps=new RewriteRuleSubtreeStream(adaptor,"rule methodeProps");
		RewriteRuleSubtreeStream stream_paramsMethode=new RewriteRuleSubtreeStream(adaptor,"rule paramsMethode");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:2: ( ( methodeProps )? ID '(' paramsMethode ')' ( ( ':' IDCLASS ( ':=' expression | 'is' bloc ) ) | 'is' bloc ) -> ^( DEF_METHOD ^( METHOD_PROPS ( methodeProps )? ) ID paramsMethode ( IDCLASS )? ^( BODY_METHOD ( expression )? ( ^( BLOCK bloc ) )? ) ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:4: ( methodeProps )? ID '(' paramsMethode ')' ( ( ':' IDCLASS ( ':=' expression | 'is' bloc ) ) | 'is' bloc )
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:4: ( methodeProps )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==103||LA18_0==106) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:4: methodeProps
					{
					pushFollow(FOLLOW_methodeProps_in_defMethode911);
					methodeProps58=methodeProps();
					state._fsp--;

					stream_methodeProps.add(methodeProps58.getTree());
					}
					break;

			}

			ID59=(Token)match(input,ID,FOLLOW_ID_in_defMethode914);  
			stream_ID.add(ID59);

			char_literal60=(Token)match(input,73,FOLLOW_73_in_defMethode916);  
			stream_73.add(char_literal60);

			pushFollow(FOLLOW_paramsMethode_in_defMethode918);
			paramsMethode61=paramsMethode();
			state._fsp--;

			stream_paramsMethode.add(paramsMethode61.getTree());
			char_literal62=(Token)match(input,74,FOLLOW_74_in_defMethode920);  
			stream_74.add(char_literal62);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:43: ( ( ':' IDCLASS ( ':=' expression | 'is' bloc ) ) | 'is' bloc )
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==81) ) {
				alt20=1;
			}
			else if ( (LA20_0==100) ) {
				alt20=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 20, 0, input);
				throw nvae;
			}

			switch (alt20) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:44: ( ':' IDCLASS ( ':=' expression | 'is' bloc ) )
					{
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:44: ( ':' IDCLASS ( ':=' expression | 'is' bloc ) )
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:45: ':' IDCLASS ( ':=' expression | 'is' bloc )
					{
					char_literal63=(Token)match(input,81,FOLLOW_81_in_defMethode924);  
					stream_81.add(char_literal63);

					IDCLASS64=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_defMethode926);  
					stream_IDCLASS.add(IDCLASS64);

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:57: ( ':=' expression | 'is' bloc )
					int alt19=2;
					int LA19_0 = input.LA(1);
					if ( (LA19_0==82) ) {
						alt19=1;
					}
					else if ( (LA19_0==100) ) {
						alt19=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 19, 0, input);
						throw nvae;
					}

					switch (alt19) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:58: ':=' expression
							{
							string_literal65=(Token)match(input,82,FOLLOW_82_in_defMethode929);  
							stream_82.add(string_literal65);

							pushFollow(FOLLOW_expression_in_defMethode931);
							expression66=expression();
							state._fsp--;

							stream_expression.add(expression66.getTree());
							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:76: 'is' bloc
							{
							string_literal67=(Token)match(input,100,FOLLOW_100_in_defMethode935);  
							stream_100.add(string_literal67);

							pushFollow(FOLLOW_bloc_in_defMethode937);
							bloc68=bloc();
							state._fsp--;

							stream_bloc.add(bloc68.getTree());
							}
							break;

					}

					}

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:163:90: 'is' bloc
					{
					string_literal69=(Token)match(input,100,FOLLOW_100_in_defMethode943);  
					stream_100.add(string_literal69);

					pushFollow(FOLLOW_bloc_in_defMethode945);
					bloc70=bloc();
					state._fsp--;

					stream_bloc.add(bloc70.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: ID, IDCLASS, methodeProps, bloc, expression, paramsMethode
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 164:3: -> ^( DEF_METHOD ^( METHOD_PROPS ( methodeProps )? ) ID paramsMethode ( IDCLASS )? ^( BODY_METHOD ( expression )? ( ^( BLOCK bloc ) )? ) )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:6: ^( DEF_METHOD ^( METHOD_PROPS ( methodeProps )? ) ID paramsMethode ( IDCLASS )? ^( BODY_METHOD ( expression )? ( ^( BLOCK bloc ) )? ) )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DEF_METHOD, "DEF_METHOD"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:19: ^( METHOD_PROPS ( methodeProps )? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(METHOD_PROPS, "METHOD_PROPS"), root_2);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:34: ( methodeProps )?
				if ( stream_methodeProps.hasNext() ) {
					adaptor.addChild(root_2, stream_methodeProps.nextTree());
				}
				stream_methodeProps.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_1, stream_paramsMethode.nextTree());
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:66: ( IDCLASS )?
				if ( stream_IDCLASS.hasNext() ) {
					adaptor.addChild(root_1, stream_IDCLASS.nextNode());
				}
				stream_IDCLASS.reset();

				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:75: ^( BODY_METHOD ( expression )? ( ^( BLOCK bloc ) )? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(BODY_METHOD, "BODY_METHOD"), root_2);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:89: ( expression )?
				if ( stream_expression.hasNext() ) {
					adaptor.addChild(root_2, stream_expression.nextTree());
				}
				stream_expression.reset();

				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:101: ( ^( BLOCK bloc ) )?
				if ( stream_bloc.hasNext() ) {
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:164:101: ^( BLOCK bloc )
					{
					Object root_3 = (Object)adaptor.nil();
					root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(BLOCK, "BLOCK"), root_3);
					adaptor.addChild(root_3, stream_bloc.nextTree());
					adaptor.addChild(root_2, root_3);
					}

				}
				stream_bloc.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "defMethode"


	public static class methodeProps_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "methodeProps"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:167:1: methodeProps : ( 'static' -> STATIC | 'override' -> OVERRIDE );
	public final BloodParser.methodeProps_return methodeProps() throws RecognitionException {
		BloodParser.methodeProps_return retval = new BloodParser.methodeProps_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal71=null;
		Token string_literal72=null;

		Object string_literal71_tree=null;
		Object string_literal72_tree=null;
		RewriteRuleTokenStream stream_103=new RewriteRuleTokenStream(adaptor,"token 103");
		RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:168:2: ( 'static' -> STATIC | 'override' -> OVERRIDE )
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==106) ) {
				alt21=1;
			}
			else if ( (LA21_0==103) ) {
				alt21=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}

			switch (alt21) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:168:4: 'static'
					{
					string_literal71=(Token)match(input,106,FOLLOW_106_in_methodeProps994);  
					stream_106.add(string_literal71);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 168:13: -> STATIC
					{
						adaptor.addChild(root_0, (Object)adaptor.create(STATIC, "STATIC"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:169:4: 'override'
					{
					string_literal72=(Token)match(input,103,FOLLOW_103_in_methodeProps1003);  
					stream_103.add(string_literal72);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 169:15: -> OVERRIDE
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OVERRIDE, "OVERRIDE"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodeProps"


	public static class defAttr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "defAttr"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:1: defAttr : 'var' ( attrProps )? ID ':' IDCLASS ( ';' | ':=' expression ';' ) -> ^( DEF_ATTRIBUTE ^( ATTR_PROPS ( attrProps )? ) ID IDCLASS ( expression )? ) ;
	public final BloodParser.defAttr_return defAttr() throws RecognitionException {
		BloodParser.defAttr_return retval = new BloodParser.defAttr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal73=null;
		Token ID75=null;
		Token char_literal76=null;
		Token IDCLASS77=null;
		Token char_literal78=null;
		Token string_literal79=null;
		Token char_literal81=null;
		ParserRuleReturnScope attrProps74 =null;
		ParserRuleReturnScope expression80 =null;

		Object string_literal73_tree=null;
		Object ID75_tree=null;
		Object char_literal76_tree=null;
		Object IDCLASS77_tree=null;
		Object char_literal78_tree=null;
		Object string_literal79_tree=null;
		Object char_literal81_tree=null;
		RewriteRuleTokenStream stream_110=new RewriteRuleTokenStream(adaptor,"token 110");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_82=new RewriteRuleTokenStream(adaptor,"token 82");
		RewriteRuleTokenStream stream_83=new RewriteRuleTokenStream(adaptor,"token 83");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleSubtreeStream stream_attrProps=new RewriteRuleSubtreeStream(adaptor,"rule attrProps");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:9: ( 'var' ( attrProps )? ID ':' IDCLASS ( ';' | ':=' expression ';' ) -> ^( DEF_ATTRIBUTE ^( ATTR_PROPS ( attrProps )? ) ID IDCLASS ( expression )? ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:11: 'var' ( attrProps )? ID ':' IDCLASS ( ';' | ':=' expression ';' )
			{
			string_literal73=(Token)match(input,110,FOLLOW_110_in_defAttr1017);  
			stream_110.add(string_literal73);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:17: ( attrProps )?
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==106) ) {
				alt22=1;
			}
			switch (alt22) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:17: attrProps
					{
					pushFollow(FOLLOW_attrProps_in_defAttr1019);
					attrProps74=attrProps();
					state._fsp--;

					stream_attrProps.add(attrProps74.getTree());
					}
					break;

			}

			ID75=(Token)match(input,ID,FOLLOW_ID_in_defAttr1022);  
			stream_ID.add(ID75);

			char_literal76=(Token)match(input,81,FOLLOW_81_in_defAttr1024);  
			stream_81.add(char_literal76);

			IDCLASS77=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_defAttr1026);  
			stream_IDCLASS.add(IDCLASS77);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:43: ( ';' | ':=' expression ';' )
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==83) ) {
				alt23=1;
			}
			else if ( (LA23_0==82) ) {
				alt23=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 23, 0, input);
				throw nvae;
			}

			switch (alt23) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:44: ';'
					{
					char_literal78=(Token)match(input,83,FOLLOW_83_in_defAttr1029);  
					stream_83.add(char_literal78);

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:172:50: ':=' expression ';'
					{
					string_literal79=(Token)match(input,82,FOLLOW_82_in_defAttr1033);  
					stream_82.add(string_literal79);

					pushFollow(FOLLOW_expression_in_defAttr1035);
					expression80=expression();
					state._fsp--;

					stream_expression.add(expression80.getTree());
					char_literal81=(Token)match(input,83,FOLLOW_83_in_defAttr1037);  
					stream_83.add(char_literal81);

					}
					break;

			}

			// AST REWRITE
			// elements: attrProps, ID, expression, IDCLASS
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 173:3: -> ^( DEF_ATTRIBUTE ^( ATTR_PROPS ( attrProps )? ) ID IDCLASS ( expression )? )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:173:6: ^( DEF_ATTRIBUTE ^( ATTR_PROPS ( attrProps )? ) ID IDCLASS ( expression )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DEF_ATTRIBUTE, "DEF_ATTRIBUTE"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:173:22: ^( ATTR_PROPS ( attrProps )? )
				{
				Object root_2 = (Object)adaptor.nil();
				root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(ATTR_PROPS, "ATTR_PROPS"), root_2);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:173:35: ( attrProps )?
				if ( stream_attrProps.hasNext() ) {
					adaptor.addChild(root_2, stream_attrProps.nextTree());
				}
				stream_attrProps.reset();

				adaptor.addChild(root_1, root_2);
				}

				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_1, stream_IDCLASS.nextNode());
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:173:58: ( expression )?
				if ( stream_expression.hasNext() ) {
					adaptor.addChild(root_1, stream_expression.nextTree());
				}
				stream_expression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "defAttr"


	public static class attrProps_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attrProps"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:176:1: attrProps : 'static' -> STATIC ;
	public final BloodParser.attrProps_return attrProps() throws RecognitionException {
		BloodParser.attrProps_return retval = new BloodParser.attrProps_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal82=null;

		Object string_literal82_tree=null;
		RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:177:2: ( 'static' -> STATIC )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:177:4: 'static'
			{
			string_literal82=(Token)match(input,106,FOLLOW_106_in_attrProps1073);  
			stream_106.add(string_literal82);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 177:13: -> STATIC
			{
				adaptor.addChild(root_0, (Object)adaptor.create(STATIC, "STATIC"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attrProps"


	public static class paramsClasse_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "paramsClasse"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:181:1: paramsClasse : ( paramClasse ( ',' paramClasse )* )? -> ^( PARAMS_CLASS ( paramClasse )* ) ;
	public final BloodParser.paramsClasse_return paramsClasse() throws RecognitionException {
		BloodParser.paramsClasse_return retval = new BloodParser.paramsClasse_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal84=null;
		ParserRuleReturnScope paramClasse83 =null;
		ParserRuleReturnScope paramClasse85 =null;

		Object char_literal84_tree=null;
		RewriteRuleTokenStream stream_77=new RewriteRuleTokenStream(adaptor,"token 77");
		RewriteRuleSubtreeStream stream_paramClasse=new RewriteRuleSubtreeStream(adaptor,"rule paramClasse");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:182:2: ( ( paramClasse ( ',' paramClasse )* )? -> ^( PARAMS_CLASS ( paramClasse )* ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:182:4: ( paramClasse ( ',' paramClasse )* )?
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:182:4: ( paramClasse ( ',' paramClasse )* )?
			int alt25=2;
			int LA25_0 = input.LA(1);
			if ( (LA25_0==ID||LA25_0==110) ) {
				alt25=1;
			}
			switch (alt25) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:182:5: paramClasse ( ',' paramClasse )*
					{
					pushFollow(FOLLOW_paramClasse_in_paramsClasse1090);
					paramClasse83=paramClasse();
					state._fsp--;

					stream_paramClasse.add(paramClasse83.getTree());
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:182:17: ( ',' paramClasse )*
					loop24:
					while (true) {
						int alt24=2;
						int LA24_0 = input.LA(1);
						if ( (LA24_0==77) ) {
							alt24=1;
						}

						switch (alt24) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:182:18: ',' paramClasse
							{
							char_literal84=(Token)match(input,77,FOLLOW_77_in_paramsClasse1093);  
							stream_77.add(char_literal84);

							pushFollow(FOLLOW_paramClasse_in_paramsClasse1095);
							paramClasse85=paramClasse();
							state._fsp--;

							stream_paramClasse.add(paramClasse85.getTree());
							}
							break;

						default :
							break loop24;
						}
					}

					}
					break;

			}

			// AST REWRITE
			// elements: paramClasse
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 183:3: -> ^( PARAMS_CLASS ( paramClasse )* )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:183:6: ^( PARAMS_CLASS ( paramClasse )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAMS_CLASS, "PARAMS_CLASS"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:183:21: ( paramClasse )*
				while ( stream_paramClasse.hasNext() ) {
					adaptor.addChild(root_1, stream_paramClasse.nextTree());
				}
				stream_paramClasse.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paramsClasse"


	public static class paramClasse_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "paramClasse"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:186:1: paramClasse : ( 'var' ID ':' IDCLASS -> ^( PARAM_CLASS VAR ID IDCLASS ) | ID ':' IDCLASS -> ^( PARAM_CLASS ID IDCLASS ) );
	public final BloodParser.paramClasse_return paramClasse() throws RecognitionException {
		BloodParser.paramClasse_return retval = new BloodParser.paramClasse_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal86=null;
		Token ID87=null;
		Token char_literal88=null;
		Token IDCLASS89=null;
		Token ID90=null;
		Token char_literal91=null;
		Token IDCLASS92=null;

		Object string_literal86_tree=null;
		Object ID87_tree=null;
		Object char_literal88_tree=null;
		Object IDCLASS89_tree=null;
		Object ID90_tree=null;
		Object char_literal91_tree=null;
		Object IDCLASS92_tree=null;
		RewriteRuleTokenStream stream_110=new RewriteRuleTokenStream(adaptor,"token 110");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:187:2: ( 'var' ID ':' IDCLASS -> ^( PARAM_CLASS VAR ID IDCLASS ) | ID ':' IDCLASS -> ^( PARAM_CLASS ID IDCLASS ) )
			int alt26=2;
			int LA26_0 = input.LA(1);
			if ( (LA26_0==110) ) {
				alt26=1;
			}
			else if ( (LA26_0==ID) ) {
				alt26=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}

			switch (alt26) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:187:4: 'var' ID ':' IDCLASS
					{
					string_literal86=(Token)match(input,110,FOLLOW_110_in_paramClasse1122);  
					stream_110.add(string_literal86);

					ID87=(Token)match(input,ID,FOLLOW_ID_in_paramClasse1124);  
					stream_ID.add(ID87);

					char_literal88=(Token)match(input,81,FOLLOW_81_in_paramClasse1126);  
					stream_81.add(char_literal88);

					IDCLASS89=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_paramClasse1128);  
					stream_IDCLASS.add(IDCLASS89);

					// AST REWRITE
					// elements: IDCLASS, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 188:3: -> ^( PARAM_CLASS VAR ID IDCLASS )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:188:6: ^( PARAM_CLASS VAR ID IDCLASS )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAM_CLASS, "PARAM_CLASS"), root_1);
						adaptor.addChild(root_1, (Object)adaptor.create(VAR, "VAR"));
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_1, stream_IDCLASS.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:189:4: ID ':' IDCLASS
					{
					ID90=(Token)match(input,ID,FOLLOW_ID_in_paramClasse1147);  
					stream_ID.add(ID90);

					char_literal91=(Token)match(input,81,FOLLOW_81_in_paramClasse1149);  
					stream_81.add(char_literal91);

					IDCLASS92=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_paramClasse1151);  
					stream_IDCLASS.add(IDCLASS92);

					// AST REWRITE
					// elements: IDCLASS, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 190:3: -> ^( PARAM_CLASS ID IDCLASS )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:190:6: ^( PARAM_CLASS ID IDCLASS )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAM_CLASS, "PARAM_CLASS"), root_1);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_1, stream_IDCLASS.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paramClasse"


	public static class paramsConstructor_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "paramsConstructor"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:193:1: paramsConstructor : ( paramConstructor ( ',' paramConstructor )* )? -> ^( PARAMS_CONSTRUCTOR ( paramConstructor )* ) ;
	public final BloodParser.paramsConstructor_return paramsConstructor() throws RecognitionException {
		BloodParser.paramsConstructor_return retval = new BloodParser.paramsConstructor_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal94=null;
		ParserRuleReturnScope paramConstructor93 =null;
		ParserRuleReturnScope paramConstructor95 =null;

		Object char_literal94_tree=null;
		RewriteRuleTokenStream stream_77=new RewriteRuleTokenStream(adaptor,"token 77");
		RewriteRuleSubtreeStream stream_paramConstructor=new RewriteRuleSubtreeStream(adaptor,"rule paramConstructor");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:194:2: ( ( paramConstructor ( ',' paramConstructor )* )? -> ^( PARAMS_CONSTRUCTOR ( paramConstructor )* ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:194:4: ( paramConstructor ( ',' paramConstructor )* )?
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:194:4: ( paramConstructor ( ',' paramConstructor )* )?
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0==ID||LA28_0==110) ) {
				alt28=1;
			}
			switch (alt28) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:194:5: paramConstructor ( ',' paramConstructor )*
					{
					pushFollow(FOLLOW_paramConstructor_in_paramsConstructor1176);
					paramConstructor93=paramConstructor();
					state._fsp--;

					stream_paramConstructor.add(paramConstructor93.getTree());
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:194:22: ( ',' paramConstructor )*
					loop27:
					while (true) {
						int alt27=2;
						int LA27_0 = input.LA(1);
						if ( (LA27_0==77) ) {
							alt27=1;
						}

						switch (alt27) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:194:23: ',' paramConstructor
							{
							char_literal94=(Token)match(input,77,FOLLOW_77_in_paramsConstructor1179);  
							stream_77.add(char_literal94);

							pushFollow(FOLLOW_paramConstructor_in_paramsConstructor1181);
							paramConstructor95=paramConstructor();
							state._fsp--;

							stream_paramConstructor.add(paramConstructor95.getTree());
							}
							break;

						default :
							break loop27;
						}
					}

					}
					break;

			}

			// AST REWRITE
			// elements: paramConstructor
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 195:3: -> ^( PARAMS_CONSTRUCTOR ( paramConstructor )* )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:195:6: ^( PARAMS_CONSTRUCTOR ( paramConstructor )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAMS_CONSTRUCTOR, "PARAMS_CONSTRUCTOR"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:195:27: ( paramConstructor )*
				while ( stream_paramConstructor.hasNext() ) {
					adaptor.addChild(root_1, stream_paramConstructor.nextTree());
				}
				stream_paramConstructor.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paramsConstructor"


	public static class paramConstructor_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "paramConstructor"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:198:1: paramConstructor : ( 'var' ID ':' IDCLASS -> ^( PARAM_CONSTRUCTOR VAR ID IDCLASS ) | ID ':' IDCLASS -> ^( PARAM_CONSTRUCTOR ID IDCLASS ) );
	public final BloodParser.paramConstructor_return paramConstructor() throws RecognitionException {
		BloodParser.paramConstructor_return retval = new BloodParser.paramConstructor_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal96=null;
		Token ID97=null;
		Token char_literal98=null;
		Token IDCLASS99=null;
		Token ID100=null;
		Token char_literal101=null;
		Token IDCLASS102=null;

		Object string_literal96_tree=null;
		Object ID97_tree=null;
		Object char_literal98_tree=null;
		Object IDCLASS99_tree=null;
		Object ID100_tree=null;
		Object char_literal101_tree=null;
		Object IDCLASS102_tree=null;
		RewriteRuleTokenStream stream_110=new RewriteRuleTokenStream(adaptor,"token 110");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:199:2: ( 'var' ID ':' IDCLASS -> ^( PARAM_CONSTRUCTOR VAR ID IDCLASS ) | ID ':' IDCLASS -> ^( PARAM_CONSTRUCTOR ID IDCLASS ) )
			int alt29=2;
			int LA29_0 = input.LA(1);
			if ( (LA29_0==110) ) {
				alt29=1;
			}
			else if ( (LA29_0==ID) ) {
				alt29=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 29, 0, input);
				throw nvae;
			}

			switch (alt29) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:199:4: 'var' ID ':' IDCLASS
					{
					string_literal96=(Token)match(input,110,FOLLOW_110_in_paramConstructor1208);  
					stream_110.add(string_literal96);

					ID97=(Token)match(input,ID,FOLLOW_ID_in_paramConstructor1210);  
					stream_ID.add(ID97);

					char_literal98=(Token)match(input,81,FOLLOW_81_in_paramConstructor1212);  
					stream_81.add(char_literal98);

					IDCLASS99=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_paramConstructor1214);  
					stream_IDCLASS.add(IDCLASS99);

					// AST REWRITE
					// elements: ID, IDCLASS
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 200:3: -> ^( PARAM_CONSTRUCTOR VAR ID IDCLASS )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:200:6: ^( PARAM_CONSTRUCTOR VAR ID IDCLASS )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAM_CONSTRUCTOR, "PARAM_CONSTRUCTOR"), root_1);
						adaptor.addChild(root_1, (Object)adaptor.create(VAR, "VAR"));
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_1, stream_IDCLASS.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:201:4: ID ':' IDCLASS
					{
					ID100=(Token)match(input,ID,FOLLOW_ID_in_paramConstructor1233);  
					stream_ID.add(ID100);

					char_literal101=(Token)match(input,81,FOLLOW_81_in_paramConstructor1235);  
					stream_81.add(char_literal101);

					IDCLASS102=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_paramConstructor1237);  
					stream_IDCLASS.add(IDCLASS102);

					// AST REWRITE
					// elements: ID, IDCLASS
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 202:3: -> ^( PARAM_CONSTRUCTOR ID IDCLASS )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:202:6: ^( PARAM_CONSTRUCTOR ID IDCLASS )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAM_CONSTRUCTOR, "PARAM_CONSTRUCTOR"), root_1);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_1, stream_IDCLASS.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paramConstructor"


	public static class paramsMethode_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "paramsMethode"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:205:1: paramsMethode : ( paramMethode ( ',' paramMethode )* )? -> ^( PARAMS_METHOD ( paramMethode )* ) ;
	public final BloodParser.paramsMethode_return paramsMethode() throws RecognitionException {
		BloodParser.paramsMethode_return retval = new BloodParser.paramsMethode_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal104=null;
		ParserRuleReturnScope paramMethode103 =null;
		ParserRuleReturnScope paramMethode105 =null;

		Object char_literal104_tree=null;
		RewriteRuleTokenStream stream_77=new RewriteRuleTokenStream(adaptor,"token 77");
		RewriteRuleSubtreeStream stream_paramMethode=new RewriteRuleSubtreeStream(adaptor,"rule paramMethode");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:206:2: ( ( paramMethode ( ',' paramMethode )* )? -> ^( PARAMS_METHOD ( paramMethode )* ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:206:4: ( paramMethode ( ',' paramMethode )* )?
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:206:4: ( paramMethode ( ',' paramMethode )* )?
			int alt31=2;
			int LA31_0 = input.LA(1);
			if ( (LA31_0==ID) ) {
				alt31=1;
			}
			switch (alt31) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:206:5: paramMethode ( ',' paramMethode )*
					{
					pushFollow(FOLLOW_paramMethode_in_paramsMethode1261);
					paramMethode103=paramMethode();
					state._fsp--;

					stream_paramMethode.add(paramMethode103.getTree());
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:206:18: ( ',' paramMethode )*
					loop30:
					while (true) {
						int alt30=2;
						int LA30_0 = input.LA(1);
						if ( (LA30_0==77) ) {
							alt30=1;
						}

						switch (alt30) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:206:19: ',' paramMethode
							{
							char_literal104=(Token)match(input,77,FOLLOW_77_in_paramsMethode1264);  
							stream_77.add(char_literal104);

							pushFollow(FOLLOW_paramMethode_in_paramsMethode1266);
							paramMethode105=paramMethode();
							state._fsp--;

							stream_paramMethode.add(paramMethode105.getTree());
							}
							break;

						default :
							break loop30;
						}
					}

					}
					break;

			}

			// AST REWRITE
			// elements: paramMethode
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 207:3: -> ^( PARAMS_METHOD ( paramMethode )* )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:207:6: ^( PARAMS_METHOD ( paramMethode )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAMS_METHOD, "PARAMS_METHOD"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:207:22: ( paramMethode )*
				while ( stream_paramMethode.hasNext() ) {
					adaptor.addChild(root_1, stream_paramMethode.nextTree());
				}
				stream_paramMethode.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paramsMethode"


	public static class paramMethode_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "paramMethode"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:210:1: paramMethode : ID ':' IDCLASS -> ^( PARAM_METHOD ID IDCLASS ) ;
	public final BloodParser.paramMethode_return paramMethode() throws RecognitionException {
		BloodParser.paramMethode_return retval = new BloodParser.paramMethode_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID106=null;
		Token char_literal107=null;
		Token IDCLASS108=null;

		Object ID106_tree=null;
		Object char_literal107_tree=null;
		Object IDCLASS108_tree=null;
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:211:2: ( ID ':' IDCLASS -> ^( PARAM_METHOD ID IDCLASS ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:211:4: ID ':' IDCLASS
			{
			ID106=(Token)match(input,ID,FOLLOW_ID_in_paramMethode1293);  
			stream_ID.add(ID106);

			char_literal107=(Token)match(input,81,FOLLOW_81_in_paramMethode1295);  
			stream_81.add(char_literal107);

			IDCLASS108=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_paramMethode1297);  
			stream_IDCLASS.add(IDCLASS108);

			// AST REWRITE
			// elements: IDCLASS, ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 212:3: -> ^( PARAM_METHOD ID IDCLASS )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:212:6: ^( PARAM_METHOD ID IDCLASS )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PARAM_METHOD, "PARAM_METHOD"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_1, stream_IDCLASS.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paramMethode"


	public static class args_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "args"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:215:1: args : ( expression ( ',' expression )* )? -> ^( ARGS ( expression )* ) ;
	public final BloodParser.args_return args() throws RecognitionException {
		BloodParser.args_return retval = new BloodParser.args_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal110=null;
		ParserRuleReturnScope expression109 =null;
		ParserRuleReturnScope expression111 =null;

		Object char_literal110_tree=null;
		RewriteRuleTokenStream stream_77=new RewriteRuleTokenStream(adaptor,"token 77");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:216:2: ( ( expression ( ',' expression )* )? -> ^( ARGS ( expression )* ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:216:4: ( expression ( ',' expression )* )?
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:216:4: ( expression ( ',' expression )* )?
			int alt33=2;
			int LA33_0 = input.LA(1);
			if ( ((LA33_0 >= ID && LA33_0 <= IDCLASS)||LA33_0==INT||LA33_0==STRING||LA33_0==73||LA33_0==76||LA33_0==78||LA33_0==101||LA33_0==104||LA33_0==107||LA33_0==109||LA33_0==115) ) {
				alt33=1;
			}
			switch (alt33) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:216:5: expression ( ',' expression )*
					{
					pushFollow(FOLLOW_expression_in_args1322);
					expression109=expression();
					state._fsp--;

					stream_expression.add(expression109.getTree());
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:216:16: ( ',' expression )*
					loop32:
					while (true) {
						int alt32=2;
						int LA32_0 = input.LA(1);
						if ( (LA32_0==77) ) {
							alt32=1;
						}

						switch (alt32) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:216:17: ',' expression
							{
							char_literal110=(Token)match(input,77,FOLLOW_77_in_args1325);  
							stream_77.add(char_literal110);

							pushFollow(FOLLOW_expression_in_args1327);
							expression111=expression();
							state._fsp--;

							stream_expression.add(expression111.getTree());
							}
							break;

						default :
							break loop32;
						}
					}

					}
					break;

			}

			// AST REWRITE
			// elements: expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 217:3: -> ^( ARGS ( expression )* )
			{
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:217:6: ^( ARGS ( expression )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ARGS, "ARGS"), root_1);
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:217:13: ( expression )*
				while ( stream_expression.hasNext() ) {
					adaptor.addChild(root_1, stream_expression.nextTree());
				}
				stream_expression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "args"


	public static class expression_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:221:1: expression : ( instanciation | exprOp );
	public final BloodParser.expression_return expression() throws RecognitionException {
		BloodParser.expression_return retval = new BloodParser.expression_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope instanciation112 =null;
		ParserRuleReturnScope exprOp113 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:222:2: ( instanciation | exprOp )
			int alt34=2;
			int LA34_0 = input.LA(1);
			if ( (LA34_0==101) ) {
				alt34=1;
			}
			else if ( ((LA34_0 >= ID && LA34_0 <= IDCLASS)||LA34_0==INT||LA34_0==STRING||LA34_0==73||LA34_0==76||LA34_0==78||LA34_0==104||LA34_0==107||LA34_0==109||LA34_0==115) ) {
				alt34=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 34, 0, input);
				throw nvae;
			}

			switch (alt34) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:222:4: instanciation
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_instanciation_in_expression1354);
					instanciation112=instanciation();
					state._fsp--;

					adaptor.addChild(root_0, instanciation112.getTree());

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:223:4: exprOp
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_exprOp_in_expression1359);
					exprOp113=exprOp();
					state._fsp--;

					adaptor.addChild(root_0, exprOp113.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class exprOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:226:1: exprOp : exprOr ;
	public final BloodParser.exprOp_return exprOp() throws RecognitionException {
		BloodParser.exprOp_return retval = new BloodParser.exprOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprOr114 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:226:8: ( exprOr )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:226:10: exprOr
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprOr_in_exprOp1369);
			exprOr114=exprOr();
			state._fsp--;

			adaptor.addChild(root_0, exprOr114.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprOp"


	public static class exprOr_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprOr"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:229:1: exprOr : exprXor ( exprOrOp ^ exprXor )* ;
	public final BloodParser.exprOr_return exprOr() throws RecognitionException {
		BloodParser.exprOr_return retval = new BloodParser.exprOr_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprXor115 =null;
		ParserRuleReturnScope exprOrOp116 =null;
		ParserRuleReturnScope exprXor117 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:229:8: ( exprXor ( exprOrOp ^ exprXor )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:229:10: exprXor ( exprOrOp ^ exprXor )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprXor_in_exprOr1380);
			exprXor115=exprXor();
			state._fsp--;

			adaptor.addChild(root_0, exprXor115.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:229:18: ( exprOrOp ^ exprXor )*
			loop35:
			while (true) {
				int alt35=2;
				int LA35_0 = input.LA(1);
				if ( (LA35_0==102) ) {
					alt35=1;
				}

				switch (alt35) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:229:19: exprOrOp ^ exprXor
					{
					pushFollow(FOLLOW_exprOrOp_in_exprOr1383);
					exprOrOp116=exprOrOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprOrOp116.getTree(), root_0);
					pushFollow(FOLLOW_exprXor_in_exprOr1386);
					exprXor117=exprXor();
					state._fsp--;

					adaptor.addChild(root_0, exprXor117.getTree());

					}
					break;

				default :
					break loop35;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprOr"


	public static class exprOrOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprOrOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:232:1: exprOrOp : 'or' -> OP_OR ;
	public final BloodParser.exprOrOp_return exprOrOp() throws RecognitionException {
		BloodParser.exprOrOp_return retval = new BloodParser.exprOrOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal118=null;

		Object string_literal118_tree=null;
		RewriteRuleTokenStream stream_102=new RewriteRuleTokenStream(adaptor,"token 102");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:232:9: ( 'or' -> OP_OR )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:232:11: 'or'
			{
			string_literal118=(Token)match(input,102,FOLLOW_102_in_exprOrOp1397);  
			stream_102.add(string_literal118);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 232:16: -> OP_OR
			{
				adaptor.addChild(root_0, (Object)adaptor.create(OP_OR, "OP_OR"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprOrOp"


	public static class exprXor_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprXor"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:235:1: exprXor : exprAnd ( exprXorOp ^ exprAnd )* ;
	public final BloodParser.exprXor_return exprXor() throws RecognitionException {
		BloodParser.exprXor_return retval = new BloodParser.exprXor_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprAnd119 =null;
		ParserRuleReturnScope exprXorOp120 =null;
		ParserRuleReturnScope exprAnd121 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:235:9: ( exprAnd ( exprXorOp ^ exprAnd )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:235:11: exprAnd ( exprXorOp ^ exprAnd )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprAnd_in_exprXor1412);
			exprAnd119=exprAnd();
			state._fsp--;

			adaptor.addChild(root_0, exprAnd119.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:235:19: ( exprXorOp ^ exprAnd )*
			loop36:
			while (true) {
				int alt36=2;
				int LA36_0 = input.LA(1);
				if ( (LA36_0==112) ) {
					alt36=1;
				}

				switch (alt36) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:235:20: exprXorOp ^ exprAnd
					{
					pushFollow(FOLLOW_exprXorOp_in_exprXor1415);
					exprXorOp120=exprXorOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprXorOp120.getTree(), root_0);
					pushFollow(FOLLOW_exprAnd_in_exprXor1418);
					exprAnd121=exprAnd();
					state._fsp--;

					adaptor.addChild(root_0, exprAnd121.getTree());

					}
					break;

				default :
					break loop36;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprXor"


	public static class exprXorOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprXorOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:238:1: exprXorOp : 'xor' -> OP_XOR ;
	public final BloodParser.exprXorOp_return exprXorOp() throws RecognitionException {
		BloodParser.exprXorOp_return retval = new BloodParser.exprXorOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal122=null;

		Object string_literal122_tree=null;
		RewriteRuleTokenStream stream_112=new RewriteRuleTokenStream(adaptor,"token 112");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:239:2: ( 'xor' -> OP_XOR )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:239:4: 'xor'
			{
			string_literal122=(Token)match(input,112,FOLLOW_112_in_exprXorOp1431);  
			stream_112.add(string_literal122);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 239:10: -> OP_XOR
			{
				adaptor.addChild(root_0, (Object)adaptor.create(OP_XOR, "OP_XOR"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprXorOp"


	public static class exprAnd_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprAnd"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:242:1: exprAnd : exprCompEg ( exprAndOp ^ exprCompEg )* ;
	public final BloodParser.exprAnd_return exprAnd() throws RecognitionException {
		BloodParser.exprAnd_return retval = new BloodParser.exprAnd_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprCompEg123 =null;
		ParserRuleReturnScope exprAndOp124 =null;
		ParserRuleReturnScope exprCompEg125 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:242:9: ( exprCompEg ( exprAndOp ^ exprCompEg )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:242:11: exprCompEg ( exprAndOp ^ exprCompEg )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprCompEg_in_exprAnd1445);
			exprCompEg123=exprCompEg();
			state._fsp--;

			adaptor.addChild(root_0, exprCompEg123.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:242:22: ( exprAndOp ^ exprCompEg )*
			loop37:
			while (true) {
				int alt37=2;
				int LA37_0 = input.LA(1);
				if ( (LA37_0==92) ) {
					alt37=1;
				}

				switch (alt37) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:242:23: exprAndOp ^ exprCompEg
					{
					pushFollow(FOLLOW_exprAndOp_in_exprAnd1448);
					exprAndOp124=exprAndOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprAndOp124.getTree(), root_0);
					pushFollow(FOLLOW_exprCompEg_in_exprAnd1451);
					exprCompEg125=exprCompEg();
					state._fsp--;

					adaptor.addChild(root_0, exprCompEg125.getTree());

					}
					break;

				default :
					break loop37;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprAnd"


	public static class exprAndOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprAndOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:245:1: exprAndOp : 'and' -> OP_AND ;
	public final BloodParser.exprAndOp_return exprAndOp() throws RecognitionException {
		BloodParser.exprAndOp_return retval = new BloodParser.exprAndOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal126=null;

		Object string_literal126_tree=null;
		RewriteRuleTokenStream stream_92=new RewriteRuleTokenStream(adaptor,"token 92");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:246:2: ( 'and' -> OP_AND )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:246:4: 'and'
			{
			string_literal126=(Token)match(input,92,FOLLOW_92_in_exprAndOp1464);  
			stream_92.add(string_literal126);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 246:10: -> OP_AND
			{
				adaptor.addChild(root_0, (Object)adaptor.create(OP_AND, "OP_AND"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprAndOp"


	public static class exprCompEg_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprCompEg"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:249:1: exprCompEg : exprComp ( exprCompEgOp ^ exprComp )* ;
	public final BloodParser.exprCompEg_return exprCompEg() throws RecognitionException {
		BloodParser.exprCompEg_return retval = new BloodParser.exprCompEg_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprComp127 =null;
		ParserRuleReturnScope exprCompEgOp128 =null;
		ParserRuleReturnScope exprComp129 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:250:2: ( exprComp ( exprCompEgOp ^ exprComp )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:250:4: exprComp ( exprCompEgOp ^ exprComp )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprComp_in_exprCompEg1480);
			exprComp127=exprComp();
			state._fsp--;

			adaptor.addChild(root_0, exprComp127.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:250:13: ( exprCompEgOp ^ exprComp )*
			loop38:
			while (true) {
				int alt38=2;
				int LA38_0 = input.LA(1);
				if ( ((LA38_0 >= 87 && LA38_0 <= 88)) ) {
					alt38=1;
				}

				switch (alt38) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:250:14: exprCompEgOp ^ exprComp
					{
					pushFollow(FOLLOW_exprCompEgOp_in_exprCompEg1483);
					exprCompEgOp128=exprCompEgOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprCompEgOp128.getTree(), root_0);
					pushFollow(FOLLOW_exprComp_in_exprCompEg1486);
					exprComp129=exprComp();
					state._fsp--;

					adaptor.addChild(root_0, exprComp129.getTree());

					}
					break;

				default :
					break loop38;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprCompEg"


	public static class exprCompEgOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprCompEgOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:253:1: exprCompEgOp : ( '=' -> OP_EQU | '<>' -> OP_NEQ );
	public final BloodParser.exprCompEgOp_return exprCompEgOp() throws RecognitionException {
		BloodParser.exprCompEgOp_return retval = new BloodParser.exprCompEgOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal130=null;
		Token string_literal131=null;

		Object char_literal130_tree=null;
		Object string_literal131_tree=null;
		RewriteRuleTokenStream stream_88=new RewriteRuleTokenStream(adaptor,"token 88");
		RewriteRuleTokenStream stream_87=new RewriteRuleTokenStream(adaptor,"token 87");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:254:2: ( '=' -> OP_EQU | '<>' -> OP_NEQ )
			int alt39=2;
			int LA39_0 = input.LA(1);
			if ( (LA39_0==88) ) {
				alt39=1;
			}
			else if ( (LA39_0==87) ) {
				alt39=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 39, 0, input);
				throw nvae;
			}

			switch (alt39) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:254:4: '='
					{
					char_literal130=(Token)match(input,88,FOLLOW_88_in_exprCompEgOp1499);  
					stream_88.add(char_literal130);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 254:8: -> OP_EQU
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_EQU, "OP_EQU"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:255:4: '<>'
					{
					string_literal131=(Token)match(input,87,FOLLOW_87_in_exprCompEgOp1508);  
					stream_87.add(string_literal131);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 255:9: -> OP_NEQ
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_NEQ, "OP_NEQ"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprCompEgOp"


	public static class exprComp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprComp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:258:1: exprComp : exprShift ( exprCompOp ^ exprShift )* ;
	public final BloodParser.exprComp_return exprComp() throws RecognitionException {
		BloodParser.exprComp_return retval = new BloodParser.exprComp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprShift132 =null;
		ParserRuleReturnScope exprCompOp133 =null;
		ParserRuleReturnScope exprShift134 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:258:9: ( exprShift ( exprCompOp ^ exprShift )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:258:11: exprShift ( exprCompOp ^ exprShift )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprShift_in_exprComp1522);
			exprShift132=exprShift();
			state._fsp--;

			adaptor.addChild(root_0, exprShift132.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:258:21: ( exprCompOp ^ exprShift )*
			loop40:
			while (true) {
				int alt40=2;
				int LA40_0 = input.LA(1);
				if ( (LA40_0==84||LA40_0==86||(LA40_0 >= 89 && LA40_0 <= 90)) ) {
					alt40=1;
				}

				switch (alt40) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:258:22: exprCompOp ^ exprShift
					{
					pushFollow(FOLLOW_exprCompOp_in_exprComp1525);
					exprCompOp133=exprCompOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprCompOp133.getTree(), root_0);
					pushFollow(FOLLOW_exprShift_in_exprComp1528);
					exprShift134=exprShift();
					state._fsp--;

					adaptor.addChild(root_0, exprShift134.getTree());

					}
					break;

				default :
					break loop40;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprComp"


	public static class exprCompOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprCompOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:261:1: exprCompOp : ( '<' -> OP_LES | '<=' -> OP_LEQ | '>' -> OP_GRT | '>=' -> OP_GEQ );
	public final BloodParser.exprCompOp_return exprCompOp() throws RecognitionException {
		BloodParser.exprCompOp_return retval = new BloodParser.exprCompOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal135=null;
		Token string_literal136=null;
		Token char_literal137=null;
		Token string_literal138=null;

		Object char_literal135_tree=null;
		Object string_literal136_tree=null;
		Object char_literal137_tree=null;
		Object string_literal138_tree=null;
		RewriteRuleTokenStream stream_89=new RewriteRuleTokenStream(adaptor,"token 89");
		RewriteRuleTokenStream stream_90=new RewriteRuleTokenStream(adaptor,"token 90");
		RewriteRuleTokenStream stream_84=new RewriteRuleTokenStream(adaptor,"token 84");
		RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:262:2: ( '<' -> OP_LES | '<=' -> OP_LEQ | '>' -> OP_GRT | '>=' -> OP_GEQ )
			int alt41=4;
			switch ( input.LA(1) ) {
			case 84:
				{
				alt41=1;
				}
				break;
			case 86:
				{
				alt41=2;
				}
				break;
			case 89:
				{
				alt41=3;
				}
				break;
			case 90:
				{
				alt41=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 41, 0, input);
				throw nvae;
			}
			switch (alt41) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:262:4: '<'
					{
					char_literal135=(Token)match(input,84,FOLLOW_84_in_exprCompOp1541);  
					stream_84.add(char_literal135);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 262:8: -> OP_LES
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_LES, "OP_LES"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:263:4: '<='
					{
					string_literal136=(Token)match(input,86,FOLLOW_86_in_exprCompOp1550);  
					stream_86.add(string_literal136);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 263:9: -> OP_LEQ
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_LEQ, "OP_LEQ"));
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:264:4: '>'
					{
					char_literal137=(Token)match(input,89,FOLLOW_89_in_exprCompOp1559);  
					stream_89.add(char_literal137);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 264:8: -> OP_GRT
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_GRT, "OP_GRT"));
					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:265:4: '>='
					{
					string_literal138=(Token)match(input,90,FOLLOW_90_in_exprCompOp1568);  
					stream_90.add(string_literal138);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 265:9: -> OP_GEQ
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_GEQ, "OP_GEQ"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprCompOp"


	public static class exprShift_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprShift"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:268:1: exprShift : exprAdd ( exprShiftOp ^ exprAdd )* ;
	public final BloodParser.exprShift_return exprShift() throws RecognitionException {
		BloodParser.exprShift_return retval = new BloodParser.exprShift_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprAdd139 =null;
		ParserRuleReturnScope exprShiftOp140 =null;
		ParserRuleReturnScope exprAdd141 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:269:2: ( exprAdd ( exprShiftOp ^ exprAdd )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:269:4: exprAdd ( exprShiftOp ^ exprAdd )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprAdd_in_exprShift1584);
			exprAdd139=exprAdd();
			state._fsp--;

			adaptor.addChild(root_0, exprAdd139.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:269:12: ( exprShiftOp ^ exprAdd )*
			loop42:
			while (true) {
				int alt42=2;
				int LA42_0 = input.LA(1);
				if ( (LA42_0==85||LA42_0==91) ) {
					alt42=1;
				}

				switch (alt42) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:269:13: exprShiftOp ^ exprAdd
					{
					pushFollow(FOLLOW_exprShiftOp_in_exprShift1587);
					exprShiftOp140=exprShiftOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprShiftOp140.getTree(), root_0);
					pushFollow(FOLLOW_exprAdd_in_exprShift1590);
					exprAdd141=exprAdd();
					state._fsp--;

					adaptor.addChild(root_0, exprAdd141.getTree());

					}
					break;

				default :
					break loop42;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprShift"


	public static class exprShiftOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprShiftOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:272:1: exprShiftOp : ( '<<' -> OP_SHL | '>>' -> OP_SHR );
	public final BloodParser.exprShiftOp_return exprShiftOp() throws RecognitionException {
		BloodParser.exprShiftOp_return retval = new BloodParser.exprShiftOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal142=null;
		Token string_literal143=null;

		Object string_literal142_tree=null;
		Object string_literal143_tree=null;
		RewriteRuleTokenStream stream_91=new RewriteRuleTokenStream(adaptor,"token 91");
		RewriteRuleTokenStream stream_85=new RewriteRuleTokenStream(adaptor,"token 85");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:273:2: ( '<<' -> OP_SHL | '>>' -> OP_SHR )
			int alt43=2;
			int LA43_0 = input.LA(1);
			if ( (LA43_0==85) ) {
				alt43=1;
			}
			else if ( (LA43_0==91) ) {
				alt43=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 43, 0, input);
				throw nvae;
			}

			switch (alt43) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:273:4: '<<'
					{
					string_literal142=(Token)match(input,85,FOLLOW_85_in_exprShiftOp1603);  
					stream_85.add(string_literal142);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 273:9: -> OP_SHL
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_SHL, "OP_SHL"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:274:4: '>>'
					{
					string_literal143=(Token)match(input,91,FOLLOW_91_in_exprShiftOp1612);  
					stream_91.add(string_literal143);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 274:9: -> OP_SHR
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_SHR, "OP_SHR"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprShiftOp"


	public static class exprAdd_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprAdd"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:277:1: exprAdd : exprMult ( exprAddOp ^ exprMult )* ;
	public final BloodParser.exprAdd_return exprAdd() throws RecognitionException {
		BloodParser.exprAdd_return retval = new BloodParser.exprAdd_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprMult144 =null;
		ParserRuleReturnScope exprAddOp145 =null;
		ParserRuleReturnScope exprMult146 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:277:9: ( exprMult ( exprAddOp ^ exprMult )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:277:11: exprMult ( exprAddOp ^ exprMult )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprMult_in_exprAdd1626);
			exprMult144=exprMult();
			state._fsp--;

			adaptor.addChild(root_0, exprMult144.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:277:20: ( exprAddOp ^ exprMult )*
			loop44:
			while (true) {
				int alt44=2;
				int LA44_0 = input.LA(1);
				if ( (LA44_0==72||LA44_0==76||LA44_0==78) ) {
					alt44=1;
				}

				switch (alt44) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:277:21: exprAddOp ^ exprMult
					{
					pushFollow(FOLLOW_exprAddOp_in_exprAdd1629);
					exprAddOp145=exprAddOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprAddOp145.getTree(), root_0);
					pushFollow(FOLLOW_exprMult_in_exprAdd1632);
					exprMult146=exprMult();
					state._fsp--;

					adaptor.addChild(root_0, exprMult146.getTree());

					}
					break;

				default :
					break loop44;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprAdd"


	public static class exprAddOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprAddOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:280:1: exprAddOp : ( '+' -> OP_ADD | '-' -> OP_SUB | '&' -> OP_CAT );
	public final BloodParser.exprAddOp_return exprAddOp() throws RecognitionException {
		BloodParser.exprAddOp_return retval = new BloodParser.exprAddOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal147=null;
		Token char_literal148=null;
		Token char_literal149=null;

		Object char_literal147_tree=null;
		Object char_literal148_tree=null;
		Object char_literal149_tree=null;
		RewriteRuleTokenStream stream_78=new RewriteRuleTokenStream(adaptor,"token 78");
		RewriteRuleTokenStream stream_72=new RewriteRuleTokenStream(adaptor,"token 72");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:281:2: ( '+' -> OP_ADD | '-' -> OP_SUB | '&' -> OP_CAT )
			int alt45=3;
			switch ( input.LA(1) ) {
			case 76:
				{
				alt45=1;
				}
				break;
			case 78:
				{
				alt45=2;
				}
				break;
			case 72:
				{
				alt45=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 45, 0, input);
				throw nvae;
			}
			switch (alt45) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:281:4: '+'
					{
					char_literal147=(Token)match(input,76,FOLLOW_76_in_exprAddOp1645);  
					stream_76.add(char_literal147);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 281:8: -> OP_ADD
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_ADD, "OP_ADD"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:282:4: '-'
					{
					char_literal148=(Token)match(input,78,FOLLOW_78_in_exprAddOp1654);  
					stream_78.add(char_literal148);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 282:8: -> OP_SUB
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_SUB, "OP_SUB"));
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:283:4: '&'
					{
					char_literal149=(Token)match(input,72,FOLLOW_72_in_exprAddOp1663);  
					stream_72.add(char_literal149);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 283:8: -> OP_CAT
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_CAT, "OP_CAT"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprAddOp"


	public static class exprMult_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprMult"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:286:1: exprMult : exprMessage ( exprMultOp ^ exprMessage )* ;
	public final BloodParser.exprMult_return exprMult() throws RecognitionException {
		BloodParser.exprMult_return retval = new BloodParser.exprMult_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope exprMessage150 =null;
		ParserRuleReturnScope exprMultOp151 =null;
		ParserRuleReturnScope exprMessage152 =null;


		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:286:9: ( exprMessage ( exprMultOp ^ exprMessage )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:286:11: exprMessage ( exprMultOp ^ exprMessage )*
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_exprMessage_in_exprMult1676);
			exprMessage150=exprMessage();
			state._fsp--;

			adaptor.addChild(root_0, exprMessage150.getTree());

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:286:23: ( exprMultOp ^ exprMessage )*
			loop46:
			while (true) {
				int alt46=2;
				int LA46_0 = input.LA(1);
				if ( (LA46_0==71||LA46_0==75||LA46_0==80) ) {
					alt46=1;
				}

				switch (alt46) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:286:24: exprMultOp ^ exprMessage
					{
					pushFollow(FOLLOW_exprMultOp_in_exprMult1679);
					exprMultOp151=exprMultOp();
					state._fsp--;

					root_0 = (Object)adaptor.becomeRoot(exprMultOp151.getTree(), root_0);
					pushFollow(FOLLOW_exprMessage_in_exprMult1682);
					exprMessage152=exprMessage();
					state._fsp--;

					adaptor.addChild(root_0, exprMessage152.getTree());

					}
					break;

				default :
					break loop46;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprMult"


	public static class exprMultOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprMultOp"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:289:1: exprMultOp : ( '*' -> OP_MUL | '/' -> OP_DIV | '%' -> OP_MOD );
	public final BloodParser.exprMultOp_return exprMultOp() throws RecognitionException {
		BloodParser.exprMultOp_return retval = new BloodParser.exprMultOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal153=null;
		Token char_literal154=null;
		Token char_literal155=null;

		Object char_literal153_tree=null;
		Object char_literal154_tree=null;
		Object char_literal155_tree=null;
		RewriteRuleTokenStream stream_80=new RewriteRuleTokenStream(adaptor,"token 80");
		RewriteRuleTokenStream stream_71=new RewriteRuleTokenStream(adaptor,"token 71");
		RewriteRuleTokenStream stream_75=new RewriteRuleTokenStream(adaptor,"token 75");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:290:2: ( '*' -> OP_MUL | '/' -> OP_DIV | '%' -> OP_MOD )
			int alt47=3;
			switch ( input.LA(1) ) {
			case 75:
				{
				alt47=1;
				}
				break;
			case 80:
				{
				alt47=2;
				}
				break;
			case 71:
				{
				alt47=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 47, 0, input);
				throw nvae;
			}
			switch (alt47) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:290:4: '*'
					{
					char_literal153=(Token)match(input,75,FOLLOW_75_in_exprMultOp1695);  
					stream_75.add(char_literal153);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 290:8: -> OP_MUL
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_MUL, "OP_MUL"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:291:4: '/'
					{
					char_literal154=(Token)match(input,80,FOLLOW_80_in_exprMultOp1704);  
					stream_80.add(char_literal154);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 291:8: -> OP_DIV
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_DIV, "OP_DIV"));
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:292:4: '%'
					{
					char_literal155=(Token)match(input,71,FOLLOW_71_in_exprMultOp1713);  
					stream_71.add(char_literal155);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 292:8: -> OP_MOD
					{
						adaptor.addChild(root_0, (Object)adaptor.create(OP_MOD, "OP_MOD"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprMultOp"


	public static class exprMessage_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprMessage"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:295:1: exprMessage : ( exprFin ( message -> ^( MESSAGE_BASE exprFin message ) | -> exprFin ) | IDCLASS message -> ^( MESSAGE_BASE IDCLASS message ) );
	public final BloodParser.exprMessage_return exprMessage() throws RecognitionException {
		BloodParser.exprMessage_return retval = new BloodParser.exprMessage_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token IDCLASS158=null;
		ParserRuleReturnScope exprFin156 =null;
		ParserRuleReturnScope message157 =null;
		ParserRuleReturnScope message159 =null;

		Object IDCLASS158_tree=null;
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleSubtreeStream stream_exprFin=new RewriteRuleSubtreeStream(adaptor,"rule exprFin");
		RewriteRuleSubtreeStream stream_message=new RewriteRuleSubtreeStream(adaptor,"rule message");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:296:2: ( exprFin ( message -> ^( MESSAGE_BASE exprFin message ) | -> exprFin ) | IDCLASS message -> ^( MESSAGE_BASE IDCLASS message ) )
			int alt49=2;
			int LA49_0 = input.LA(1);
			if ( (LA49_0==ID||LA49_0==INT||LA49_0==STRING||LA49_0==73||LA49_0==76||LA49_0==78||LA49_0==104||LA49_0==107||LA49_0==109||LA49_0==115) ) {
				alt49=1;
			}
			else if ( (LA49_0==IDCLASS) ) {
				alt49=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 49, 0, input);
				throw nvae;
			}

			switch (alt49) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:296:4: exprFin ( message -> ^( MESSAGE_BASE exprFin message ) | -> exprFin )
					{
					pushFollow(FOLLOW_exprFin_in_exprMessage1728);
					exprFin156=exprFin();
					state._fsp--;

					stream_exprFin.add(exprFin156.getTree());
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:297:3: ( message -> ^( MESSAGE_BASE exprFin message ) | -> exprFin )
					int alt48=2;
					int LA48_0 = input.LA(1);
					if ( (LA48_0==79) ) {
						alt48=1;
					}
					else if ( ((LA48_0 >= 71 && LA48_0 <= 72)||(LA48_0 >= 74 && LA48_0 <= 78)||(LA48_0 >= 80 && LA48_0 <= 92)||(LA48_0 >= 95 && LA48_0 <= 96)||LA48_0==102||LA48_0==108||LA48_0==110||LA48_0==112||LA48_0==114) ) {
						alt48=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 48, 0, input);
						throw nvae;
					}

					switch (alt48) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:297:5: message
							{
							pushFollow(FOLLOW_message_in_exprMessage1734);
							message157=message();
							state._fsp--;

							stream_message.add(message157.getTree());
							// AST REWRITE
							// elements: message, exprFin
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 298:4: -> ^( MESSAGE_BASE exprFin message )
							{
								// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:298:7: ^( MESSAGE_BASE exprFin message )
								{
								Object root_1 = (Object)adaptor.nil();
								root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MESSAGE_BASE, "MESSAGE_BASE"), root_1);
								adaptor.addChild(root_1, stream_exprFin.nextTree());
								adaptor.addChild(root_1, stream_message.nextTree());
								adaptor.addChild(root_0, root_1);
								}

							}


							retval.tree = root_0;

							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:299:5: 
							{
							// AST REWRITE
							// elements: exprFin
							// token labels: 
							// rule labels: retval
							// token list labels: 
							// rule list labels: 
							// wildcard labels: 
							retval.tree = root_0;
							RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

							root_0 = (Object)adaptor.nil();
							// 299:5: -> exprFin
							{
								adaptor.addChild(root_0, stream_exprFin.nextTree());
							}


							retval.tree = root_0;

							}
							break;

					}

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:301:4: IDCLASS message
					{
					IDCLASS158=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_exprMessage1764);  
					stream_IDCLASS.add(IDCLASS158);

					pushFollow(FOLLOW_message_in_exprMessage1766);
					message159=message();
					state._fsp--;

					stream_message.add(message159.getTree());
					// AST REWRITE
					// elements: IDCLASS, message
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 302:3: -> ^( MESSAGE_BASE IDCLASS message )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:302:6: ^( MESSAGE_BASE IDCLASS message )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MESSAGE_BASE, "MESSAGE_BASE"), root_1);
						adaptor.addChild(root_1, stream_IDCLASS.nextNode());
						adaptor.addChild(root_1, stream_message.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprMessage"


	public static class message_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "message"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:305:1: message : '.' ID ( '(' args ')' ( message )? -> ^( MESSAGE_CALL ID args ( message )? ) | ( message )? -> ^( MESSAGE ID ( message )? ) ) ;
	public final BloodParser.message_return message() throws RecognitionException {
		BloodParser.message_return retval = new BloodParser.message_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal160=null;
		Token ID161=null;
		Token char_literal162=null;
		Token char_literal164=null;
		ParserRuleReturnScope args163 =null;
		ParserRuleReturnScope message165 =null;
		ParserRuleReturnScope message166 =null;

		Object char_literal160_tree=null;
		Object ID161_tree=null;
		Object char_literal162_tree=null;
		Object char_literal164_tree=null;
		RewriteRuleTokenStream stream_79=new RewriteRuleTokenStream(adaptor,"token 79");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleSubtreeStream stream_args=new RewriteRuleSubtreeStream(adaptor,"rule args");
		RewriteRuleSubtreeStream stream_message=new RewriteRuleSubtreeStream(adaptor,"rule message");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:305:9: ( '.' ID ( '(' args ')' ( message )? -> ^( MESSAGE_CALL ID args ( message )? ) | ( message )? -> ^( MESSAGE ID ( message )? ) ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:305:11: '.' ID ( '(' args ')' ( message )? -> ^( MESSAGE_CALL ID args ( message )? ) | ( message )? -> ^( MESSAGE ID ( message )? ) )
			{
			char_literal160=(Token)match(input,79,FOLLOW_79_in_message1789);  
			stream_79.add(char_literal160);

			ID161=(Token)match(input,ID,FOLLOW_ID_in_message1791);  
			stream_ID.add(ID161);

			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:306:3: ( '(' args ')' ( message )? -> ^( MESSAGE_CALL ID args ( message )? ) | ( message )? -> ^( MESSAGE ID ( message )? ) )
			int alt52=2;
			int LA52_0 = input.LA(1);
			if ( (LA52_0==73) ) {
				alt52=1;
			}
			else if ( ((LA52_0 >= 71 && LA52_0 <= 72)||(LA52_0 >= 74 && LA52_0 <= 92)||(LA52_0 >= 95 && LA52_0 <= 96)||LA52_0==102||LA52_0==108||LA52_0==110||LA52_0==112||LA52_0==114) ) {
				alt52=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 52, 0, input);
				throw nvae;
			}

			switch (alt52) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:306:5: '(' args ')' ( message )?
					{
					char_literal162=(Token)match(input,73,FOLLOW_73_in_message1797);  
					stream_73.add(char_literal162);

					pushFollow(FOLLOW_args_in_message1799);
					args163=args();
					state._fsp--;

					stream_args.add(args163.getTree());
					char_literal164=(Token)match(input,74,FOLLOW_74_in_message1801);  
					stream_74.add(char_literal164);

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:306:18: ( message )?
					int alt50=2;
					int LA50_0 = input.LA(1);
					if ( (LA50_0==79) ) {
						alt50=1;
					}
					switch (alt50) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:306:18: message
							{
							pushFollow(FOLLOW_message_in_message1803);
							message165=message();
							state._fsp--;

							stream_message.add(message165.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: ID, args, message
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 307:4: -> ^( MESSAGE_CALL ID args ( message )? )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:307:7: ^( MESSAGE_CALL ID args ( message )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MESSAGE_CALL, "MESSAGE_CALL"), root_1);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_1, stream_args.nextTree());
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:307:30: ( message )?
						if ( stream_message.hasNext() ) {
							adaptor.addChild(root_1, stream_message.nextTree());
						}
						stream_message.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:308:5: ( message )?
					{
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:308:5: ( message )?
					int alt51=2;
					int LA51_0 = input.LA(1);
					if ( (LA51_0==79) ) {
						alt51=1;
					}
					switch (alt51) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:308:5: message
							{
							pushFollow(FOLLOW_message_in_message1826);
							message166=message();
							state._fsp--;

							stream_message.add(message166.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: message, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 309:4: -> ^( MESSAGE ID ( message )? )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:309:7: ^( MESSAGE ID ( message )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MESSAGE, "MESSAGE"), root_1);
						adaptor.addChild(root_1, stream_ID.nextNode());
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:309:20: ( message )?
						if ( stream_message.hasNext() ) {
							adaptor.addChild(root_1, stream_message.nextTree());
						}
						stream_message.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "message"


	public static class exprFin_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprFin"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:313:1: exprFin : ( '(' exprCast ')' -> exprCast | constante | identificateur | '+' ( constante | identificateur ) -> ( constante )? ( identificateur )? | '-' ( constante | identificateur ) -> ^( OP_NEG ( constante )? ( identificateur )? ) | '~' ( constante | identificateur ) -> ^( OP_NOT ( constante )? ( identificateur )? ) );
	public final BloodParser.exprFin_return exprFin() throws RecognitionException {
		BloodParser.exprFin_return retval = new BloodParser.exprFin_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal167=null;
		Token char_literal169=null;
		Token char_literal172=null;
		Token char_literal175=null;
		Token char_literal178=null;
		ParserRuleReturnScope exprCast168 =null;
		ParserRuleReturnScope constante170 =null;
		ParserRuleReturnScope identificateur171 =null;
		ParserRuleReturnScope constante173 =null;
		ParserRuleReturnScope identificateur174 =null;
		ParserRuleReturnScope constante176 =null;
		ParserRuleReturnScope identificateur177 =null;
		ParserRuleReturnScope constante179 =null;
		ParserRuleReturnScope identificateur180 =null;

		Object char_literal167_tree=null;
		Object char_literal169_tree=null;
		Object char_literal172_tree=null;
		Object char_literal175_tree=null;
		Object char_literal178_tree=null;
		RewriteRuleTokenStream stream_78=new RewriteRuleTokenStream(adaptor,"token 78");
		RewriteRuleTokenStream stream_115=new RewriteRuleTokenStream(adaptor,"token 115");
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");
		RewriteRuleSubtreeStream stream_exprCast=new RewriteRuleSubtreeStream(adaptor,"rule exprCast");
		RewriteRuleSubtreeStream stream_constante=new RewriteRuleSubtreeStream(adaptor,"rule constante");
		RewriteRuleSubtreeStream stream_identificateur=new RewriteRuleSubtreeStream(adaptor,"rule identificateur");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:314:2: ( '(' exprCast ')' -> exprCast | constante | identificateur | '+' ( constante | identificateur ) -> ( constante )? ( identificateur )? | '-' ( constante | identificateur ) -> ^( OP_NEG ( constante )? ( identificateur )? ) | '~' ( constante | identificateur ) -> ^( OP_NOT ( constante )? ( identificateur )? ) )
			int alt56=6;
			switch ( input.LA(1) ) {
			case 73:
				{
				alt56=1;
				}
				break;
			case INT:
			case STRING:
				{
				alt56=2;
				}
				break;
			case ID:
			case 104:
			case 107:
			case 109:
				{
				alt56=3;
				}
				break;
			case 76:
				{
				alt56=4;
				}
				break;
			case 78:
				{
				alt56=5;
				}
				break;
			case 115:
				{
				alt56=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 56, 0, input);
				throw nvae;
			}
			switch (alt56) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:314:4: '(' exprCast ')'
					{
					char_literal167=(Token)match(input,73,FOLLOW_73_in_exprFin1856);  
					stream_73.add(char_literal167);

					pushFollow(FOLLOW_exprCast_in_exprFin1858);
					exprCast168=exprCast();
					state._fsp--;

					stream_exprCast.add(exprCast168.getTree());
					char_literal169=(Token)match(input,74,FOLLOW_74_in_exprFin1860);  
					stream_74.add(char_literal169);

					// AST REWRITE
					// elements: exprCast
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 315:3: -> exprCast
					{
						adaptor.addChild(root_0, stream_exprCast.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:316:4: constante
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_constante_in_exprFin1871);
					constante170=constante();
					state._fsp--;

					adaptor.addChild(root_0, constante170.getTree());

					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:317:4: identificateur
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_identificateur_in_exprFin1876);
					identificateur171=identificateur();
					state._fsp--;

					adaptor.addChild(root_0, identificateur171.getTree());

					}
					break;
				case 4 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:318:4: '+' ( constante | identificateur )
					{
					char_literal172=(Token)match(input,76,FOLLOW_76_in_exprFin1881);  
					stream_76.add(char_literal172);

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:318:8: ( constante | identificateur )
					int alt53=2;
					int LA53_0 = input.LA(1);
					if ( (LA53_0==INT||LA53_0==STRING) ) {
						alt53=1;
					}
					else if ( (LA53_0==ID||LA53_0==104||LA53_0==107||LA53_0==109) ) {
						alt53=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 53, 0, input);
						throw nvae;
					}

					switch (alt53) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:318:9: constante
							{
							pushFollow(FOLLOW_constante_in_exprFin1884);
							constante173=constante();
							state._fsp--;

							stream_constante.add(constante173.getTree());
							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:318:21: identificateur
							{
							pushFollow(FOLLOW_identificateur_in_exprFin1888);
							identificateur174=identificateur();
							state._fsp--;

							stream_identificateur.add(identificateur174.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: identificateur, constante
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 318:37: -> ( constante )? ( identificateur )?
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:318:40: ( constante )?
						if ( stream_constante.hasNext() ) {
							adaptor.addChild(root_0, stream_constante.nextTree());
						}
						stream_constante.reset();

						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:318:51: ( identificateur )?
						if ( stream_identificateur.hasNext() ) {
							adaptor.addChild(root_0, stream_identificateur.nextTree());
						}
						stream_identificateur.reset();

					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:4: '-' ( constante | identificateur )
					{
					char_literal175=(Token)match(input,78,FOLLOW_78_in_exprFin1902);  
					stream_78.add(char_literal175);

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:8: ( constante | identificateur )
					int alt54=2;
					int LA54_0 = input.LA(1);
					if ( (LA54_0==INT||LA54_0==STRING) ) {
						alt54=1;
					}
					else if ( (LA54_0==ID||LA54_0==104||LA54_0==107||LA54_0==109) ) {
						alt54=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 54, 0, input);
						throw nvae;
					}

					switch (alt54) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:9: constante
							{
							pushFollow(FOLLOW_constante_in_exprFin1905);
							constante176=constante();
							state._fsp--;

							stream_constante.add(constante176.getTree());
							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:21: identificateur
							{
							pushFollow(FOLLOW_identificateur_in_exprFin1909);
							identificateur177=identificateur();
							state._fsp--;

							stream_identificateur.add(identificateur177.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: identificateur, constante
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 319:37: -> ^( OP_NEG ( constante )? ( identificateur )? )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:40: ^( OP_NEG ( constante )? ( identificateur )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(OP_NEG, "OP_NEG"), root_1);
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:49: ( constante )?
						if ( stream_constante.hasNext() ) {
							adaptor.addChild(root_1, stream_constante.nextTree());
						}
						stream_constante.reset();

						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:319:60: ( identificateur )?
						if ( stream_identificateur.hasNext() ) {
							adaptor.addChild(root_1, stream_identificateur.nextTree());
						}
						stream_identificateur.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 6 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:4: '~' ( constante | identificateur )
					{
					char_literal178=(Token)match(input,115,FOLLOW_115_in_exprFin1927);  
					stream_115.add(char_literal178);

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:8: ( constante | identificateur )
					int alt55=2;
					int LA55_0 = input.LA(1);
					if ( (LA55_0==INT||LA55_0==STRING) ) {
						alt55=1;
					}
					else if ( (LA55_0==ID||LA55_0==104||LA55_0==107||LA55_0==109) ) {
						alt55=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 55, 0, input);
						throw nvae;
					}

					switch (alt55) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:9: constante
							{
							pushFollow(FOLLOW_constante_in_exprFin1930);
							constante179=constante();
							state._fsp--;

							stream_constante.add(constante179.getTree());
							}
							break;
						case 2 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:21: identificateur
							{
							pushFollow(FOLLOW_identificateur_in_exprFin1934);
							identificateur180=identificateur();
							state._fsp--;

							stream_identificateur.add(identificateur180.getTree());
							}
							break;

					}

					// AST REWRITE
					// elements: constante, identificateur
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 320:37: -> ^( OP_NOT ( constante )? ( identificateur )? )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:40: ^( OP_NOT ( constante )? ( identificateur )? )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(OP_NOT, "OP_NOT"), root_1);
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:49: ( constante )?
						if ( stream_constante.hasNext() ) {
							adaptor.addChild(root_1, stream_constante.nextTree());
						}
						stream_constante.reset();

						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:320:60: ( identificateur )?
						if ( stream_identificateur.hasNext() ) {
							adaptor.addChild(root_1, stream_identificateur.nextTree());
						}
						stream_identificateur.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprFin"


	public static class exprCast_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "exprCast"
	// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:323:1: exprCast : ( 'as' IDCLASS ':' expression -> ^( CAST IDCLASS expression ) | expression );
	public final BloodParser.exprCast_return exprCast() throws RecognitionException {
		BloodParser.exprCast_return retval = new BloodParser.exprCast_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal181=null;
		Token IDCLASS182=null;
		Token char_literal183=null;
		ParserRuleReturnScope expression184 =null;
		ParserRuleReturnScope expression185 =null;

		Object string_literal181_tree=null;
		Object IDCLASS182_tree=null;
		Object char_literal183_tree=null;
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleTokenStream stream_IDCLASS=new RewriteRuleTokenStream(adaptor,"token IDCLASS");
		RewriteRuleTokenStream stream_93=new RewriteRuleTokenStream(adaptor,"token 93");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:323:9: ( 'as' IDCLASS ':' expression -> ^( CAST IDCLASS expression ) | expression )
			int alt57=2;
			int LA57_0 = input.LA(1);
			if ( (LA57_0==93) ) {
				alt57=1;
			}
			else if ( ((LA57_0 >= ID && LA57_0 <= IDCLASS)||LA57_0==INT||LA57_0==STRING||LA57_0==73||LA57_0==76||LA57_0==78||LA57_0==101||LA57_0==104||LA57_0==107||LA57_0==109||LA57_0==115) ) {
				alt57=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 57, 0, input);
				throw nvae;
			}

			switch (alt57) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:323:11: 'as' IDCLASS ':' expression
					{
					string_literal181=(Token)match(input,93,FOLLOW_93_in_exprCast1956);  
					stream_93.add(string_literal181);

					IDCLASS182=(Token)match(input,IDCLASS,FOLLOW_IDCLASS_in_exprCast1958);  
					stream_IDCLASS.add(IDCLASS182);

					char_literal183=(Token)match(input,81,FOLLOW_81_in_exprCast1960);  
					stream_81.add(char_literal183);

					pushFollow(FOLLOW_expression_in_exprCast1962);
					expression184=expression();
					state._fsp--;

					stream_expression.add(expression184.getTree());
					// AST REWRITE
					// elements: expression, IDCLASS
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 324:3: -> ^( CAST IDCLASS expression )
					{
						// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:324:6: ^( CAST IDCLASS expression )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CAST, "CAST"), root_1);
						adaptor.addChild(root_1, stream_IDCLASS.nextNode());
						adaptor.addChild(root_1, stream_expression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:325:4: expression
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_expression_in_exprCast1979);
					expression185=expression();
					state._fsp--;

					adaptor.addChild(root_0, expression185.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "exprCast"

	// Delegated rules



	public static final BitSet FOLLOW_defClasse_in_prog313 = new BitSet(new long[]{0x4000000004C00000L,0x000AAB2840005200L});
	public static final BitSet FOLLOW_instruction_in_prog316 = new BitSet(new long[]{0x4000000004C00000L,0x000AAB2800005200L});
	public static final BitSet FOLLOW_EOF_in_prog319 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_instruction349 = new BitSet(new long[]{0x0000000000000000L,0x00000000000E0000L});
	public static final BitSet FOLLOW_81_in_instruction355 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_instruction357 = new BitSet(new long[]{0x0000000000000000L,0x00000000000C0000L});
	public static final BitSet FOLLOW_82_in_instruction364 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_instruction368 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_82_in_instruction415 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_instruction419 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_83_in_instruction458 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_105_in_instruction465 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_83_in_instruction467 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_99_in_instruction481 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_instruction483 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
	public static final BitSet FOLLOW_108_in_instruction485 = new BitSet(new long[]{0x4000000004C00000L,0x000AAB2800005200L});
	public static final BitSet FOLLOW_instruction_in_instruction489 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
	public static final BitSet FOLLOW_97_in_instruction491 = new BitSet(new long[]{0x4000000004C00000L,0x000AAB2800005200L});
	public static final BitSet FOLLOW_instruction_in_instruction495 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_111_in_instruction527 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_instruction529 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
	public static final BitSet FOLLOW_96_in_instruction531 = new BitSet(new long[]{0x4000000004C00000L,0x000AAB2800005200L});
	public static final BitSet FOLLOW_instruction_in_instruction533 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_bloc_in_instruction550 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_113_in_bloc573 = new BitSet(new long[]{0x4000000004C00000L,0x000EAB2800005200L});
	public static final BitSet FOLLOW_instruction_in_bloc583 = new BitSet(new long[]{0x4000000004C00000L,0x000EAB3800005200L});
	public static final BitSet FOLLOW_100_in_bloc592 = new BitSet(new long[]{0x4000000004C00000L,0x000AAB2800005200L});
	public static final BitSet FOLLOW_instruction_in_bloc597 = new BitSet(new long[]{0x4000000004C00000L,0x000EAB2800005200L});
	public static final BitSet FOLLOW_114_in_bloc647 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_INT_in_constante659 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_constante665 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_101_in_instanciation677 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_instanciation679 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_instanciation681 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005600L});
	public static final BitSet FOLLOW_args_in_instanciation683 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_instanciation685 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_109_in_identificateur709 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_107_in_identificateur720 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_104_in_identificateur731 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_identificateur742 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_94_in_defClasse754 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_defClasse756 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_defClasse758 = new BitSet(new long[]{0x0000000000400000L,0x0000400000000400L});
	public static final BitSet FOLLOW_paramsClasse_in_defClasse760 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_defClasse762 = new BitSet(new long[]{0x0000000000000000L,0x0000001400000000L});
	public static final BitSet FOLLOW_98_in_defClasse765 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_defClasse767 = new BitSet(new long[]{0x0000000000000000L,0x0000001000000000L});
	public static final BitSet FOLLOW_100_in_defClasse771 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
	public static final BitSet FOLLOW_113_in_defClasse773 = new BitSet(new long[]{0x0000000000000000L,0x0000400080000000L});
	public static final BitSet FOLLOW_defClasse2_in_defClasse775 = new BitSet(new long[]{0x0000000000000000L,0x0004400080000000L});
	public static final BitSet FOLLOW_114_in_defClasse778 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_defAttr_in_defClasse2815 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_95_in_defClasse2821 = new BitSet(new long[]{0x0000000000C00000L,0x0000048000000000L});
	public static final BitSet FOLLOW_defMethode_in_defClasse2825 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_defConstructeur_in_defClasse2830 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDCLASS_in_defConstructeur845 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_defConstructeur847 = new BitSet(new long[]{0x0000000000400000L,0x0000400000000400L});
	public static final BitSet FOLLOW_paramsConstructor_in_defConstructeur849 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_defConstructeur851 = new BitSet(new long[]{0x0000000000000000L,0x0000001000020000L});
	public static final BitSet FOLLOW_81_in_defConstructeur854 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_defConstructeur858 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_defConstructeur860 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005600L});
	public static final BitSet FOLLOW_args_in_defConstructeur862 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_defConstructeur864 = new BitSet(new long[]{0x0000000000000000L,0x0000001000000000L});
	public static final BitSet FOLLOW_100_in_defConstructeur868 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
	public static final BitSet FOLLOW_bloc_in_defConstructeur870 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodeProps_in_defMethode911 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_ID_in_defMethode914 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
	public static final BitSet FOLLOW_73_in_defMethode916 = new BitSet(new long[]{0x0000000000400000L,0x0000000000000400L});
	public static final BitSet FOLLOW_paramsMethode_in_defMethode918 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_defMethode920 = new BitSet(new long[]{0x0000000000000000L,0x0000001000020000L});
	public static final BitSet FOLLOW_81_in_defMethode924 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_defMethode926 = new BitSet(new long[]{0x0000000000000000L,0x0000001000040000L});
	public static final BitSet FOLLOW_82_in_defMethode929 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_defMethode931 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_100_in_defMethode935 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
	public static final BitSet FOLLOW_bloc_in_defMethode937 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_100_in_defMethode943 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
	public static final BitSet FOLLOW_bloc_in_defMethode945 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_106_in_methodeProps994 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_103_in_methodeProps1003 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_110_in_defAttr1017 = new BitSet(new long[]{0x0000000000400000L,0x0000040000000000L});
	public static final BitSet FOLLOW_attrProps_in_defAttr1019 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_ID_in_defAttr1022 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_defAttr1024 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_defAttr1026 = new BitSet(new long[]{0x0000000000000000L,0x00000000000C0000L});
	public static final BitSet FOLLOW_83_in_defAttr1029 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_82_in_defAttr1033 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_defAttr1035 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
	public static final BitSet FOLLOW_83_in_defAttr1037 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_106_in_attrProps1073 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_paramClasse_in_paramsClasse1090 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_77_in_paramsClasse1093 = new BitSet(new long[]{0x0000000000400000L,0x0000400000000000L});
	public static final BitSet FOLLOW_paramClasse_in_paramsClasse1095 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_110_in_paramClasse1122 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_ID_in_paramClasse1124 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_paramClasse1126 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_paramClasse1128 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_paramClasse1147 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_paramClasse1149 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_paramClasse1151 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_paramConstructor_in_paramsConstructor1176 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_77_in_paramsConstructor1179 = new BitSet(new long[]{0x0000000000400000L,0x0000400000000000L});
	public static final BitSet FOLLOW_paramConstructor_in_paramsConstructor1181 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_110_in_paramConstructor1208 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_ID_in_paramConstructor1210 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_paramConstructor1212 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_paramConstructor1214 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_paramConstructor1233 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_paramConstructor1235 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_paramConstructor1237 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_paramMethode_in_paramsMethode1261 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_77_in_paramsMethode1264 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_paramMethode_in_paramsMethode1266 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_ID_in_paramMethode1293 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_paramMethode1295 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_paramMethode1297 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_args1322 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_77_in_args1325 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_args1327 = new BitSet(new long[]{0x0000000000000002L,0x0000000000002000L});
	public static final BitSet FOLLOW_instanciation_in_expression1354 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprOp_in_expression1359 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprOr_in_exprOp1369 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprXor_in_exprOr1380 = new BitSet(new long[]{0x0000000000000002L,0x0000004000000000L});
	public static final BitSet FOLLOW_exprOrOp_in_exprOr1383 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprXor_in_exprOr1386 = new BitSet(new long[]{0x0000000000000002L,0x0000004000000000L});
	public static final BitSet FOLLOW_102_in_exprOrOp1397 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprAnd_in_exprXor1412 = new BitSet(new long[]{0x0000000000000002L,0x0001000000000000L});
	public static final BitSet FOLLOW_exprXorOp_in_exprXor1415 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprAnd_in_exprXor1418 = new BitSet(new long[]{0x0000000000000002L,0x0001000000000000L});
	public static final BitSet FOLLOW_112_in_exprXorOp1431 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprCompEg_in_exprAnd1445 = new BitSet(new long[]{0x0000000000000002L,0x0000000010000000L});
	public static final BitSet FOLLOW_exprAndOp_in_exprAnd1448 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprCompEg_in_exprAnd1451 = new BitSet(new long[]{0x0000000000000002L,0x0000000010000000L});
	public static final BitSet FOLLOW_92_in_exprAndOp1464 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprComp_in_exprCompEg1480 = new BitSet(new long[]{0x0000000000000002L,0x0000000001800000L});
	public static final BitSet FOLLOW_exprCompEgOp_in_exprCompEg1483 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprComp_in_exprCompEg1486 = new BitSet(new long[]{0x0000000000000002L,0x0000000001800000L});
	public static final BitSet FOLLOW_88_in_exprCompEgOp1499 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_87_in_exprCompEgOp1508 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprShift_in_exprComp1522 = new BitSet(new long[]{0x0000000000000002L,0x0000000006500000L});
	public static final BitSet FOLLOW_exprCompOp_in_exprComp1525 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprShift_in_exprComp1528 = new BitSet(new long[]{0x0000000000000002L,0x0000000006500000L});
	public static final BitSet FOLLOW_84_in_exprCompOp1541 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_86_in_exprCompOp1550 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_89_in_exprCompOp1559 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_90_in_exprCompOp1568 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprAdd_in_exprShift1584 = new BitSet(new long[]{0x0000000000000002L,0x0000000008200000L});
	public static final BitSet FOLLOW_exprShiftOp_in_exprShift1587 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprAdd_in_exprShift1590 = new BitSet(new long[]{0x0000000000000002L,0x0000000008200000L});
	public static final BitSet FOLLOW_85_in_exprShiftOp1603 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_91_in_exprShiftOp1612 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprMult_in_exprAdd1626 = new BitSet(new long[]{0x0000000000000002L,0x0000000000005100L});
	public static final BitSet FOLLOW_exprAddOp_in_exprAdd1629 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprMult_in_exprAdd1632 = new BitSet(new long[]{0x0000000000000002L,0x0000000000005100L});
	public static final BitSet FOLLOW_76_in_exprAddOp1645 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_78_in_exprAddOp1654 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_72_in_exprAddOp1663 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprMessage_in_exprMult1676 = new BitSet(new long[]{0x0000000000000002L,0x0000000000010880L});
	public static final BitSet FOLLOW_exprMultOp_in_exprMult1679 = new BitSet(new long[]{0x4000000004C00000L,0x0008290000005200L});
	public static final BitSet FOLLOW_exprMessage_in_exprMult1682 = new BitSet(new long[]{0x0000000000000002L,0x0000000000010880L});
	public static final BitSet FOLLOW_75_in_exprMultOp1695 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_80_in_exprMultOp1704 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_71_in_exprMultOp1713 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_exprFin_in_exprMessage1728 = new BitSet(new long[]{0x0000000000000002L,0x0000000000008000L});
	public static final BitSet FOLLOW_message_in_exprMessage1734 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IDCLASS_in_exprMessage1764 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008000L});
	public static final BitSet FOLLOW_message_in_exprMessage1766 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_79_in_message1789 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_ID_in_message1791 = new BitSet(new long[]{0x0000000000000002L,0x0000000000008200L});
	public static final BitSet FOLLOW_73_in_message1797 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005600L});
	public static final BitSet FOLLOW_args_in_message1799 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_message1801 = new BitSet(new long[]{0x0000000000000002L,0x0000000000008000L});
	public static final BitSet FOLLOW_message_in_message1803 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_message_in_message1826 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_73_in_exprFin1856 = new BitSet(new long[]{0x4000000004C00000L,0x0008292020005200L});
	public static final BitSet FOLLOW_exprCast_in_exprFin1858 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_exprFin1860 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_constante_in_exprFin1871 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identificateur_in_exprFin1876 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_76_in_exprFin1881 = new BitSet(new long[]{0x4000000004400000L,0x0000290000000000L});
	public static final BitSet FOLLOW_constante_in_exprFin1884 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identificateur_in_exprFin1888 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_78_in_exprFin1902 = new BitSet(new long[]{0x4000000004400000L,0x0000290000000000L});
	public static final BitSet FOLLOW_constante_in_exprFin1905 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identificateur_in_exprFin1909 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_115_in_exprFin1927 = new BitSet(new long[]{0x4000000004400000L,0x0000290000000000L});
	public static final BitSet FOLLOW_constante_in_exprFin1930 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_identificateur_in_exprFin1934 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_93_in_exprCast1956 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_IDCLASS_in_exprCast1958 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_exprCast1960 = new BitSet(new long[]{0x4000000004C00000L,0x0008292000005200L});
	public static final BitSet FOLLOW_expression_in_exprCast1962 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_exprCast1979 = new BitSet(new long[]{0x0000000000000002L});
}
