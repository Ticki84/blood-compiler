package BloodCompiler.antlr;

import BloodCompiler.ast.nodes.*;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.Tree;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ANTLRParser {
    private Tree tree;
    private int lastSourceLine = 0;

    public void parseSource(String sourcePath) throws IOException, RecognitionException {
        File file = new File(sourcePath);
        InputStream stream = new DataInputStream(new FileInputStream(file));
        ANTLRInputStream astream = new ANTLRInputStream(stream);
        BloodLexer lexer = new BloodLexer(astream);
        TokenStream tokens = new CommonTokenStream(lexer);
        BloodParser parser = new BloodParser(tokens);
        tree = (Tree) parser.prog().getTree();
    }

    public String getStringTree() {
        return tree.toStringTree();
    }

    private List<Node> buildAST(Tree tree) throws MalformedTreeException {
        int sourceLine = tree.getLine();

        /* Very hacky fix for null source line for some nodes,
         * better than nothing ¯\_(ツ)_/¯
         * */
        if (sourceLine == 0) sourceLine = lastSourceLine;
        lastSourceLine = sourceLine;
        int sourceCharPos = tree.getCharPositionInLine();
        String text = tree.getText();

        /* declare variables used for multiple types */
        Node child;
        Node leftOperand;
        Node rightOperand;
        Node operand;
        Node argsNode;
        ArrayList<Node> treeChildren;
        String className;
        String name;
        String type;
        ArrayList<Node> parameters;
        Node message;


        List<Node> children = new ArrayList<>();

        try {
            switch (tree.getType()) {
                case BloodParser.ARGS:
                    parameters = new ArrayList<>();
                    for (int i = 0; i < tree.getChildCount(); i++) {
                        parameters.add(buildAST(tree.getChild(i)).get(0));
                    }
                    children.add(new Args(sourceLine, sourceCharPos, text, parameters));
                    break;
                case BloodParser.ASSIGNMENT:
                    Node var = buildAST(tree.getChild(0)).get(0);
                    Node assignee = buildAST(tree.getChild(1)).get(0);
                    children.add(new Assignment(sourceLine, sourceCharPos, text, var, assignee));
                    break;
                case BloodParser.ATTR_PROPS:
                    if (tree.getChild(0) != null) {
                        children.add(new AttrProps(sourceLine, sourceCharPos, text, true));
                    }
                    break;
                case BloodParser.BLOCK:
                    ArrayList<Node> instructions = new ArrayList<>();
                    if (tree.getChild(0).getType() == BloodParser.DEF_LVARS) {
                        Tree declarationsTree = tree.getChild(0);
                        ArrayList<Node> declarations = new ArrayList<>();
                        for (int i = 0; i < declarationsTree.getChildCount(); i++) {
                            declarations.add(buildAST(declarationsTree.getChild(i)).get(0));
                        }
                        for (int i = 1; i < tree.getChildCount(); i++) {
                            instructions.add(buildAST(tree.getChild(i)).get(0));
                        }
                        children.add(new Block(sourceLine, sourceCharPos, text, declarations, instructions));
                    } else {
                        for (int i = 0; i < tree.getChildCount(); i++) {
                            instructions.add(buildAST(tree.getChild(i)).get(0));
                        }
                        children.add(new Block(sourceLine, sourceCharPos, text, instructions));
                    }
                    break;
                case BloodParser.BODY_CLASS:
                    ArrayList<Node> attributes = new ArrayList<>();
                    ArrayList<Node> methods = new ArrayList<>();
                    ArrayList<Node> constructors = new ArrayList<>();

                    for (int i = 0; i < tree.getChildCount(); i++) {
                        Tree curTree = tree.getChild(i);
                        Node node = buildAST(curTree).get(0);
                        switch (curTree.getType()) {
                            case BloodParser.DEF_ATTRIBUTE:
                                attributes.add(node);
                                break;
                            case BloodParser.DEF_METHOD:
                                methods.add(node);
                                break;
                            case BloodParser.DEF_CONSTRUCTOR:
                                constructors.add(node);
                                break;
                        }
                    }

                    children.add(new BodyClass(sourceLine, sourceCharPos, text, attributes, methods, constructors));
                    break;
                case BloodParser.BODY_CONSTRUCTOR:
                    instructions = new ArrayList<>();
                    if (tree.getChildCount() > 0 && tree.getChild(0).getType() == BloodParser.DEF_LVARS) {
                        Tree declarationsTree = tree.getChild(0);
                        ArrayList<Node> declarations = new ArrayList<>();
                        for (int i = 0; i < declarationsTree.getChildCount(); i++) {
                            declarations.add(buildAST(declarationsTree.getChild(i)).get(0));
                        }
                        for (int i = 1; i < tree.getChildCount(); i++) {
                            instructions.add(buildAST(tree.getChild(i)).get(0));
                        }
                        children.add(new Block(sourceLine, sourceCharPos, text, declarations, instructions));
                    } else {
                        for (int i = 0; i < tree.getChildCount(); i++) {
                            instructions.add(buildAST(tree.getChild(i)).get(0));
                        }
                        children.add(new Block(sourceLine, sourceCharPos, text, instructions));
                    }
                    break;
                case BloodParser.BODY_METHOD:
                    if (tree.getChild(0).getType() == BloodParser.BLOCK) {
                        /* directly add body as Block node */
                        children.add(buildAST(tree.getChild(0)).get(0));
                    } else {
                        Node expression = buildAST(tree.getChild(0)).get(0);
                        children.add(new BodyMethodExpression(sourceLine, sourceCharPos, text, expression));
                    }
                    break;
                case BloodParser.CAST:
                    className = tree.getChild(0).getText();
                    child = buildAST(tree.getChild(1)).get(0);
                    children.add(new Cast(sourceLine, sourceCharPos, text, className, child));
                    break;
                case BloodParser.CLASSDEF:
                    className = tree.getChild(0).getText();
                    ArrayList<ParamClass> classParameters = new ArrayList<>();
                    Tree parametersTree = tree.getChild(1);
                    for (int i = 0; i < parametersTree.getChildCount(); i++) {
                        classParameters.add((ParamClass) buildAST(parametersTree.getChild(i)).get(0));
                    }
                    String superClass = null;
                    Tree bodyTree;
                    if (tree.getChild(2).getType() == BloodParser.SUPERCLASS) {
                        superClass = tree.getChild(2).getChild(0).getText();
                        bodyTree = tree.getChild(3);
                    } else {
                        bodyTree = tree.getChild(2);
                    }
                    Node bodyClass = buildAST(bodyTree).get(0);
                    children.add(new ClassDef(sourceLine, sourceCharPos, text, className, classParameters, superClass, bodyClass));
                    break;
                case BloodParser.DEF_ATTRIBUTE:
                    boolean staticAttribute = tree.getChild(0).getChildCount() > 0 && tree.getChild(0).getChild(0).getType() == BloodParser.STATIC;
                    name = tree.getChild(1).getText();
                    className = tree.getChild(2).getText();
                    Node expression = null;
                    if (tree.getChildCount() > 3) {
                        expression = buildAST(tree.getChild(3)).get(0);
                    }
                    children.add(new DefAttribute(sourceLine, sourceCharPos, text, staticAttribute, name, className, expression));
                    break;
                case BloodParser.DEF_CONSTRUCTOR:
                    name = tree.getChild(0).getText();
                    ArrayList<ParamConstructor> args = new ArrayList<>();
                    Tree argsTree = tree.getChild(1);
                    for (int i = 0; i < argsTree.getChildCount(); i++) {
                        args.add((ParamConstructor) buildAST(argsTree.getChild(i)).get(0));
                    }
                    Node superClassInit = null;
                    Node bloc;
                    if (tree.getChild(2).getType() == BloodParser.SUPERCLASS_INIT) {
                        superClassInit = buildAST(tree.getChild(2)).get(0);
                        bloc = buildAST(tree.getChild(3)).get(0);
                    } else {
                        bloc = buildAST(tree.getChild(2)).get(0);
                    }
                    children.add(new DefConstructor(sourceLine, sourceCharPos, text, name, args, superClassInit, bloc));
                    break;
                case BloodParser.DEF_LVAR:
                    Node lVal = buildAST(tree.getChild(0)).get(0);
                    String lValClass = tree.getChild(1).getText();
                    if (tree.getChildCount() == 3) {
                        Node initializer = buildAST(tree.getChild(2)).get(0);
                        children.add(new DefLVal(sourceLine, sourceCharPos, text, lVal, lValClass, initializer));
                    } else {
                        children.add(new DefLVal(sourceLine, sourceCharPos, text, lVal, lValClass));
                    }
                    break;
                case BloodParser.DEF_LVARS:
                    treeChildren = new ArrayList<>();
                    for (int i = 0; i < tree.getChildCount(); i++) {
                        treeChildren.add(buildAST(tree.getChild(i)).get(0));
                    }
                    children.add(new DefLVals(sourceLine, sourceCharPos, text, treeChildren));
                    break;
                case BloodParser.DEF_METHOD:
                    boolean staticProp = false;
                    boolean overrideProp = false;
                    Tree propsTree = tree.getChild(0);
                    if (propsTree.getChildCount() > 0) {
                        /* static and override are mutually exclusive in the grammar */
                        switch (propsTree.getChild(0).getType()) {
                            case BloodParser.STATIC:
                                staticProp = true;
                                break;
                            case BloodParser.OVERRIDE:
                                overrideProp = true;
                                break;
                        }
                    }
                    name = tree.getChild(1).getText();
                    Node paramsMethod = buildAST(tree.getChild(2)).get(0);
                    String returnType = null;
                    Tree bodyMethodTree;
                    if (tree.getChild(3).getType() != BloodParser.BODY_METHOD) {
                        /* child is return type */
                        returnType = tree.getChild(3).getText();
                        bodyMethodTree = tree.getChild(4);
                    } else {
                        bodyMethodTree = tree.getChild(3);
                    }
                    Node bodyMethod = buildAST(bodyMethodTree).get(0);
                    children.add(new DefMethod(sourceLine, sourceCharPos, text, staticProp, overrideProp, name, paramsMethod, returnType, bodyMethod));
                    break;
                case BloodParser.IF:
                    Node condition = buildAST(tree.getChild(0)).get(0);
                    Node thenNode = null;
                    if (tree.getChild(1).getChildCount() > 0) {
                        thenNode = buildAST(tree.getChild(1).getChild(0)).get(0);
                    }
                    Node elseNode = null;
                    if (tree.getChild(2).getChildCount() > 0) {
                        elseNode = buildAST(tree.getChild(2).getChild(0)).get(0);
                    }
                    children.add(new IfNode(sourceLine, sourceCharPos, text, condition, thenNode, elseNode));
                    break;
                case BloodParser.INSTANTIATE:
                    String targetClass = tree.getChild(0).getText();
                    Node instArgs = buildAST(tree.getChild(1)).get(0);
                    children.add(new Instantiate(sourceLine, sourceCharPos, text, targetClass, instArgs));
                    break;
                case BloodParser.ID:
                    children.add(new Identifier(sourceLine, sourceCharPos, text));
                    break;
                case BloodParser.INT:
                    int value = Integer.parseInt(tree.getText());
                    children.add(new IntegerNode(sourceLine, sourceCharPos, text, value));
                    break;
                case BloodParser.MESSAGE:
                    name = tree.getChild(0).getText();
                    message = tree.getChildCount() > 1 ? buildAST(tree.getChild(1)).get(0) : null;
                    children.add(new Message(sourceLine, sourceCharPos, text, name, message));
                    break;
                case BloodParser.MESSAGE_BASE:
                    message = buildAST(tree.getChild(1)).get(0);
                    if (tree.getChild(0).getType() == BloodParser.IDCLASS) {
                        className = tree.getChild(0).getText();
                        children.add(new MessageBaseClass(sourceLine, sourceCharPos, text, className, message));
                    } else {
                        expression = buildAST(tree.getChild(0)).get(0);
                        children.add(new MessageBaseExpr(sourceLine, sourceCharPos, text, expression, message));
                    }
                    break;
                case BloodParser.MESSAGE_CALL:
                    String calledMethod = tree.getChild(0).getText();
                    Node callArgs = buildAST(tree.getChild(1)).get(0);
                    message = tree.getChildCount() > 2 ? buildAST(tree.getChild(2)).get(0) : null;
                    children.add(new MessageCall(sourceLine, sourceCharPos, text, calledMethod, callArgs, message));
                    break;
                case BloodParser.OP_ADD:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPAdd(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_AND:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPAnd(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_CAT:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPCat(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_DIV:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPDiv(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_EQU:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPEqu(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_GEQ:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPGeq(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_GRT:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPGrt(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_LEQ:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPLeq(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_LES:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPLes(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_MOD:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPMod(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_MUL:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPMul(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_NEG:
                    operand = buildAST(tree.getChild(0)).get(0);
                    children.add(new OPNeg(sourceLine, sourceCharPos, text, operand));
                    break;
                case BloodParser.OP_NEQ:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPNeq(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_NOT:
                    operand = buildAST(tree.getChild(0)).get(0);
                    children.add(new OPNot(sourceLine, sourceCharPos, text, operand));
                    break;
                case BloodParser.OP_OR:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPOr(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_SHL:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPShl(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_SHR:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPShr(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_SUB:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPSub(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.OP_XOR:
                    leftOperand = buildAST(tree.getChild(0)).get(0);
                    rightOperand = buildAST(tree.getChild(1)).get(0);
                    children.add(new OPXor(sourceLine, sourceCharPos, text, leftOperand, rightOperand));
                    break;
                case BloodParser.PROGRAM:
                    ArrayList<Node> classes = new ArrayList<>();
                    int j = 0;
                    for (; j < tree.getChildCount() && tree.getChild(j).getType() == BloodParser.CLASSDEF; j++) {
                        classes.add(buildAST(tree.getChild(j)).get(0));
                    }
                    instructions = new ArrayList<>();
                    for (; j < tree.getChildCount(); j++) {
                        instructions.add(buildAST(tree.getChild(j)).get(0));
                    }
                    children.add(new Root(sourceLine, sourceCharPos, text, classes, instructions));
                    break;
                case BloodParser.PARAM_CLASS:
                    if (tree.getChildCount() == 2) {
                        name = tree.getChild(0).getText();
                        type = tree.getChild(1).getText();
                        children.add(new ParamClass(sourceLine, sourceCharPos, text, name, type, false));
                    } else {
                        name = tree.getChild(1).getText();
                        type = tree.getChild(2).getText();
                        children.add(new ParamClass(sourceLine, sourceCharPos, text, name, type, true));
                    }
                    break;
                case BloodParser.PARAM_CONSTRUCTOR:
                    if (tree.getChildCount() == 2) {
                        name = tree.getChild(0).getText();
                        type = tree.getChild(1).getText();
                        children.add(new ParamConstructor(sourceLine, sourceCharPos, text, name, type, false));
                    } else {
                        name = tree.getChild(1).getText();
                        type = tree.getChild(2).getText();
                        children.add(new ParamConstructor(sourceLine, sourceCharPos, text, name, type, true));
                    }
                    break;
                case BloodParser.PARAM_METHOD:
                    name = tree.getChild(0).getText();
                    type = tree.getChild(1).getText();
                    children.add(new ParamMethod(sourceLine, sourceCharPos, text, name, type));
                    break;
                case BloodParser.PARAMS_METHOD:
                    ArrayList<ParamMethod> methodParams = new ArrayList<>();
                    for (int i = 0; i < tree.getChildCount(); i++) {
                        methodParams.add((ParamMethod) buildAST(tree.getChild(i)).get(0));
                    }
                    children.add(new ParamsMethod(sourceLine, sourceCharPos, text, methodParams));
                    break;
                case BloodParser.RESULT:
                    children.add(new ResultNode(sourceLine, sourceCharPos, text));
                    break;
                case BloodParser.RETURN:
                    children.add(new ReturnNode(sourceLine, sourceCharPos, text));
                    break;
                case BloodParser.STRING:
                    children.add(new StringNode(sourceLine, sourceCharPos, text));
                    break;
                case BloodParser.SUPERCLASS:
                    className = tree.getChild(0).getText();
                    children.add(new SuperClass(sourceLine, sourceCharPos, text, className));
                    break;
                case BloodParser.SUPERCLASS_INIT:
                    className = tree.getChild(0).getText();
                    argsNode = buildAST(tree.getChild(1)).get(0);
                    children.add(new SuperClassInit(sourceLine, sourceCharPos, text, className, argsNode));
                    break;
                case BloodParser.SUPER:
                    children.add(new SuperNode(sourceLine, sourceCharPos, text));
                    break;
                case BloodParser.THIS:
                    children.add(new ThisNode(sourceLine, sourceCharPos, text));
                    break;
                case BloodParser.WHILE:
                    Node loopExpression = buildAST(tree.getChild(0)).get(0);
                    Node instruction = buildAST(tree.getChild(1)).get(0);
                    children.add(new While(sourceLine, sourceCharPos, text, loopExpression, instruction));
                    break;
            }
        } catch (Exception e) {
            System.err.printf("[%d:%d] AST build error on node %d with text '%s'.\n", sourceLine, sourceCharPos, tree.getType(), tree.getText());
            throw e;
        }

        if (children.size() == 0) {
            throw new MalformedTreeException(String.format(
                    "[%d:%d] AST build error: empty tree on node %d with text '%s'.\n", sourceLine, sourceCharPos, tree.getType(), tree.getText()
            ));
        }

        return children;
    }

    public Root buildAST() throws MalformedTreeException {
        List<Node> nodes = buildAST(tree);

        Node first = nodes.get(0);
        if (!(first instanceof Root)) {
            throw new MalformedTreeException("AST build error: no root node found.");
        }

        return (Root)first;
    }
}
