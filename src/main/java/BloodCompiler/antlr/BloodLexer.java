// $ANTLR null D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g 2021-04-15 00:11:58

package BloodCompiler.antlr;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class BloodLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int T__89=89;
	public static final int T__90=90;
	public static final int T__91=91;
	public static final int T__92=92;
	public static final int T__93=93;
	public static final int T__94=94;
	public static final int T__95=95;
	public static final int T__96=96;
	public static final int T__97=97;
	public static final int T__98=98;
	public static final int T__99=99;
	public static final int T__100=100;
	public static final int T__101=101;
	public static final int T__102=102;
	public static final int T__103=103;
	public static final int T__104=104;
	public static final int T__105=105;
	public static final int T__106=106;
	public static final int T__107=107;
	public static final int T__108=108;
	public static final int T__109=109;
	public static final int T__110=110;
	public static final int T__111=111;
	public static final int T__112=112;
	public static final int T__113=113;
	public static final int T__114=114;
	public static final int T__115=115;
	public static final int ARGS=4;
	public static final int ASSIGNMENT=5;
	public static final int ATTR_PROPS=6;
	public static final int BLOCK=7;
	public static final int BODY_CLASS=8;
	public static final int BODY_CONSTRUCTOR=9;
	public static final int BODY_METHOD=10;
	public static final int CAST=11;
	public static final int CLASSDEF=12;
	public static final int COMMENT=13;
	public static final int DEF_ATTRIBUTE=14;
	public static final int DEF_CONSTRUCTOR=15;
	public static final int DEF_LVAR=16;
	public static final int DEF_LVARS=17;
	public static final int DEF_METHOD=18;
	public static final int ELSE=19;
	public static final int EXPRESSION=20;
	public static final int EXPRESSIONS=21;
	public static final int ID=22;
	public static final int IDCLASS=23;
	public static final int IF=24;
	public static final int INSTANTIATE=25;
	public static final int INT=26;
	public static final int MESSAGE=27;
	public static final int MESSAGE_BASE=28;
	public static final int MESSAGE_CALL=29;
	public static final int METHOD_PROPS=30;
	public static final int NEWLINE=31;
	public static final int OP_ADD=32;
	public static final int OP_AND=33;
	public static final int OP_CAT=34;
	public static final int OP_DIV=35;
	public static final int OP_EQU=36;
	public static final int OP_GEQ=37;
	public static final int OP_GRT=38;
	public static final int OP_LEQ=39;
	public static final int OP_LES=40;
	public static final int OP_MOD=41;
	public static final int OP_MUL=42;
	public static final int OP_NEG=43;
	public static final int OP_NEQ=44;
	public static final int OP_NOT=45;
	public static final int OP_OR=46;
	public static final int OP_SHL=47;
	public static final int OP_SHR=48;
	public static final int OP_SUB=49;
	public static final int OP_XOR=50;
	public static final int OVERRIDE=51;
	public static final int PARAMS_CLASS=52;
	public static final int PARAMS_CONSTRUCTOR=53;
	public static final int PARAMS_METHOD=54;
	public static final int PARAM_CLASS=55;
	public static final int PARAM_CONSTRUCTOR=56;
	public static final int PARAM_METHOD=57;
	public static final int PROGRAM=58;
	public static final int RESULT=59;
	public static final int RETURN=60;
	public static final int STATIC=61;
	public static final int STRING=62;
	public static final int SUPER=63;
	public static final int SUPERCLASS=64;
	public static final int SUPERCLASS_INIT=65;
	public static final int THEN=66;
	public static final int THIS=67;
	public static final int VAR=68;
	public static final int WHILE=69;
	public static final int WS=70;

	  @Override
	  public void reportError(RecognitionException e) {
	    throw new RuntimeException(e);
	  }


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public BloodLexer() {} 
	public BloodLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public BloodLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g"; }

	// $ANTLR start "T__71"
	public final void mT__71() throws RecognitionException {
		try {
			int _type = T__71;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:14:7: ( '%' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:14:9: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__71"

	// $ANTLR start "T__72"
	public final void mT__72() throws RecognitionException {
		try {
			int _type = T__72;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:15:7: ( '&' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:15:9: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__72"

	// $ANTLR start "T__73"
	public final void mT__73() throws RecognitionException {
		try {
			int _type = T__73;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:16:7: ( '(' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:16:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__73"

	// $ANTLR start "T__74"
	public final void mT__74() throws RecognitionException {
		try {
			int _type = T__74;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:17:7: ( ')' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:17:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__74"

	// $ANTLR start "T__75"
	public final void mT__75() throws RecognitionException {
		try {
			int _type = T__75;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:18:7: ( '*' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:18:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__75"

	// $ANTLR start "T__76"
	public final void mT__76() throws RecognitionException {
		try {
			int _type = T__76;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:19:7: ( '+' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:19:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__76"

	// $ANTLR start "T__77"
	public final void mT__77() throws RecognitionException {
		try {
			int _type = T__77;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:20:7: ( ',' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:20:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__77"

	// $ANTLR start "T__78"
	public final void mT__78() throws RecognitionException {
		try {
			int _type = T__78;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:21:7: ( '-' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:21:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__78"

	// $ANTLR start "T__79"
	public final void mT__79() throws RecognitionException {
		try {
			int _type = T__79;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:22:7: ( '.' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:22:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__79"

	// $ANTLR start "T__80"
	public final void mT__80() throws RecognitionException {
		try {
			int _type = T__80;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:23:7: ( '/' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:23:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__80"

	// $ANTLR start "T__81"
	public final void mT__81() throws RecognitionException {
		try {
			int _type = T__81;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:24:7: ( ':' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:24:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__81"

	// $ANTLR start "T__82"
	public final void mT__82() throws RecognitionException {
		try {
			int _type = T__82;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:25:7: ( ':=' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:25:9: ':='
			{
			match(":="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__82"

	// $ANTLR start "T__83"
	public final void mT__83() throws RecognitionException {
		try {
			int _type = T__83;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:26:7: ( ';' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:26:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__83"

	// $ANTLR start "T__84"
	public final void mT__84() throws RecognitionException {
		try {
			int _type = T__84;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:27:7: ( '<' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:27:9: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__84"

	// $ANTLR start "T__85"
	public final void mT__85() throws RecognitionException {
		try {
			int _type = T__85;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:28:7: ( '<<' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:28:9: '<<'
			{
			match("<<"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__85"

	// $ANTLR start "T__86"
	public final void mT__86() throws RecognitionException {
		try {
			int _type = T__86;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:29:7: ( '<=' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:29:9: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__86"

	// $ANTLR start "T__87"
	public final void mT__87() throws RecognitionException {
		try {
			int _type = T__87;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:30:7: ( '<>' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:30:9: '<>'
			{
			match("<>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__87"

	// $ANTLR start "T__88"
	public final void mT__88() throws RecognitionException {
		try {
			int _type = T__88;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:31:7: ( '=' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:31:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__88"

	// $ANTLR start "T__89"
	public final void mT__89() throws RecognitionException {
		try {
			int _type = T__89;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:32:7: ( '>' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:32:9: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__89"

	// $ANTLR start "T__90"
	public final void mT__90() throws RecognitionException {
		try {
			int _type = T__90;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:33:7: ( '>=' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:33:9: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__90"

	// $ANTLR start "T__91"
	public final void mT__91() throws RecognitionException {
		try {
			int _type = T__91;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:34:7: ( '>>' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:34:9: '>>'
			{
			match(">>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__91"

	// $ANTLR start "T__92"
	public final void mT__92() throws RecognitionException {
		try {
			int _type = T__92;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:35:7: ( 'and' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:35:9: 'and'
			{
			match("and"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__92"

	// $ANTLR start "T__93"
	public final void mT__93() throws RecognitionException {
		try {
			int _type = T__93;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:36:7: ( 'as' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:36:9: 'as'
			{
			match("as"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__93"

	// $ANTLR start "T__94"
	public final void mT__94() throws RecognitionException {
		try {
			int _type = T__94;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:37:7: ( 'class' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:37:9: 'class'
			{
			match("class"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__94"

	// $ANTLR start "T__95"
	public final void mT__95() throws RecognitionException {
		try {
			int _type = T__95;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:38:7: ( 'def' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:38:9: 'def'
			{
			match("def"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__95"

	// $ANTLR start "T__96"
	public final void mT__96() throws RecognitionException {
		try {
			int _type = T__96;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:39:7: ( 'do' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:39:9: 'do'
			{
			match("do"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__96"

	// $ANTLR start "T__97"
	public final void mT__97() throws RecognitionException {
		try {
			int _type = T__97;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:40:7: ( 'else' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:40:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__97"

	// $ANTLR start "T__98"
	public final void mT__98() throws RecognitionException {
		try {
			int _type = T__98;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:41:7: ( 'extends' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:41:9: 'extends'
			{
			match("extends"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__98"

	// $ANTLR start "T__99"
	public final void mT__99() throws RecognitionException {
		try {
			int _type = T__99;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:42:7: ( 'if' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:42:9: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__99"

	// $ANTLR start "T__100"
	public final void mT__100() throws RecognitionException {
		try {
			int _type = T__100;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:43:8: ( 'is' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:43:10: 'is'
			{
			match("is"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__100"

	// $ANTLR start "T__101"
	public final void mT__101() throws RecognitionException {
		try {
			int _type = T__101;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:44:8: ( 'new' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:44:10: 'new'
			{
			match("new"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__101"

	// $ANTLR start "T__102"
	public final void mT__102() throws RecognitionException {
		try {
			int _type = T__102;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:45:8: ( 'or' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:45:10: 'or'
			{
			match("or"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__102"

	// $ANTLR start "T__103"
	public final void mT__103() throws RecognitionException {
		try {
			int _type = T__103;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:46:8: ( 'override' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:46:10: 'override'
			{
			match("override"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__103"

	// $ANTLR start "T__104"
	public final void mT__104() throws RecognitionException {
		try {
			int _type = T__104;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:47:8: ( 'result' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:47:10: 'result'
			{
			match("result"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__104"

	// $ANTLR start "T__105"
	public final void mT__105() throws RecognitionException {
		try {
			int _type = T__105;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:48:8: ( 'return' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:48:10: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__105"

	// $ANTLR start "T__106"
	public final void mT__106() throws RecognitionException {
		try {
			int _type = T__106;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:49:8: ( 'static' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:49:10: 'static'
			{
			match("static"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__106"

	// $ANTLR start "T__107"
	public final void mT__107() throws RecognitionException {
		try {
			int _type = T__107;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:50:8: ( 'super' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:50:10: 'super'
			{
			match("super"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__107"

	// $ANTLR start "T__108"
	public final void mT__108() throws RecognitionException {
		try {
			int _type = T__108;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:51:8: ( 'then' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:51:10: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__108"

	// $ANTLR start "T__109"
	public final void mT__109() throws RecognitionException {
		try {
			int _type = T__109;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:52:8: ( 'this' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:52:10: 'this'
			{
			match("this"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__109"

	// $ANTLR start "T__110"
	public final void mT__110() throws RecognitionException {
		try {
			int _type = T__110;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:53:8: ( 'var' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:53:10: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__110"

	// $ANTLR start "T__111"
	public final void mT__111() throws RecognitionException {
		try {
			int _type = T__111;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:54:8: ( 'while' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:54:10: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__111"

	// $ANTLR start "T__112"
	public final void mT__112() throws RecognitionException {
		try {
			int _type = T__112;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:55:8: ( 'xor' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:55:10: 'xor'
			{
			match("xor"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__112"

	// $ANTLR start "T__113"
	public final void mT__113() throws RecognitionException {
		try {
			int _type = T__113;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:56:8: ( '{' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:56:10: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__113"

	// $ANTLR start "T__114"
	public final void mT__114() throws RecognitionException {
		try {
			int _type = T__114;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:57:8: ( '}' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:57:10: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__114"

	// $ANTLR start "T__115"
	public final void mT__115() throws RecognitionException {
		try {
			int _type = T__115;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:58:8: ( '~' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:58:10: '~'
			{
			match('~'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__115"

	// $ANTLR start "IDCLASS"
	public final void mIDCLASS() throws RecognitionException {
		try {
			int _type = IDCLASS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:331:9: ( ( 'A' .. 'Z' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:331:11: ( 'A' .. 'Z' ) ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:331:21: ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IDCLASS"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:334:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:334:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:334:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')||(LA2_0 >= 'A' && LA2_0 <= 'Z')||LA2_0=='_'||(LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop2;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:337:5: ( ( '0' .. '9' )+ )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:337:7: ( '0' .. '9' )+
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:337:7: ( '0' .. '9' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:340:9: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0=='/') ) {
				int LA7_1 = input.LA(2);
				if ( (LA7_1=='/') ) {
					alt7=1;
				}
				else if ( (LA7_1=='*') ) {
					alt7=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 7, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:340:11: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					match("//"); 

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:340:16: (~ ( '\\n' | '\\r' ) )*
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= '\u0000' && LA4_0 <= '\t')||(LA4_0 >= '\u000B' && LA4_0 <= '\f')||(LA4_0 >= '\u000E' && LA4_0 <= '\uFFFF')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop4;
						}
					}

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:340:30: ( '\\r' )?
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0=='\r') ) {
						alt5=1;
					}
					switch (alt5) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:340:30: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					_channel=HIDDEN;
					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:341:4: '/*' ( options {greedy=false; } : . )* '*/'
					{
					match("/*"); 

					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:341:9: ( options {greedy=false; } : . )*
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( (LA6_0=='*') ) {
							int LA6_1 = input.LA(2);
							if ( (LA6_1=='/') ) {
								alt6=2;
							}
							else if ( ((LA6_1 >= '\u0000' && LA6_1 <= '.')||(LA6_1 >= '0' && LA6_1 <= '\uFFFF')) ) {
								alt6=1;
							}

						}
						else if ( ((LA6_0 >= '\u0000' && LA6_0 <= ')')||(LA6_0 >= '+' && LA6_0 <= '\uFFFF')) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:341:37: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop6;
						}
					}

					match("*/"); 

					_channel=HIDDEN;
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:344:4: ( ( ' ' | '\\t' | '\\r' ~ ( '\\n' ) ) )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:344:6: ( ' ' | '\\t' | '\\r' ~ ( '\\n' ) )
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:344:6: ( ' ' | '\\t' | '\\r' ~ ( '\\n' ) )
			int alt8=3;
			switch ( input.LA(1) ) {
			case ' ':
				{
				alt8=1;
				}
				break;
			case '\t':
				{
				alt8=2;
				}
				break;
			case '\r':
				{
				alt8=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}
			switch (alt8) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:344:7: ' '
					{
					match(' '); 
					}
					break;
				case 2 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:344:13: '\\t'
					{
					match('\t'); 
					}
					break;
				case 3 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:344:20: '\\r' ~ ( '\\n' )
					{
					match('\r'); 
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "NEWLINE"
	public final void mNEWLINE() throws RecognitionException {
		try {
			int _type = NEWLINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:347:9: ( ( '\\r' )? '\\n' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:347:11: ( '\\r' )? '\\n'
			{
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:347:11: ( '\\r' )?
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0=='\r') ) {
				alt9=1;
			}
			switch (alt9) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:347:11: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEWLINE"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:350:8: ( '\"' (~ ( '\"' ) )* '\"' )
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:350:10: '\"' (~ ( '\"' ) )* '\"'
			{
			match('\"'); 
			// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:350:14: (~ ( '\"' ) )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( ((LA10_0 >= '\u0000' && LA10_0 <= '!')||(LA10_0 >= '#' && LA10_0 <= '\uFFFF')) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop10;
				}
			}

			match('\"'); 
			setText(getText().substring(1, getText().length()-1));
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	@Override
	public void mTokens() throws RecognitionException {
		// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:8: ( T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | IDCLASS | ID | INT | COMMENT | WS | NEWLINE | STRING )
		int alt11=52;
		alt11 = dfa11.predict(input);
		switch (alt11) {
			case 1 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:10: T__71
				{
				mT__71(); 

				}
				break;
			case 2 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:16: T__72
				{
				mT__72(); 

				}
				break;
			case 3 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:22: T__73
				{
				mT__73(); 

				}
				break;
			case 4 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:28: T__74
				{
				mT__74(); 

				}
				break;
			case 5 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:34: T__75
				{
				mT__75(); 

				}
				break;
			case 6 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:40: T__76
				{
				mT__76(); 

				}
				break;
			case 7 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:46: T__77
				{
				mT__77(); 

				}
				break;
			case 8 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:52: T__78
				{
				mT__78(); 

				}
				break;
			case 9 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:58: T__79
				{
				mT__79(); 

				}
				break;
			case 10 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:64: T__80
				{
				mT__80(); 

				}
				break;
			case 11 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:70: T__81
				{
				mT__81(); 

				}
				break;
			case 12 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:76: T__82
				{
				mT__82(); 

				}
				break;
			case 13 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:82: T__83
				{
				mT__83(); 

				}
				break;
			case 14 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:88: T__84
				{
				mT__84(); 

				}
				break;
			case 15 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:94: T__85
				{
				mT__85(); 

				}
				break;
			case 16 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:100: T__86
				{
				mT__86(); 

				}
				break;
			case 17 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:106: T__87
				{
				mT__87(); 

				}
				break;
			case 18 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:112: T__88
				{
				mT__88(); 

				}
				break;
			case 19 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:118: T__89
				{
				mT__89(); 

				}
				break;
			case 20 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:124: T__90
				{
				mT__90(); 

				}
				break;
			case 21 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:130: T__91
				{
				mT__91(); 

				}
				break;
			case 22 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:136: T__92
				{
				mT__92(); 

				}
				break;
			case 23 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:142: T__93
				{
				mT__93(); 

				}
				break;
			case 24 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:148: T__94
				{
				mT__94(); 

				}
				break;
			case 25 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:154: T__95
				{
				mT__95(); 

				}
				break;
			case 26 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:160: T__96
				{
				mT__96(); 

				}
				break;
			case 27 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:166: T__97
				{
				mT__97(); 

				}
				break;
			case 28 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:172: T__98
				{
				mT__98(); 

				}
				break;
			case 29 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:178: T__99
				{
				mT__99(); 

				}
				break;
			case 30 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:184: T__100
				{
				mT__100(); 

				}
				break;
			case 31 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:191: T__101
				{
				mT__101(); 

				}
				break;
			case 32 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:198: T__102
				{
				mT__102(); 

				}
				break;
			case 33 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:205: T__103
				{
				mT__103(); 

				}
				break;
			case 34 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:212: T__104
				{
				mT__104(); 

				}
				break;
			case 35 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:219: T__105
				{
				mT__105(); 

				}
				break;
			case 36 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:226: T__106
				{
				mT__106(); 

				}
				break;
			case 37 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:233: T__107
				{
				mT__107(); 

				}
				break;
			case 38 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:240: T__108
				{
				mT__108(); 

				}
				break;
			case 39 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:247: T__109
				{
				mT__109(); 

				}
				break;
			case 40 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:254: T__110
				{
				mT__110(); 

				}
				break;
			case 41 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:261: T__111
				{
				mT__111(); 

				}
				break;
			case 42 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:268: T__112
				{
				mT__112(); 

				}
				break;
			case 43 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:275: T__113
				{
				mT__113(); 

				}
				break;
			case 44 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:282: T__114
				{
				mT__114(); 

				}
				break;
			case 45 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:289: T__115
				{
				mT__115(); 

				}
				break;
			case 46 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:296: IDCLASS
				{
				mIDCLASS(); 

				}
				break;
			case 47 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:304: ID
				{
				mID(); 

				}
				break;
			case 48 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:307: INT
				{
				mINT(); 

				}
				break;
			case 49 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:311: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 50 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:319: WS
				{
				mWS(); 

				}
				break;
			case 51 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:322: NEWLINE
				{
				mNEWLINE(); 

				}
				break;
			case 52 :
				// D:\\Git\\TN2A\\PCL\\pcl-colomb3u\\grammaire\\Blood.g:1:330: STRING
				{
				mSTRING(); 

				}
				break;

		}
	}


	protected DFA11 dfa11 = new DFA11(this);
	static final String DFA11_eotS =
		"\12\uffff\1\50\1\52\1\uffff\1\56\1\uffff\1\61\15\41\3\uffff\1\106\21\uffff"+
		"\1\41\1\110\2\41\1\113\2\41\1\116\1\117\1\41\1\121\10\41\1\106\1\uffff"+
		"\1\134\1\uffff\1\41\1\136\1\uffff\2\41\2\uffff\1\141\1\uffff\7\41\1\151"+
		"\1\41\1\153\1\uffff\1\41\1\uffff\1\155\1\41\1\uffff\5\41\1\164\1\165\1"+
		"\uffff\1\41\1\uffff\1\167\1\uffff\5\41\1\175\2\uffff\1\176\1\uffff\2\41"+
		"\1\u0081\1\u0082\1\u0083\2\uffff\1\u0084\1\41\4\uffff\1\u0086\1\uffff";
	static final String DFA11_eofS =
		"\u0087\uffff";
	static final String DFA11_minS =
		"\1\11\11\uffff\1\52\1\75\1\uffff\1\74\1\uffff\1\75\1\156\1\154\1\145\1"+
		"\154\1\146\1\145\1\162\1\145\1\164\1\150\1\141\1\150\1\157\3\uffff\1\60"+
		"\3\uffff\1\0\15\uffff\1\144\1\60\1\141\1\146\1\60\1\163\1\164\2\60\1\167"+
		"\1\60\1\145\1\163\1\141\1\160\1\145\1\162\1\151\1\162\1\60\1\uffff\1\60"+
		"\1\uffff\1\163\1\60\1\uffff\2\145\2\uffff\1\60\1\uffff\1\162\2\165\1\164"+
		"\1\145\1\156\1\163\1\60\1\154\1\60\1\uffff\1\163\1\uffff\1\60\1\156\1"+
		"\uffff\1\162\1\154\1\162\1\151\1\162\2\60\1\uffff\1\145\1\uffff\1\60\1"+
		"\uffff\1\144\1\151\1\164\1\156\1\143\1\60\2\uffff\1\60\1\uffff\1\163\1"+
		"\144\3\60\2\uffff\1\60\1\145\4\uffff\1\60\1\uffff";
	static final String DFA11_maxS =
		"\1\176\11\uffff\1\57\1\75\1\uffff\1\76\1\uffff\1\76\1\163\1\154\1\157"+
		"\1\170\1\163\1\145\1\166\1\145\1\165\1\150\1\141\1\150\1\157\3\uffff\1"+
		"\172\3\uffff\1\uffff\15\uffff\1\144\1\172\1\141\1\146\1\172\1\163\1\164"+
		"\2\172\1\167\1\172\1\145\1\164\1\141\1\160\1\151\1\162\1\151\1\162\1\172"+
		"\1\uffff\1\172\1\uffff\1\163\1\172\1\uffff\2\145\2\uffff\1\172\1\uffff"+
		"\1\162\2\165\1\164\1\145\1\156\1\163\1\172\1\154\1\172\1\uffff\1\163\1"+
		"\uffff\1\172\1\156\1\uffff\1\162\1\154\1\162\1\151\1\162\2\172\1\uffff"+
		"\1\145\1\uffff\1\172\1\uffff\1\144\1\151\1\164\1\156\1\143\1\172\2\uffff"+
		"\1\172\1\uffff\1\163\1\144\3\172\2\uffff\1\172\1\145\4\uffff\1\172\1\uffff";
	static final String DFA11_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\2\uffff\1\15\1\uffff\1"+
		"\22\16\uffff\1\53\1\54\1\55\1\uffff\1\57\1\60\1\62\1\uffff\1\63\1\64\1"+
		"\61\1\12\1\14\1\13\1\17\1\20\1\21\1\16\1\24\1\25\1\23\24\uffff\1\56\1"+
		"\uffff\1\27\2\uffff\1\32\2\uffff\1\35\1\36\1\uffff\1\40\12\uffff\1\26"+
		"\1\uffff\1\31\2\uffff\1\37\7\uffff\1\50\1\uffff\1\52\1\uffff\1\33\6\uffff"+
		"\1\46\1\47\1\uffff\1\30\5\uffff\1\45\1\51\2\uffff\1\42\1\43\1\44\1\34"+
		"\1\uffff\1\41";
	static final String DFA11_specialS =
		"\44\uffff\1\0\142\uffff}>";
	static final String[] DFA11_transitionS = {
			"\1\43\1\45\2\uffff\1\44\22\uffff\1\43\1\uffff\1\46\2\uffff\1\1\1\2\1"+
			"\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\12\42\1\13\1\14\1\15\1\16\1"+
			"\17\2\uffff\32\40\4\uffff\1\41\1\uffff\1\20\1\41\1\21\1\22\1\23\3\41"+
			"\1\24\4\41\1\25\1\26\2\41\1\27\1\30\1\31\1\41\1\32\1\33\1\34\2\41\1\35"+
			"\1\uffff\1\36\1\37",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\47\4\uffff\1\47",
			"\1\51",
			"",
			"\1\53\1\54\1\55",
			"",
			"\1\57\1\60",
			"\1\62\4\uffff\1\63",
			"\1\64",
			"\1\65\11\uffff\1\66",
			"\1\67\13\uffff\1\70",
			"\1\71\14\uffff\1\72",
			"\1\73",
			"\1\74\3\uffff\1\75",
			"\1\76",
			"\1\77\1\100",
			"\1\101",
			"\1\102",
			"\1\103",
			"\1\104",
			"",
			"",
			"",
			"\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff\32\105",
			"",
			"",
			"",
			"\12\43\1\45\ufff5\43",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\107",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\111",
			"\1\112",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\114",
			"\1\115",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\120",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\122",
			"\1\123\1\124",
			"\1\125",
			"\1\126",
			"\1\127\3\uffff\1\130",
			"\1\131",
			"\1\132",
			"\1\133",
			"\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff\32\105",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\135",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\137",
			"\1\140",
			"",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\142",
			"\1\143",
			"\1\144",
			"\1\145",
			"\1\146",
			"\1\147",
			"\1\150",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\152",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\154",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\156",
			"",
			"\1\157",
			"\1\160",
			"\1\161",
			"\1\162",
			"\1\163",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\166",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\170",
			"\1\171",
			"\1\172",
			"\1\173",
			"\1\174",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\177",
			"\1\u0080",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\u0085",
			"",
			"",
			"",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			""
	};

	static final short[] DFA11_eot = DFA.unpackEncodedString(DFA11_eotS);
	static final short[] DFA11_eof = DFA.unpackEncodedString(DFA11_eofS);
	static final char[] DFA11_min = DFA.unpackEncodedStringToUnsignedChars(DFA11_minS);
	static final char[] DFA11_max = DFA.unpackEncodedStringToUnsignedChars(DFA11_maxS);
	static final short[] DFA11_accept = DFA.unpackEncodedString(DFA11_acceptS);
	static final short[] DFA11_special = DFA.unpackEncodedString(DFA11_specialS);
	static final short[][] DFA11_transition;

	static {
		int numStates = DFA11_transitionS.length;
		DFA11_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA11_transition[i] = DFA.unpackEncodedString(DFA11_transitionS[i]);
		}
	}

	protected class DFA11 extends DFA {

		public DFA11(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 11;
			this.eot = DFA11_eot;
			this.eof = DFA11_eof;
			this.min = DFA11_min;
			this.max = DFA11_max;
			this.accept = DFA11_accept;
			this.special = DFA11_special;
			this.transition = DFA11_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | T__89 | T__90 | T__91 | T__92 | T__93 | T__94 | T__95 | T__96 | T__97 | T__98 | T__99 | T__100 | T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | IDCLASS | ID | INT | COMMENT | WS | NEWLINE | STRING );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA11_36 = input.LA(1);
						s = -1;
						if ( ((LA11_36 >= '\u0000' && LA11_36 <= '\t')||(LA11_36 >= '\u000B' && LA11_36 <= '\uFFFF')) ) {s = 35;}
						else if ( (LA11_36=='\n') ) {s = 37;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 11, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
