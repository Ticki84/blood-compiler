package BloodCompiler.antlr;

public class MalformedTreeException extends Exception {
    public MalformedTreeException(String message) {
        super(message);
    }
}
