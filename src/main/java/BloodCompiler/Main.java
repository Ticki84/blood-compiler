package BloodCompiler;

import BloodCompiler.antlr.ANTLRParser;
import BloodCompiler.antlr.MalformedTreeException;
import BloodCompiler.assembly.ASMGenerator;
import BloodCompiler.assembly.InstructionWriter;
import BloodCompiler.assembly.visual.Launcher;
import BloodCompiler.ast.nodes.Root;
import BloodCompiler.logging.Log;
import BloodCompiler.semantic.SemanticChecker;
import BloodCompiler.symbolTable.TableFiller;
import BloodCompiler.symbolTable.TableManager;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.antlr.runtime.RecognitionException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class Main {
    private static TableManager tm;
    private static Root root;

    public static void main(String[] args) {
        ArgumentParser argParser = ArgumentParsers.newFor("BloodCompiler").build()
                .defaultHelp(false)
                .description("Compile a Blood file into microPIUP assembly.");
        argParser.addArgument("-d", "--debug")
                .action(Arguments.storeTrue())
                .help("Enables the display of debug messages")
                .setDefault(false);
        argParser.addArgument("-s", "--symbolTables")
                .action(Arguments.storeTrue())
                .help("Enables the display of the symbol tables")
                .setDefault(false);
        argParser.addArgument("-a", "--ast")
                .action(Arguments.storeTrue())
                .help("Enables printing the text AST")
                .setDefault(false);
        argParser.addArgument("file")
                .help("Blood source file")
                .required(true);

        Namespace ns;
        try {
            ns = argParser.parseArgs(args);
        } catch (ArgumentParserException e) {
            argParser.handleError(e);
            return;
        }

        Root root;
        try {
            root = parse(ns.getString("file"), ns.getBoolean("ast"));
        } catch (Exception e) {
            System.out.println("Error parsing tree.");
            e.printStackTrace();
            return;
        }

        if (!process(root, ns.getBoolean("debug"), ns.getBoolean("symbolTables"))) {
            System.err.println("Error: semantic checking has failed.");
            return;
        }
        if (!compile()) {
            System.err.println("Error: compilation has failed.");
            return;
        }
        run();
    }

    public static Root parse(String file, boolean ast) throws IOException, RecognitionException, MalformedTreeException {
        ANTLRParser parser = new ANTLRParser();
        parser.parseSource(file);
        if (ast)
            System.out.println(parser.getStringTree());
        root = parser.buildAST();

        return root;
    }

    public static boolean process(Root root, boolean debug, boolean symbolTables) {
        Log.initialize(debug);
        tm = new TableManager();
        TableFiller tf = new TableFiller(tm);
        tf.visit(root);
        tm.reset();
        SemanticChecker sc = new SemanticChecker(tm);
        sc.visit(root);

        boolean success = Log.success();

        if (!success)
            return false;

        tm.computeOffsets();

        if (symbolTables)
            Log.printSymbolTable(tm.getRootTable());

        return true;
    }

    public static boolean compile() {
        PrintWriter writer;
        try {
            writer = new PrintWriter("out.S", "UTF-8");
        } catch (FileNotFoundException e) {
            System.err.println("Fatal: assembly output file not found.");
            return false;
        } catch (UnsupportedEncodingException e) {
            System.err.println("Fatal: unsupported encoding for assembly output file.");
            return false;
        }

        InstructionWriter.initialize(writer);

        tm.reset();
        ASMGenerator g = new ASMGenerator(tm);
        g.initializeHeap();
        g.visit(root);

        InstructionWriter.finish();

        return true;
    }

    public static void run() {
        Launcher.run("out.S");
    }
}
