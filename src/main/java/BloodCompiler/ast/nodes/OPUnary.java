package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.Type;

public abstract class OPUnary extends Node {

    private final Node operand;
    private final String operator;
    private final Class<? extends Type> allowedType;

    public OPUnary(int sourceLine, int sourceCharPos, String text, Node operand, String operator, Class<? extends Type> allowedType) {
        super(sourceLine, sourceCharPos, text);
        this.operand = operand;
        this.operator = operator;
        this.allowedType = allowedType;
    }

    public Node getOperand() {
        return operand;
    }

    public String getOperator() {
        return operator;
    }

    public Class<? extends Type> getAllowedType() {
        return allowedType;
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}
