package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class Message extends Node {

    private final String name;
    private final Node message;
    private String baseClass;

    public Message(int sourceLine, int sourceCharPos, String text, String name, Node message) {
        super(sourceLine, sourceCharPos, text);
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public Node getMessage() {
        return message;
    }

    public void setBaseClass(String baseClass) {
        this.baseClass = baseClass;
    }

    public String getBaseClass() {
        return baseClass;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
