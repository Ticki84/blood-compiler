package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class SuperClassInit extends Node {

    private final String className;
    private final Node args;

    public SuperClassInit(int sourceLine, int sourceCharPos, String text, String className, Node args) {
        super(sourceLine, sourceCharPos, text);
        this.className = className;
        this.args = args;
    }

    public String getClassName() {
        return className;
    }

    public Node getArgs() {
        return args;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
