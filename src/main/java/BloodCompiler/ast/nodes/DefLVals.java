package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class DefLVals extends Node {

    private final ArrayList<Node> lVarDefs;

    public DefLVals(int sourceLine, int sourceCharPos, String text, ArrayList<Node> lVarDefs) {
        super(sourceLine, sourceCharPos, text);
        this.lVarDefs = lVarDefs;
    }

    public ArrayList<Node> getLVarDefs() {
        return lVarDefs;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
