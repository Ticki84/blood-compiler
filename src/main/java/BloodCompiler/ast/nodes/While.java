package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class While extends Node{

    private final Node expression;
    private final Node instruction;

    public While(int sourceLine, int sourceCharPos, String text, Node expression, Node instruction) {
        super(sourceLine, sourceCharPos, text);
        this.expression = expression;
        this.instruction = instruction;
    }

    public Node getExpression() {
        return expression;
    }

    public Node getInstruction() {
        return instruction;
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}