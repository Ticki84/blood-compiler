package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class ThisNode extends Node {

    public ThisNode(int sourceLine, int sourceCharPos, String text) {
        super(sourceLine, sourceCharPos, text);
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}
