package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.IntegerType;

public class OPXor extends OPBinary{

    public OPXor(int sourceLine, int sourceCharPos, String text, Node leftOperand, Node rightOperand) {
        super(sourceLine, sourceCharPos, text, leftOperand, rightOperand, "xor", IntegerType.class);
    }

    public void accept(Visitor v) {
        v.visit((OPBinary) this);
        v.visit(this);
    }
}