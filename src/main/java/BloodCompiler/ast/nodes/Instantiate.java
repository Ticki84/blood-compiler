package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class Instantiate extends Node {

    private final String classType;
    private final Node args;

    public Instantiate(int sourceLine, int sourceCharPos, String text, String classType, Node args) {
        super(sourceLine, sourceCharPos, text);
        this.classType = classType;
        this.args = args;
    }

    public String getClassType() {
        return classType;
    }

    public Node getArgs() {
        return args;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
