package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class SuperClass extends Node {

    private final String className;

    public SuperClass(int sourceLine, int sourceCharPos, String text, String className) {
        super(sourceLine, sourceCharPos, text);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
