package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class Root extends Node {
    private final ArrayList<Node> classes;
    private final ArrayList<Node> mainProgram;

    public Root(int sourceLine, int sourceCharPos, String text, ArrayList<Node> classes, ArrayList<Node> mainProgram) {
        super(sourceLine, sourceCharPos, text);
        this.classes = classes;
        this.mainProgram = mainProgram;
    }

    public ArrayList<Node> getClasses() {
        return classes;
    }

    public ArrayList<Node> getMainProgram() {
        return mainProgram;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
