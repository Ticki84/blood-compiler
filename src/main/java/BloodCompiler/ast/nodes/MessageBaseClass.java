package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class MessageBaseClass extends Node {

    private final Node message;
    private final String className;

    public MessageBaseClass(int sourceLine, int sourceCharPos, String text, String className, Node message) {
        super(sourceLine, sourceCharPos, text);
        this.message = message;
        this.className = className;
    }

    public Node getMessage() {
        return message;
    }

    public String getClassName() {
        return className;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
