package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class MessageCall extends Node {

    private final String calledMethod;
    private final Node args;
    private final Node message;
    private String baseClass;

    public MessageCall(int sourceLine, int sourceCharPos, String text, String calledMethod, Node args, Node message) {
        super(sourceLine, sourceCharPos, text);
        this.calledMethod = calledMethod;
        this.args = args;
        this.message = message;
    }

    public String getCalledMethod() {
        return calledMethod;
    }

    public Node getArgs() {
        return args;
    }

    public Node getMessage() {
        return message;
    }

    public void setBaseClass(String baseClass) {
        this.baseClass = baseClass;
    }

    public String getBaseClass() {
        return baseClass;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
