package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class Assignment extends Node {

    private final Node var;
    private final Node assignee;

    public Assignment(int sourceLine, int sourceCharPos, String text, Node var, Node assignee) {
        super(sourceLine, sourceCharPos, text);
        this.var = var;
        this.assignee = assignee;
    }

    public Node getVar() {
        return this.var;
    }

    public Node getAssignee() {
        return this.assignee;
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}
