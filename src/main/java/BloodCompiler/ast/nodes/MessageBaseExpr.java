package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class MessageBaseExpr extends Node {

    private final Node base;
    private final Node message;

    public MessageBaseExpr(int sourceLine, int sourceCharPos, String text, Node base, Node message) {
        super(sourceLine, sourceCharPos, text);
        this.base = base;
        this.message = message;
    }

    public Node getBase() {
        return base;
    }

    public Node getMessage() {
        return message;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
