package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class IfNode extends Node {

    private final Node condition;

    /* can both be null */
    private final Node thenNode;
    private final Node elseNode;

    public IfNode(int sourceLine, int sourceCharPos, String text, Node condition, Node thenNode, Node elseNode) {
        super(sourceLine, sourceCharPos, text);
        this.condition = condition;
        this.thenNode = thenNode;
        this.elseNode = elseNode;
    }

    public Node getCondition() {
        return condition;
    }

    public Node getThenNode() {
        return thenNode;
    }

    public Node getElseNode() {
        return elseNode;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
