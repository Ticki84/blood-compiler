package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class BodyClass extends Node {

    private final ArrayList<Node> attributes;
    private final ArrayList<Node> methods;
    private final ArrayList<Node> constructors;  /* syntactic check needed later to ensure constructor is unique */

    public BodyClass(int sourceLine, int sourceCharPos, String text, ArrayList<Node> attributes, ArrayList<Node> methods, ArrayList<Node> constructors) {
        super(sourceLine, sourceCharPos, text);
        this.attributes = attributes;
        this.methods = methods;
        this.constructors = constructors;
    }

    public ArrayList<Node> getAttributes() {
        return attributes;
    }

    public ArrayList<Node> getMethods() {
        return methods;
    }

    public ArrayList<Node> getConstructors() {
        return constructors;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
