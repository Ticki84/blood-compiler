package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class BodyConstructor extends Node {

    private final ArrayList<Node> instructions;

    public BodyConstructor(int sourceLine, int sourceCharPos, String text, ArrayList<Node> instructions) {
        super(sourceLine, sourceCharPos, text);
        this.instructions = instructions;
    }

    public ArrayList<Node> getInstructions() {
        return instructions;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
