package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class ParamConstructor extends ParamClassBase{

    public ParamConstructor(int sourceLine, int sourceCharPos, String text, String name, String type, boolean isNewAttribute) {
        super(sourceLine, sourceCharPos, text, name, type, isNewAttribute);
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}