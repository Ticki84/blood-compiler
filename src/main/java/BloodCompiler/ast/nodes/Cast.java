package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class Cast extends Node {
    private final String targetClass;
    private final Node castee;

    public Cast(int sourceLine, int sourceCharPos, String text, String targetClass, Node castee) {
        super(sourceLine, sourceCharPos, text);
        this.targetClass = targetClass;
        this.castee = castee;
    }

    public String getTargetClass() {
        return targetClass;
    }

    public Node getCastee() {
        return castee;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
