package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class IntegerNode extends Node {
    private final int value;

    public IntegerNode(int sourceLine, int sourceCharPos, String text, int value) {
        super(sourceLine, sourceCharPos, text);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
