package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.StringType;

public class OPCat extends OPBinary{

    public OPCat(int sourceLine, int sourceCharPos, String text, Node leftOperand, Node rightOperand) {
        super(sourceLine, sourceCharPos, text, leftOperand, rightOperand, "&", StringType.class);
    }

    public void accept(Visitor v) {
        v.visit((OPBinary) this);
        v.visit(this);
    }
}