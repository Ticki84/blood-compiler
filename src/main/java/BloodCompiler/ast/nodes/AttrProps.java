package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.logging.Log;

public class AttrProps extends Node {

    private final boolean isStatic;

    public AttrProps(int sourceLine, int sourceCharPos, String text, boolean isStatic) {
        super(sourceLine, sourceCharPos, text);
        this.isStatic = isStatic;
    }

    public boolean isStatic() {
        return this.isStatic;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
