package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class Args extends Node {

    private final ArrayList<Node> argsList;

    public Args(int sourceLine, int sourceCharPos, String text, ArrayList<Node> argsList) {
        super(sourceLine, sourceCharPos, text);
        this.argsList = argsList;
    }

    public ArrayList<Node> getArgsList() {
        return this.argsList;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
