package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class ParamsMethod extends Node{

    private final ArrayList<ParamMethod> parameterMethods ;

    public ParamsMethod(int sourceLine, int sourceCharPos, String text, ArrayList<ParamMethod> parameterMethods) {
        super(sourceLine, sourceCharPos, text);
        this.parameterMethods = parameterMethods;
    }

    public ArrayList<ParamMethod> getParameterMethods() {
        return parameterMethods;
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}