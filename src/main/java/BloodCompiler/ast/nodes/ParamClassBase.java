package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.Objects;

public abstract class ParamClassBase extends Node{

    private final String name;
    private final String type;
    private final boolean isNewAttribute;

    public ParamClassBase(int sourceLine, int sourceCharPos, String text, String name, String type, boolean isNewAttribute) {
        super(sourceLine, sourceCharPos, text);
        this.name = name;
        this.type = type;
        this.isNewAttribute = isNewAttribute;
    }

    public String getName(){
        return name;
    }

    public String getType(){
        return type;
    }

    public boolean isNewAttribute() {
        return isNewAttribute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParamClassBase)) return false;
        ParamClassBase that = (ParamClassBase) o;
        return isNewAttribute == that.isNewAttribute && name.equals(that.name) && type.equals(that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, isNewAttribute);
    }

    public abstract void accept(Visitor v);
}