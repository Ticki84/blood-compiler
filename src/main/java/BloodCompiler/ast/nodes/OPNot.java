package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.IntegerType;

public class OPNot extends OPUnary{

    public OPNot(int sourceLine, int sourceCharPos, String text, Node operand) {
        super(sourceLine, sourceCharPos, text, operand, "~", IntegerType.class);
    }

    public void accept(Visitor v) {
        v.visit((OPUnary) this);
        v.visit(this);
    }
}