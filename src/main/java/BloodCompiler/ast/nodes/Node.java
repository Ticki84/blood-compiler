package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public abstract class Node {
    private final int sourceLine;
    private final int sourceCharPos;
    private final String text;

    public Node(int sourceLine, int sourceCharPos, String text) {
        this.sourceLine = sourceLine;
        this.sourceCharPos = sourceCharPos;
        this.text = text;
    }

    public int getSourceLine() {
        return sourceLine;
    }

    public int getSourceCharPos() {
        return sourceCharPos;
    }

    public String getText() {
        return text;
    }

    public abstract void accept(Visitor v);
}
