package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.IntegerType;

public class OPAnd extends OPBinary{

    public OPAnd(int sourceLine, int sourceCharPos, String text, Node leftOperand, Node rightOperand) {
        super(sourceLine, sourceCharPos, text, leftOperand, rightOperand, "and", IntegerType.class);
    }

    public void accept(Visitor v) {
        v.visit((OPBinary) this);
        v.visit(this);
    }

}