package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class SuperNode extends Node {

    public SuperNode(int sourceLine, int sourceCharPos, String text) {
        super(sourceLine, sourceCharPos, text);
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
