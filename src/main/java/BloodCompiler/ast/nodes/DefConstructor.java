package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;
import java.util.List;

public class DefConstructor extends Node {

    private final String name;
    private final ArrayList<ParamConstructor> args;
    private final Node superClassInit;
    private final Node bloc;

    public DefConstructor(int sourceLine, int sourceCharPos, String text, String name, ArrayList<ParamConstructor> args, Node superClassInit, Node bloc) {
        super(sourceLine, sourceCharPos, text);
        this.name = name;
        this.args = args;
        this.superClassInit = superClassInit;
        this.bloc = bloc;
    }

    public String getName() {
        return name;
    }

    public List<ParamConstructor> getArgs() {
        return args;
    }

    public Node getSuperClassInit() {
        return superClassInit;
    }

    public Node getBloc() {
        return bloc;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
