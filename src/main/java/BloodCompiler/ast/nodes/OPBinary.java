package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.Type;

public abstract class OPBinary extends Node {

    private final Node leftOperand;
    private final Node rightOperand;
    private final String operator;
    private final Class<? extends Type> allowedType;

    public OPBinary(int sourceLine, int sourceCharPos, String text, Node leftOperand, Node rightOperand, String operator, Class<? extends Type> allowedType) {
        super(sourceLine, sourceCharPos, text);
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
        this.operator = operator;
        this.allowedType = allowedType;
    }

    public Node getLeftOperand() {
        return leftOperand;
    }

    public Node getRightOperand() {
        return rightOperand;
    }

    public String getOperator() {
        return operator;
    }

    public Class<? extends Type> getAllowedType() {
        return allowedType;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
