package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class DefLVal extends Node {

    private final Node lVal;
    private final String lValClass;
    private Node initializer;  /* can be null */

    public DefLVal(int sourceLine, int sourceCharPos, String text, Node lVal, String lValClass) {
        super(sourceLine, sourceCharPos, text);
        this.lVal = lVal;
        this.lValClass = lValClass;
    }

    public DefLVal(int sourceLine, int sourceCharPos, String text, Node lVal, String lValClass, Node initializer) {
        this(sourceLine, sourceCharPos, text, lVal, lValClass);
        this.initializer = initializer;
    }

    public Node getLVal() {
        return lVal;
    }

    public String getLValClass() {
        return lValClass;
    }

    public Node getInitializer() {
        return initializer;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
