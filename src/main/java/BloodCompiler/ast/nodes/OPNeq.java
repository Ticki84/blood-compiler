package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;
import BloodCompiler.symbolTable.types.IntegerType;

public class OPNeq extends OPBinary{

    public OPNeq(int sourceLine, int sourceCharPos, String text, Node leftOperand, Node rightOperand) {
        super(sourceLine, sourceCharPos, text, leftOperand, rightOperand, "<>", IntegerType.class);
    }

    public void accept(Visitor v) {
        v.visit((OPBinary) this);
        v.visit(this);
    }
}