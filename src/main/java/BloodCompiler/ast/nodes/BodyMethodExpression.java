package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class BodyMethodExpression extends Node {

    private final Node expression;

    public BodyMethodExpression(int sourceLine, int sourceCharPos, String text, Node expression) {
        super(sourceLine, sourceCharPos, text);
        this.expression = expression;
    }

    public Node getExpression() {
        return expression;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
