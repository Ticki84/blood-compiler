package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class StringNode extends Node {

    private final String stringContent;

    public StringNode(int sourceLine, int sourceCharPos, String text) {
        super(sourceLine, sourceCharPos, text);
        this.stringContent = text;
    }

    public String getStringContent() {
        return stringContent;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
