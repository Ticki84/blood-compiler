package BloodCompiler.ast.nodes;


import BloodCompiler.ast.Visitor;

import java.util.List;

public class ClassDef extends Node {

    private final String className;
    private final List<ParamClass> parametersClass;
    private final String superClass;
    private final Node bodyClass;


    public ClassDef(int sourceLine, int sourceCharPos, String text, String className, List<ParamClass> parametersClass, String superClass, Node bodyClass) {
        super(sourceLine, sourceCharPos, text);
        this.bodyClass = bodyClass;
        this.className = className;
        this.parametersClass = parametersClass;
        this.superClass = superClass;
    }

    public String getClassName() {
        return className;
    }

    public List<ParamClass> getParametersClass() {
        return parametersClass;
    }

    public String getSuperClass() {
        return superClass;
    }

    public Node getBodyClass() {
        return bodyClass;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
