package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class ReturnNode extends Node {

    public ReturnNode(int sourceLine, int sourceCharPos, String text) {
        super(sourceLine, sourceCharPos, text);
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}
