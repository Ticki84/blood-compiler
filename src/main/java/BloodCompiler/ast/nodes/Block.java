package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

import java.util.ArrayList;

public class Block extends Node {

    private final ArrayList<Node> instructions;
    private final ArrayList<Node> declarations;

    public Block(int sourceLine, int sourceCharPos, String text, ArrayList<Node> instructions) {
        this(sourceLine, sourceCharPos, text, new ArrayList<>(), instructions);
    }

    public Block(int sourceLine, int sourceCharPos, String text, ArrayList<Node> declarations, ArrayList<Node> instructions) {
        super(sourceLine, sourceCharPos, text);
        this.declarations = declarations;
        this.instructions = instructions;
    }

    public ArrayList<Node> getDeclarations() {
        return declarations;
    }

    public ArrayList<Node> getInstructions() {
        return instructions;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
