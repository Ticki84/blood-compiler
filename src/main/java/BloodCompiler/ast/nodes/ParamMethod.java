package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class ParamMethod extends Node{

    private final String name;
    private final String type;

    public ParamMethod(int sourceLine, int sourceCharPos, String text, String name, String type) {
        super(sourceLine, sourceCharPos, text);
        this.name = name;
        this.type = type;
    }

    public String getName(){
        return name;
    }

    public String getType(){
        return type;
    }


    public void accept(Visitor v) {
        v.visit(this);
    }
}