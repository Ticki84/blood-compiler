package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class Identifier extends Node {
    private final String name;

    public Identifier(int sourceLine, int sourceCharPos, String text) {
        super(sourceLine, sourceCharPos, text);
        this.name = text;
    }

    public String getName() {
        return name;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
