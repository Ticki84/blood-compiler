package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class DefAttribute extends Node {

    private final boolean isStatic;
    private final String name;
    private final String type;
    private final Node expression;

    public DefAttribute(int sourceLine, int sourceCharPos, String text, boolean isStatic, String name, String type, Node expression) {
        super(sourceLine, sourceCharPos, text);
        this.isStatic = isStatic;
        this.name = name;
        this.type = type;
        this.expression = expression;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Node getExpression() {
        return expression;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
