package BloodCompiler.ast.nodes;

import BloodCompiler.ast.Visitor;

public class DefMethod extends Node {

    private final boolean isStatic;
    private final boolean isOverride;
    private final String name;
    private final Node paramsMeth;
    private final String returnType;
    private final Node bodyMeth;

    public DefMethod(int sourceLine, int sourceCharPos, String text, boolean isStatic, boolean isOverride, String name, Node paramsMeth, String returnType, Node bodyMeth) {
        super(sourceLine, sourceCharPos, text);
        this.isStatic = isStatic;
        this.isOverride = isOverride;
        this.name = name;
        this.paramsMeth = paramsMeth;
        this.returnType = returnType;
        this.bodyMeth = bodyMeth;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public boolean isOverride() {
        return isOverride;
    }

    public String getName() {
        return name;
    }

    public Node getParamsMeth() {
        return paramsMeth;
    }

    public String getReturnType() {
        return returnType;
    }

    public Node getBodyMeth() {
        return bodyMeth;
    }

    public void accept(Visitor v) {
        v.visit(this);
    }
}
