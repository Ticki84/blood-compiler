package BloodCompiler.ast;

import BloodCompiler.ast.nodes.*;

@SuppressWarnings("EmptyMethod")
public interface Visitor {
    void visit(Args node);

    void visit(Assignment node);

    void visit(AttrProps node);

    void visit(Block node);

    void visit(BodyClass node);

    void visit(BodyConstructor node);

    void visit(BodyMethodExpression node);

    void visit(Cast node);

    void visit(ClassDef node);

    void visit(DefAttribute node);

    void visit(DefConstructor node);

    void visit(DefLVal node);

    void visit(DefLVals node);

    void visit(DefMethod node);

    void visit(Identifier node);

    void visit(IfNode node);

    void visit(Instantiate node);

    void visit(IntegerNode node);

    void visit(MessageBaseClass node);

    void visit(MessageBaseExpr node);

    void visit(MessageCall node);

    void visit(Message node);

    void visit(OPBinary node);

    void visit(OPAdd node);

    void visit(OPAnd node);

    void visit(OPCat node);

    void visit(OPDiv node);

    void visit(OPEqu node);

    void visit(OPGeq node);

    void visit(OPGrt node);

    void visit(OPLeq node);

    void visit(OPLes node);

    void visit(OPMod node);

    void visit(OPMul node);

    void visit(OPNeg node);

    void visit(OPNeq node);

    void visit(OPNot node);

    void visit(OPOr node);

    void visit(OPShl node);

    void visit(OPShr node);

    void visit(OPSub node);

    void visit(OPUnary node);

    void visit(OPXor node);

    void visit(ParamClass node);

    void visit(ParamConstructor node);

    void visit(ParamMethod node);

    void visit(ParamsMethod node);

    void visit(ResultNode node);

    void visit(ReturnNode node);

    void visit(Root node);

    void visit(StringNode node);

    void visit(SuperClassInit node);

    void visit(SuperClass node);

    void visit(SuperNode node);

    void visit(ThisNode node);

    void visit(While node);
}
