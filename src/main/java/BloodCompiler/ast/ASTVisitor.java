package BloodCompiler.ast;

import BloodCompiler.ast.nodes.*;
import BloodCompiler.logging.Log;
import BloodCompiler.semantic.SemanticChecker;
import BloodCompiler.symbolTable.SymbolTableKind;
import BloodCompiler.symbolTable.TableFiller;
import BloodCompiler.symbolTable.TableManager;
import BloodCompiler.symbolTable.records.ClassRecord;
import BloodCompiler.symbolTable.records.ConstructorRecord;
import BloodCompiler.symbolTable.records.MethodRecord;
import BloodCompiler.symbolTable.records.ParameterRecord;
import BloodCompiler.symbolTable.types.Type;

import java.util.ArrayList;
import java.util.List;

public abstract class ASTVisitor implements ProcessingVisitor {

    protected final TableManager tableManager;
    protected ClassRecord currentClass;
    protected MethodRecord currentMethod;

    public ASTVisitor(TableManager tableManager) {
        this.tableManager = tableManager;
    }

    /* Utility methods */

    protected ArrayList<Type> signatureFromClassParams(List<ParamClass> params) {
        ArrayList<Type> signature = new ArrayList<>();

        for (ParamClass param : params) {
            signature.add(tableManager.getType(param.getType()));
        }

        return signature;
    }

    protected ArrayList<Type> signatureFromConstructorParams(List<ParamConstructor> params) {
        ArrayList<Type> signature = new ArrayList<>();

        for (ParamConstructor param : params) {
            signature.add(tableManager.getType(param.getType()));
        }

        return signature;
    }

    protected ArrayList<Type> signatureFromMethodParams(ParamsMethod params) {
        ArrayList<Type> signature = new ArrayList<>();

        for (ParamMethod parameter : params.getParameterMethods()) {
            signature.add(tableManager.getType(parameter.getType()));
        }

        return signature;
    }

    protected String getCurrentClassName() {
        if (currentClass != null) return currentClass.getId();
        return "<main body>";
    }

    protected String getCurrentMethodName() {
        if (currentMethod != null) return currentMethod.getId();
        return "<main body>";
    }


    @Override
    public void visit(Args node) {
        process(node);
        for (Node arg : node.getArgsList()) {
            arg.accept(this);
        }
    }

    @Override
    public void visit(Assignment node) {
        process(node);
        node.getAssignee().accept(this);
        node.getVar().accept(this);
    }

    @Override
    public void visit(AttrProps node) {
        process(node);
    }

    @Override
    public void visit(Block node) {
        Log.debugSemantic(node, "Entering scope for block");
        tableManager.enterScope(SymbolTableKind.BLOCK);
        process(node);
        for (Node declaration : node.getDeclarations()) {
            if (declaration instanceof DefLVal) {
                declaration.accept(this);
            }
        }
        for (Node instruction : node.getInstructions()) {
            if (!(instruction instanceof DefLVal)) {
                instruction.accept(this);
            }
        }
        tableManager.exitScope();
        Log.debugSemantic(node, "Exiting scope for block");
    }

    @Override
    public void visit(BodyClass node) {
        process(node);
        for (Node attribute : node.getAttributes()) {
            attribute.accept(this);
        }
        for (Node constructor : node.getConstructors()) {
            constructor.accept(this);
        }
        for (Node method : node.getMethods()) {
            method.accept(this);
        }
    }

    @Override
    public void visit(BodyConstructor node) {
        process(node);
        for (Node instruction : node.getInstructions()) {
            instruction.accept(this);
        }
    }

    @Override
    public void visit(BodyMethodExpression node) {
        process(node);
        node.getExpression().accept(this);
    }

    @Override
    public void visit(Cast node) {
        process(node);
        node.getCastee().accept(this);
    }

    @Override
    public void visit(ClassDef node) {
        currentClass = tableManager.getClass(node.getClassName());
        if (this instanceof TableFiller) process(node);
        Log.debugSemantic(node, String.format("Entering scope for class '%s' body.", getCurrentClassName()));
        tableManager.enterScope(SymbolTableKind.CLASS, node.getClassName());
        if (this instanceof SemanticChecker) process(node);
        for (ParamClass param : node.getParametersClass()) {
            param.accept(this);
        }
        node.getBodyClass().accept(this);
        tableManager.exitScope();
        Log.debugSemantic(node, String.format("Exiting scope for class '%s' body.", getCurrentClassName()));
        currentClass = null;
    }

    @Override
    public void visit(DefAttribute node) {
        process(node);
        if (node.getExpression() != null)
            node.getExpression().accept(this);
    }

    @Override
    public void visit(DefConstructor node) {
        if (this instanceof TableFiller) process(node);
        Log.debugSemantic(node, String.format("Entering scope for constructor '%s' body.", Log.prettyMethod(getCurrentClassName(), node.getName())));
        tableManager.enterScope(SymbolTableKind.CONSTRUCTOR);
        if (this instanceof SemanticChecker) process(node);

        if (this instanceof TableFiller) {
            /* sorry, didn't have time to go back to the grammar and rebuild the AST */
            ParameterRecord thisRecord = new ParameterRecord("this", tableManager.getType(currentClass.getId()), tableManager.getCurrentImbrication());
            ConstructorRecord constructorRecord = tableManager.getClassConstructor(currentClass);
            constructorRecord.addParameterRecord(thisRecord);
            tableManager.addSymbol("this", thisRecord);
        }

        for (Node arg : node.getArgs()) {
            arg.accept(this);
        }
        if (node.getSuperClassInit() != null)
            node.getSuperClassInit().accept(this);
        node.getBloc().accept(this);
        tableManager.exitScope();
    }

    @Override
    public void visit(DefLVal node) {
        process(node);
        node.getLVal().accept(this);
        if (node.getInitializer() != null)
            node.getInitializer().accept(this);
    }

    @Override
    public void visit(DefLVals node) {
        process(node);
        for (Node lVal : node.getLVarDefs()) {
            lVal.accept(this);
        }
    }

    @Override
    public void visit(DefMethod node) {
        currentMethod = tableManager.getClassMethodWithoutSuperclass(currentClass, node.getName());
        if (this instanceof TableFiller) process(node);
        Log.debugSemantic(node, String.format("Entering scope for method '%s' body.", Log.prettyMethod(getCurrentClassName(), node.getName())));
        tableManager.enterScope(SymbolTableKind.METHOD, node.getName());
        if (this instanceof SemanticChecker) process(node);
        node.getParamsMeth().accept(this);
        node.getBodyMeth().accept(this);
        tableManager.exitScope();
        Log.debugSemantic(node, String.format("Exiting scope for method '%s' body.", Log.prettyMethod(getCurrentClassName(), node.getName())));
        currentMethod = null;
    }

    @Override
    public void visit(Identifier node) {
        process(node);
    }

    @Override
    public void visit(IfNode node) {
        process(node);
        node.getCondition().accept(this);
        if (node.getThenNode() != null)
            node.getThenNode().accept(this);
        if (node.getElseNode() != null)
            node.getElseNode().accept(this);
    }

    @Override
    public void visit(Instantiate node) {
        process(node);
        node.getArgs().accept(this);
    }

    @Override
    public void visit(IntegerNode node) {
        process(node);
    }

    @Override
    public void visit(MessageBaseClass node) {
        process(node);
        node.getMessage().accept(this);
    }

    @Override
    public void visit(MessageBaseExpr node) {
        process(node);
        node.getMessage().accept(this);
        node.getBase().accept(this);
    }

    @Override
    public void visit(MessageCall node) {
        process(node);
        if (node.getMessage() != null)
            node.getMessage().accept(this);
        node.getArgs().accept(this);
    }

    @Override
    public void visit(Message node) {
        process(node);
        if (node.getMessage() != null)
            node.getMessage().accept(this);
    }

    @Override
    public void visit(OPBinary node) {
        process(node);
        node.getLeftOperand().accept(this);
        node.getRightOperand().accept(this);
    }

    @Override
    public void visit(OPAdd node) {
        process(node);
    }

    @Override
    public void visit(OPAnd node) {
        process(node);
    }

    @Override
    public void visit(OPCat node) {
        process(node);
    }

    @Override
    public void visit(OPDiv node) {
        process(node);
    }

    @Override
    public void visit(OPEqu node) {
        process(node);
    }

    @Override
    public void visit(OPGeq node) {
        process(node);
    }

    @Override
    public void visit(OPGrt node) {
        process(node);
    }

    @Override
    public void visit(OPLeq node) {
        process(node);
    }

    @Override
    public void visit(OPLes node) {
        process(node);
    }

    @Override
    public void visit(OPMod node) {
        process(node);
    }

    @Override
    public void visit(OPMul node) {
        process(node);
    }

    @Override
    public void visit(OPNeg node) {
        process(node);
    }

    @Override
    public void visit(OPNeq node) {
        process(node);
    }

    @Override
    public void visit(OPNot node) {
        process(node);
    }

    @Override
    public void visit(OPOr node) {
        process(node);
    }

    @Override
    public void visit(OPShl node) {
        process(node);
    }

    @Override
    public void visit(OPShr node) {
        process(node);
    }

    @Override
    public void visit(OPSub node) {
        process(node);
    }

    @Override
    public void visit(OPUnary node) {
        process(node);
        node.getOperand().accept(this);
    }

    @Override
    public void visit(OPXor node) {
        process(node);
    }

    @Override
    public void visit(ParamClass node) {
        process(node);
    }

    @Override
    public void visit(ParamConstructor node) {
        process(node);
    }

    @Override
    public void visit(ParamMethod node) {
        process(node);
    }

    @Override
    public void visit(ParamsMethod node) {
        process(node);
        for (Node param : node.getParameterMethods()){
            param.accept(this);
        }
    }

    @Override
    public void visit(ResultNode node) {
        process(node);
    }

    @Override
    public void visit(ReturnNode node) {
        process(node);
    }

    @Override
    public void visit(Root node) {
        process(node);
        for (Node currentClass : node.getClasses()) {
            currentClass.accept(this);
        }
        for (Node currentInstruction : node.getMainProgram()) {
            currentInstruction.accept(this);
        }
    }

    @Override
    public void visit(StringNode node) {
        process(node);
    }

    @Override
    public void visit(SuperClassInit node) {
        process(node);
        node.getArgs().accept(this);
    }

    @Override
    public void visit(SuperClass node) {
        process(node);
    }

    @Override
    public void visit(SuperNode node) {
        process(node);
    }

    @Override
    public void visit(ThisNode node) {
        process(node);
    }

    @Override
    public void visit(While node) {
        process(node);
        node.getExpression().accept(this);
        node.getInstruction().accept(this);
    }
}
