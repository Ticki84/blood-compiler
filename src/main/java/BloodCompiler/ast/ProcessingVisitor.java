package BloodCompiler.ast;

import BloodCompiler.ast.nodes.*;

public interface ProcessingVisitor extends Visitor {
    void process(Args node);

    void process(Assignment node);

    void process(AttrProps node);

    void process(Block node);

    void process(BodyClass node);

    void process(BodyConstructor node);

    void process(BodyMethodExpression node);

    void process(Cast node);

    void process(ClassDef node);

    void process(DefAttribute node);

    void process(DefConstructor node);

    void process(DefLVal node);

    void process(DefLVals node);

    void process(DefMethod node);

    void process(Identifier node);

    void process(IfNode node);

    void process(Instantiate node);

    void process(IntegerNode node);

    void process(MessageBaseClass node);

    void process(MessageBaseExpr node);

    void process(MessageCall node);

    void process(Message node);

    void process(OPBinary node);

    void process(OPAdd node);

    void process(OPAnd node);

    void process(OPCat node);

    void process(OPDiv node);

    void process(OPEqu node);

    void process(OPGeq node);

    void process(OPGrt node);

    void process(OPLeq node);

    void process(OPLes node);

    void process(OPMod node);

    void process(OPMul node);

    void process(OPNeg node);

    void process(OPNeq node);

    void process(OPNot node);

    void process(OPOr node);

    void process(OPShl node);

    void process(OPShr node);

    void process(OPSub node);

    void process(OPUnary node);

    void process(OPXor node);

    void process(ParamClass node);

    void process(ParamConstructor node);

    void process(ParamMethod node);

    void process(ParamsMethod node);

    void process(ResultNode node);

    void process(ReturnNode node);

    void process(Root node);

    void process(StringNode node);

    void process(SuperClassInit node);

    void process(SuperClass node);

    void process(SuperNode node);

    void process(ThisNode node);

    void process(While node);
}
