# PCL1-intermediate
Ce fichier regroupe l'ensemble des programmes écrits en langage BLOOD (par nos soins ou ceux de l'équipe pédagogique) qui serviront de tests pour la grammaire et de générateurs d'AST pour la soutenance du 09/12/2020.


## Sommaire
- [Programmes valides](#Programmes-valides)
- [Programmes comportant volontairement des erreurs](#Programmes-comportant-volontairement-des-erreurs)
- [Programme fourni par l'équipe pédagogique](#Programme-fourni-par-l'équipe-pédagogique)


## Programmes valides
Le répertoire [exemples/tests_ecrits](exemples/tests_ecrits/) contient tous les programmes BLOOD que nous avons rédigés afin de tester notre grammaire sur des exemples concrets et générer quelques AST par la même occasion.

- [exemples/tests_ecrits/DeclarationClasseNoParam.blood](exemples/tests_ecrits/DeclarationClasseNoParam.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}
```

- [exemples/tests_ecrits/DeclarationClasseWithParam.blood](exemples/tests_ecrits/DeclarationClasseWithParam.blood) :
```
class Personne(var humeur :Integer) is {
	
    /* humeur vaut +1 si la Personne est contente, -1 sinon */

    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age
    def getHumeur() : String := this.humeur

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne(var humeur :Integer) is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}
```

- [exemples/tests_ecrits/DeclarationClasseHeritage.blood](exemples/tests_ecrits/DeclarationClasseHeritage.blood) :
```
class Personne(var humeur :Integer) is {
	
    /* humeur vaut +1 si la Personne est contente, -1 sinon */

    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age
    def getHumeur() : String := this.humeur

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne(var humeur :Integer) is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


class Adulte() extends Personne is{

    /* Attribut en plus */
    var metier : String;

    /* Getter en plus */
    def getMetier() : String := this.metier

    /* Setter en plus */
    def setMetier(work : String) is {
        this.metier := work;
    }

    /* Override d'une methode */
    def override getAge() : String := this.age - 5 // Il veut paraitre plus jeune

    /* Constructeur */
    def Adulte() : Personne(1) is {
        metier := "Charcutier";
    }

}
```

- [exemples/tests_ecrits/ProgrammeAvecProgPrincipal.blood](exemples/tests_ecrits/ProgrammeAvecProgPrincipal.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


{

    benjamin : Personne := new Personne();
    dumitru : Personne := new Personne();
    florent : Personne := new Personne();
    yann : Personne := new Personne();
    
    is

    "Lets go...\n".println();

    benjamin.setNom("SEPE");
    benjamin.setPrenom("Benjamin");
    benjamin.setAge(1);

    dumitru.setNom("BULGARU");
    dumitru.setPrenom("Dumitru");
    dumitru.setAge(1);

    florent.setNom("CASPAR");
    florent.setPrenom("Florent");
    florent.setAge(1);

    yann.setNom("COLOMB");
    yann.setPrenom("Yann");
    yann.setAge(1);

    "Fini...\n".println();

}
```

- [exemples/tests_ecrits/CompositionDeClasses.blood](exemples/tests_ecrits/CompositionDeClasses.blood) :
```
class Metier(var nomDuMetier : String, var salaire : Integer) is {

    /* Constructeur */
    def Metier(var nomDuMetier : String, var salaire : Integer) is {
        smic : Integer := 1200;
        max : Integer := 10000;
        is
        if salaire < smic
            then salaire := smic;
            else if salaire > max
                then salaire := max;
                else {}
        this.salaire := salaire;
    }

    /* Getters */
    def getNomDuMetier() : String := this.nomDuMetier
    def getSalaire() : String := this.salaire

    /* Setters */
    def setNomDuMetier(nom : String) is {
        this.nomDuMetier := nom;
    }

    def setSalaire(n : Integer) is {
        this.salaire := n;
    }

}


class Entreprise(var nomEntreprise : String) is {

    /* Constructeur */
    def Entreprise() is {
        this.patron := new Metier("Patron", 8000);
        this.employe1 := new Metier("Ingenieur", 3000);
        this.employe2 := new Metier("Architecte", 2800);
        this.stagiaire := new Meteir("Etudiant de TELECOM", 1200);
    }

    /* Attributs d'instance */
    var patron : Metier;
    var employe1 : Metier;
    var employe2 : Metier;
    var stagiaire : Metier;

    /* Getters */
    def getPatron() : Metier := this.patron
    def getEmploye1() : Metier := this.employe1
    def getEmploye2() : Metier := this.employe2
    def getStagiaire() : Metier := this.stagiaire

    /* Setters */
    def setPatron(boss : Metier) is {
        this.patron := boss;
    }

    def setEmploye1(worker1 : Metier) is {
        this.employe1 := worker1;
    }

    def setEmploye2(worker2 : Metier) is {
        this.employe2 := worker2;
    }

    def setStagiaire(intern : Metier) is {
        this.stagiaire := intern;
    }

    /* Quelques methodes pour voir ce qu'il se passe */
    def jeVeuxLeSalaireDuPatron() : Integer := this.getPatron().getSalaire()
    def jeVeuxLePosteDuPremierEmploye() : String := this.getEmploye1.getNomDuMetier()
    def changerLeSalaireDuStagiaire(newSalaire : Integer) is {
        this.getStagiaire().setSalaire(newSalaire);
    }

}
```

- [exemples/tests_ecrits/ProgrammeAvecCalculs.blood](exemples/tests_ecrits/ProgrammeAvecCalculs.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


{
    
    randomPersonne : Personne := new Personne();
    x : Integer := (5+2)*(15-5)/7 + 8;
    y : Integer := 5*(4/2)/2;
    z : Integer := -1;
    
    is

    "Lets go...\n".println();
    randomPersonne.setNom("NOM_DE_FAMILLE");
    randomPersonne.setPrenom("Prenom");    
    randomPersonne.setAge(x+y+z);
    "Fini...\n".println();

}
```

- [exemples/tests_ecrits/ProgrammeAvecIF.blood](exemples/tests_ecrits/ProgrammeAvecIF.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


{

    randomPersonne : Personne := new Personne();

    is

    "Lets go...\n".println();
    
    randomPersonne.setNom("NOM_DE_FAMILLE");
    randomPersonne.setPrenom("Prenom");
    randomPersonne.setAge(25);

    if randomPersonne.getAge() < 18
        then "Il est mineur\n".println();
    else 
        "Il est majeur\n".println();

    "Fini...\n".println();

}
```

- [exemples/tests_ecrits/ProgrammeAvecCast.blood](exemples/tests_ecrits/ProgrammeAvecCast.blood) :
```
class Personne(var humeur :Integer) is {
	
    /* humeur vaut +1 si la Personne est contente, -1 sinon */

    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age
    def getHumeur() : String := this.humeur

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne(var humeur :Integer) is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


class Adulte() extends Personne is{

    /* Attribut en plus */
    var metier : String;

    /* Getter en plus */
    def getMetier() : String := this.metier

    /* Setter en plus */
    def setMetier(work : String) is {
        this.metier := work;
    }

    /* Override d'une methode */
    def override getAge() : String := this.age - 5 // Il veut paraitre plus jeune

    /* Constructeur */
    def Adulte() : Personne(1) is {
        metier := "Charcutier";
    }

}


{

    unAdulteRandom : Adulte := new Adulte();
    x : Integer := unAdulteRandom.getAge();
    y : Integer := (as Personne : unAdulteRandom).getAge();

    is

    "Lets go...\n".println();
    "Fini...\n".println();

}
```

- [exemples/tests_ecrits/ProgrammeAvecWhile.blood](exemples/tests_ecrits/ProgrammeAvecWhile.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


{

    randomPersonne : Personne := new Personne();

    is

    "Lets go...\n".println();
    
    randomPersonne.setNom("NOM_DE_FAMILLE");
    randomPersonne.setPrenom("Prenom");
    randomPersonne.setAge(25);

    while 1
        do "Salut\n".println();

    while (50-50/2)/(5*5)
        do "Bonjour\n".println();

    "Fini...\n".println();

}
```

- [exemples/tests_ecrits/ProgrammeIFandWhile.blood](exemples/tests_ecrits/ProgrammeIFandWhile.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}


{

    randomPersonne : Personne := new Personne();

    is

    "Lets go...\n".println();
    
    randomPersonne.setNom("NOM_DE_FAMILLE");
    randomPersonne.setPrenom("Prenom");
    randomPersonne.setAge(25);

    if randomPersonne.getAge() < 18
        then while 1 do if 3 > 2 then "azerty".println(); else "qwerty".println();
    else 
        if 1 < 2
            then "Coucou\n".println();
        else
            "Ciao\n".println();

    "Fini...\n".println();

}
```


## Programmes comportant volontairement des erreurs
Le répertoire [exemples/tests_nonOK_syntaxe](exemples/tests_nonOK_syntaxe_syntaxe/) contient tous les programmes BLOOD que nous avons rédigés mais qui comportent (volontairement) des erreurs afin que l'on puisse vérifier que notre grammaire est capable de détecter des erreurs syntaxiques.

- [exemples/tests_nonOK_syntaxe/FauteTypo.blood](exemples/tests_nonOK_syntaxe_syntaxe/FauteTypo.blood) :
```
classe Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}
```

- [exemples/tests_nonOK_syntaxe/FauteManquante.blood](exemples/tests_nonOK_syntaxe_syntaxe/AccoladeManquante.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }
```

- [exemples/tests_nonOK_syntaxe/MauvaisCommentaire.blood](exemples/tests_nonOK_syntaxe_syntaxe/MauvaisCommentaire.blood) :
```
class Personne() is {
	
    /* Attributs d'instance 
    var nom :String;
    var prenom :String;
    var age: Integer;

    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}
```

- [exemples/tests_nonOK_syntaxe/ParentheseManquante.blood](exemples/tests_nonOK_syntaxe_syntaxe/ParentheseManquante.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := ((5+3-2)/(22*1-2)*2;
    }

}
```

- [exemples/tests_nonOK_syntaxe/SemicolonManquant.blood](exemples/tests_nonOK_syntaxe_syntaxe/SemicolonManquant.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel"
        age := 34;
    }

}
```

- [exemples/tests_nonOK_syntaxe/TypeVariableManquant.blood](exemples/tests_nonOK_syntaxe_syntaxe/TypeVariableManquant.blood) :
```
class Personne() is {
	
    /* Attributs d'instance */
    var nom :String;
    var prenom :String;
    var age: Integer;

    /* Getters */
    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : String := this.age

    /* Setters */
    def setNom(name) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    /* Constructeur */
    def Personne() is {
        nom := "Jean";
        prenom := "Michel";
        age := 34;
    }

}
```

## Programme fourni par l'équipe pédagogique
Le répertoire [exemples/tests_fournis](exemples/tests_fournis/) contient tous les programmes BLOOD qui nous ont été fournis par l'équipe enseignante.

- [exemples/tests_fournis/Test.blood](exemples/tests_fournis/Test.blood) :
```
class Point(var x :Integer, var y :Integer , var name: String) is {
	var static next : Integer := 0;
	def static howMany() : Integer := Point.next - 1
	
	var hasClone : Integer := 0;
	
	var clone : Point;

	def Point(var x: Integer, var y: Integer , var name: String) is 
	{ Point.next := Point.next + 1; }

	def setName(s : String) is { this.name := s; }

	def getx() : Integer := this.x

	def gety() : Integer := this.y

	def isCloned() : Integer := this.hasClone <> 0

	def move(dx: Integer, dy: Integer , verbose: Integer) : Point is {
		this.x := this.x + dx; this.y := this.y + dy;
		if verbose then { this.print(verbose); } else {}
		result := this;
	}

	def print(verbose : Integer) is {
		if verbose then "Inside Point::print".println(); else { }
		this.name.print();
		( "= (" & this.x.toString() & ", " & this.y.toString() & ")" ).println();
	}

	def clone() : Point is
	{
		this.clone := new Point(this.x, this.y, this.name & "'");
		this.hasClone := 1;
		result := this.clone;
	}

	def allClones () is {
		this.print(0);
		if this.isCloned() then { this.clone.allClones(); } else { }
	}

	def egal(p: Point) : Integer is {
		b1 : Integer := p.getx() - this.x;
		b2 : Integer := p.gety() - this.y;
		is
		if b1 then result := 0; else result := b2 = 0; 
	}
}

class Couleur(var coul: Integer) is {

	def Couleur (var coul: Integer) is{
		if coul < 0 then coul :=0;
		else if coul > 2 then coul :=0; else {}
		this.coul := coul;
	}
	
	def name(verbose: Integer ) : String is {
		aux : String := "Blanc";
		is
		if verbose then "Inside Couleur::couleur".println(); else {}
		if this.coul = 0 then { result := aux; }
		else {
			aux : String := "Gris";
			is
			if this.coul = 1 then aux := "Noir"; else{ }
			result :=aux;
		}
	}

	def estGris() : Integer is {
		"Inside Couleur::estGris".println();
		result := this.coul = 2;
	}
}

class CouleurFactory() is
{
	var static theBlanc: Couleur := new Couleur(0);
	var static theNoir : Couleur := new Couleur(1);
	var static theGris : Couleur := new Couleur(2);

	def CouleurFactory() is {}

	def static blanc(): Couleur := this.theBlanc
	def static noir() : Couleur := this.theNoir
	def static gris() : Couleur := this.theGris
}

class PointColore(x: Integer, y: Integer, var coul: Couleur) extends Point is {

	def PointColore(x: Integer, y: Integer, var coul: Couleur) :
		Point(x, y, "P-" & CptPoint.howMany().toString() ) is { }

	def couleur() : Couleur := this.coul

	def colore() : Integer := this.coul.estGris() <> 0

	def override clone() : Point is {
		result := new PointColore(this.x, this.y,this.coul);
		result.setName(this.name & "'");
		this.hasClone := 1;
		this.clone := result;
	}

	def estGris() : Integer := this.coul.estGris()

	def override print(verbose : Integer ) is {
		if verbose then "Inside PointColore::print".println(); else { }
		super.print(verbose);
		this.couleur().name(verbose).println();
	}
}

class PointNoir(xc: Integer, yc: Integer) extends PointColore is {
	def PointNoir(xc: Integer , yc: Integer) : 
		PointColore(xc, yc, CouleurFactory.noir()) is { }

	def override estGris() : Integer := 0
	def override colore() : Integer := 1
	def override couleur() : Couleur := CouleurFactory.noir()
}

class DefaultPoint()
		extends PointColore is {
	def DefaultPoint() : PointColore(0, 0, CouleurFactory.blanc()) is {}
	def override estGris() : Integer := 0
	def override couleur() : Couleur := CouleurFactory.blanc()
}

class Test() is {
	def Test() is {}
	def static test(p: Point, p2: PointColore, p3: PointNoir) is {
		c: String; c2:String; c3:String;
		true: Integer := 1;
		false: Integer := 0;
		is
		p.print(true);
		p2.print(true);
		if p2.colore() <> 0 then c:= "colore"; else c:= "gris";
		if p3.colore() <> 0 then c2 := "colore"; else c2 := "gris";
		if p3.colore() <> 0 then c3 := "colore"; else c3 := "gris";
		"Resultats de test: ".println();
		c.print(); " ".print();
		c2.print(); " ".print();
		c3.print();
		"".println();
	}
	
	def static test2(p: PointColore) is {
		p.couleur().name(0).print();
	}
}

class A() is {
	var v : Integer;
	def A() is { this.v := 1; }
	def f() is { "A::f()\n".print(); }
	def h(x: Integer, y: Integer) : Integer := x + y
}

class A2() extends A is {
	var v : String;

	def A2() : A() is { this.v := "hello"; }

	def override f() is { "A2::f()\n".print(); }

	def g() is {
		this.v := "world";
		(as A : this).v := 1;
		this.f();
		(as A : this).f();
	}
	
	def override h(x: Integer, y: Integer) : Integer := super.h(x, y)
}

{
	true: Integer := 1;
	p1: Point := new Point(1, 5, "p1");
	p2: Point := new Point(2, 3, "p2");
	p3: Point := new Point(0, 0, "p3");
	o : PointColore := new PointColore(50, 100, CouleurFactory.noir());
	o2: Point;
	pn: PointNoir := new PointNoir(+1, -1);
	dp: DefaultPoint := new DefaultPoint();
	is
	p2.move(p1.getx(), p1.gety(), true);
	o.setName("origine");
	p2.move(p1.getx()-2*5-3, p1.gety(), true);
	o.print(true);
	p2.print(true);

	"\nClonage de o:".println();
	o2 := o.clone();
	o2.print(true);
	o2.clone.move(54, 36, 0).print(true);
	"\nListe des clones de o:".println();
	o.allClones();
	"Fin de la liste\n".println();

	"Seconde liste des clones de o:".println();
	o2 := p1.clone();
	o2.move(+2, -3, 0);
	p1.print(0);
	o2.clone();
	p1.allClones();
	"Fin de la seconde liste\n".println();

	"test(Point, PointColore, PointNoir)".println();
	Test.test(p1, o, pn);
	"test(PointNoir, PointNoir, PointNoir)".println();
	Test.test(pn, pn, pn);
	p1 := pn;
	Test.test2(o);
	Test.test2(pn);
	o := pn;

	"test(PointNoir, PointNoir, PointNoir)".println();
	Test.test(p1, o, pn);
	Test.test2(o);
	Test.test2(pn);
	"\nDone".println();
}
```