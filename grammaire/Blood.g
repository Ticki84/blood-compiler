grammar Blood;

options {
	language = Java;
	k = 1;
	backtrack = false;
	output = AST;
}


tokens {
	ARGS;
	ASSIGNMENT;
	ATTR_PROPS;
	BLOCK;
	BODY_CLASS;
	BODY_CONSTRUCTOR;
	BODY_METHOD;
	CAST;
	CLASSDEF;
	DEF_ATTRIBUTE;
	DEF_CONSTRUCTOR;
	DEF_LVARS;
	DEF_LVAR;
	DEF_METHOD;
	ELSE;
	EXPRESSION;
	EXPRESSIONS;
	IF;
	INSTANTIATE;
	MESSAGE;
	MESSAGE_BASE;
	MESSAGE_CALL;
	METHOD_PROPS;
	OP_OR;
	OP_XOR;
	OP_AND;
	OP_EQU;
	OP_NEQ;
	OP_LES;
	OP_LEQ;
	OP_GRT;
	OP_GEQ;
	OP_SHL;
	OP_SHR;
	OP_ADD;
	OP_SUB;
	OP_CAT;
	OP_MUL;
	OP_DIV;
	OP_MOD;
	OP_NEG;
	OP_NOT;
	OVERRIDE;
	PARAMS_CLASS;
	PARAMS_CONSTRUCTOR;
	PARAMS_METHOD;
	PARAM_CLASS;
	PARAM_CONSTRUCTOR;
	PARAM_METHOD;
	PROGRAM;
	RESULT;
	RETURN;
	STATIC;
	SUPER;
	SUPERCLASS;
	SUPERCLASS_INIT;
	THEN;
	THIS;
	VAR;
	WHILE;
}

@parser::members {
  @Override
  public void reportError(RecognitionException e) {
    throw new RuntimeException(e);
  }
}

@lexer::members {
  @Override
  public void reportError(RecognitionException e) {
    throw new RuntimeException(e);
  }
}

// Non-terminaux

prog	:	defClasse* instruction* EOF
		-> ^(PROGRAM defClasse* instruction*)
	;

		
instruction
	:	a=expression
		(	':' IDCLASS
			(	':=' b=expression
				-> ^(DEF_LVAR $a IDCLASS $b)
			|	-> ^(DEF_LVAR $a IDCLASS)
			)
		|	(	':=' b=expression
				-> ^(ASSIGNMENT $a $b)
			|	-> $a
			)
		)
		';'	/* Il faudra verifier semantiquement que expression correspond a une cible valide
			et que DEF_LVAR n'est utilise que dans un bloc */
	|	'return' ';' 
		-> ^(RETURN)
	|	'if' expression 'then' a=instruction 'else' b=instruction 
		-> ^(IF expression ^(THEN $a?) ^(ELSE $b?))
	|	'while' expression 'do' instruction
		-> ^(WHILE expression instruction)
	|	bloc
		-> ^(BLOCK bloc)?
	;
		
bloc	:	'{' 
		(	(a+=instruction)+
			(	'is' (b+=instruction)+
				-> ^(DEF_LVARS $a+) $b+
			|	-> $a+
			)
		|	->
		)
		'}'
	;
	
constante
	:	INT^
	|	STRING^
	;

instanciation
	:	'new' IDCLASS '(' args ')' 
		-> ^(INSTANTIATE IDCLASS args)
	;

identificateur
	:	'this' -> ^(THIS)
	|	'super' -> ^(SUPER)
	|	'result' -> ^(RESULT)
	|	ID^
	;

defClasse
	:	'class' IDCLASS '(' paramsClasse ')' ('extends' IDCLASS)? 'is' '{' defClasse2+ '}'
		-> ^(CLASSDEF IDCLASS paramsClasse ^(SUPERCLASS IDCLASS)? ^(BODY_CLASS defClasse2+))
	;

defClasse2
	:	defAttr^
	|	'def'! (defMethode^ | defConstructeur^)
	;

defConstructeur
	:	a=IDCLASS '(' paramsConstructor ')' (':' b=IDCLASS '(' args ')')? 'is' bloc
		-> ^(DEF_CONSTRUCTOR $a paramsConstructor ^(SUPERCLASS_INIT $b args)? ^(BODY_CONSTRUCTOR bloc?))
	;

defMethode
	:	methodeProps? ID '(' paramsMethode ')' ((':' IDCLASS (':=' expression | 'is' bloc)) | 'is' bloc)
		-> ^(DEF_METHOD ^(METHOD_PROPS methodeProps?) ID paramsMethode IDCLASS? ^(BODY_METHOD expression? ^(BLOCK bloc)?))
	;
	
methodeProps
	:	'static' -> STATIC
	|	'override' -> OVERRIDE
	;

defAttr	:	'var' attrProps? ID ':' IDCLASS (';' | ':=' expression ';')	// Variable membre
		-> ^(DEF_ATTRIBUTE ^(ATTR_PROPS attrProps?) ID IDCLASS expression?)
	;
	
attrProps
	:	'static' -> STATIC
	;

// ParametresI
paramsClasse
	:	(paramClasse (',' paramClasse)*)? 
		-> ^(PARAMS_CLASS paramClasse*)
	;

paramClasse
	:	'var' ID ':' IDCLASS
		-> ^(PARAM_CLASS VAR ID IDCLASS)
	|	ID ':' IDCLASS
		-> ^(PARAM_CLASS ID IDCLASS)
	;
	
paramsConstructor
	:	(paramConstructor (',' paramConstructor)*)? 
		-> ^(PARAMS_CONSTRUCTOR paramConstructor*)
	;

paramConstructor
	:	'var' ID ':' IDCLASS
		-> ^(PARAM_CONSTRUCTOR VAR ID IDCLASS)
	|	ID ':' IDCLASS
		-> ^(PARAM_CONSTRUCTOR ID IDCLASS)
	;

paramsMethode
	:	(paramMethode (',' paramMethode)*)? 
		-> ^(PARAMS_METHOD paramMethode*)
	;

paramMethode
	:	ID ':' IDCLASS 
		-> ^(PARAM_METHOD ID IDCLASS)
	;

args
	:	(expression (',' expression)*)?
		-> ^(ARGS expression*)
	;

// Expressions
expression
	:	instanciation
	|	exprOp
	;

exprOp	:	exprOr
	;
	
exprOr	:	exprXor (exprOrOp^ exprXor)*
	;

exprOrOp:	'or' -> OP_OR
	;
	
exprXor	:	exprAnd (exprXorOp^ exprAnd)*
	;

exprXorOp
	:	'xor' -> OP_XOR
	;

exprAnd	:	exprCompEg (exprAndOp^ exprCompEg)*
	;

exprAndOp
	:	'and' -> OP_AND
	;
	
exprCompEg
	:	exprComp (exprCompEgOp^ exprComp)*
	;

exprCompEgOp
	:	'=' -> OP_EQU
	|	'<>' -> OP_NEQ
	;
	
exprComp:	exprShift (exprCompOp^ exprShift)*
	;

exprCompOp
	:	'<' -> OP_LES
	|	'<=' -> OP_LEQ
	|	'>' -> OP_GRT
	|	'>=' -> OP_GEQ
	;
	
exprShift
	:	exprAdd (exprShiftOp^ exprAdd)*
	;

exprShiftOp
	:	'<<' -> OP_SHL
	|	'>>' -> OP_SHR
	;

exprAdd	:	exprMult (exprAddOp^ exprMult)*
	;

exprAddOp
	:	'+' -> OP_ADD
	|	'-' -> OP_SUB
	|	'&' -> OP_CAT
	;

exprMult:	exprMessage (exprMultOp^ exprMessage)*
	;

exprMultOp
	:	'*' -> OP_MUL
	|	'/' -> OP_DIV
	|	'%' -> OP_MOD
	;

exprMessage
	:	exprFin
		(	message
			-> ^(MESSAGE_BASE exprFin message)
		|	-> exprFin
		)
	|	IDCLASS message 
		-> ^(MESSAGE_BASE IDCLASS message)
	;

message	:	'.' ID
		(	'(' args ')' message?
			-> ^(MESSAGE_CALL ID args message?)
		|	message?
			-> ^(MESSAGE ID message?)
		)
	;

exprFin
	:	'(' exprCast ')'
		-> exprCast
	|	constante
	|	identificateur
	|	'+' (constante | identificateur) -> constante? identificateur?
	|	'-' (constante | identificateur) -> ^(OP_NEG constante? identificateur?)
	|	'~' (constante | identificateur) -> ^(OP_NOT constante? identificateur?)
	;

exprCast:	'as' IDCLASS ':' expression
		-> ^(CAST IDCLASS expression)
	|	expression
	;


// Terminaux

IDCLASS	:	('A'..'Z')('A'..'Z'|'a'..'z'|'0'..'9'|'_')*
	;

ID	:	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
	;

INT	:	('0'..'9')+
	;
	
COMMENT	:	'//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
	|	'/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
	;

WS	:	(' ' | '\t' | '\r' ~('\n')) {$channel=HIDDEN;}
	;

NEWLINE	:	'\r'? '\n' {$channel=HIDDEN;}
	;

STRING	:	'"' ( ~('"') )* '"'
   		{setText(getText().substring(1, getText().length()-1));}
	;
