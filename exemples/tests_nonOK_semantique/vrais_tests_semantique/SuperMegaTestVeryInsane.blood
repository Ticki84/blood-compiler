class ClasseModeleReutilisable() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ClasseModeleReutilisable() is {
         this.entier := 5;
    }

}


/*
Listes des erreurs de sémantique qui sont presentes dans le fichier :
- Une classe sans constructeur
- Une division par zero
- Heritage d'une classe qui n'existe pas
- This implicite
- Types d'operandes non compatibles
- Types incompatibles lors d'un assignement
- Mauvais type lors de l'utilisation d'une methode
- Mauvais nombre lors de l'utilisation d'une methode
- Utilisation d'une variable qui n'existe pas
- Utilisation d'une methode qui n'existe pas
- Utilisation d'une classe qui existe pas
- Mauvais type de retour d'une fonction
- Deux definitions de la meme methde
- Deux classes qui portent le meme nom
- Acces a une variable qui est pas censee etre visible
- Changement de signature lors d'un override
- Override d'une methode qui existe pas
- Cast impossible
- Deux declarations meme variable / attribut
- Conditionnelle mais pas avec un Integer
- Une classe avec deux constructeurs
- Identifiant inconnu lors d'une affectation
- Affectation avec types qui concordent pas
- Declaration d'une variable apres le is
- Override sans heritage
- Dependence circulaire
- Pas d'appel au constructeur de la SuperClasse
- Result qui n'est pas instancie
- Result qui a pas le bon type
- Declarer une variable dans un IF mais l'utiliser dans le ELSE
- Utilisation du SUPER mais en selectionnant un attribut qui n'existe pas
- Return mais avec une methode qui renvoie rien
- Profil constructeur et classe qui ne correspondent pas (nombre de parametres)
- Profil constructeur et classe qui ne correspondent pas (type des parametres)
- Manipulation de variable avant le IS
- Methode statique qui accede a des attributs non statiques
- Definition d'une classe qui s'appelle Integer
- Extends Integer
- Extends String
- Mettre un result dans le Main
- Acces à un attribut de classe depuis un contexte statique
*/


class ClasseSansConstructeur() is { // Erreur : cette classe n'a pas de constructeur

    var attributQuiSertPas : String;

    def getAttributQuiSertPas() : String := this.attributQuiSertPas

    def setAttributQuiSertPas(x : String) is {
        this.attributQuiSertPas := x;
    }

}


class DivisionParZero() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def DivisionParZero() is {
        this.entier := 5/0; // Erreur : on divise par zero
    }

}


class ClasseQuiEtendClasseInexistante() extends ClasseInexistante is { // Erreur : on etends une classe qui n'existe pas

    var attributQuiSertPas : String;

    def getAttributQuiSertPas() : String := this.attributQuiSertPas

    def setAttributQuiSertPas(x : String) is {
        this.attributQuiSertPas := x;
    }

    def ClasseQuiEtendClasseInexistante() is {
        this.attributQuiSertPas := "ne sert strictement a rien";
    }

}


class ImplicitThis() is {

    var attributQuiSertPas : String;

    def getAttributQuiSertPas() : String := this.attributQuiSertPas

    def setAttributQuiSertPas(x : String) is {
        this.attributQuiSertPas := x;
    }

    def ImplicitThis() is {
        attributQuiSertPas := "ne sert strictement a rien"; // Erreur : il y a un this implicite
    }

}


class WrongTypeOperand() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def WrongTypeOperand() is {
        this.entier := 5 + "Hello"; // Erreur : on additionne un Integer et un String
    }

}


class WrongTypeAssignment() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def WrongTypeAssignment() is {
        this.entier := "Hello"; // Erreur : on affecte un String a une variable qui attend un entier
    }

}


class WrongTypeUsingMethod() is { // L'erreur sera dans le main

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def WrongTypeUsingMethod() is {
         this.entier := 5;
    }

}


class WrongNumberParametersMethod() is { // L'erreur sera dans le main

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def WrongNumberParametersMethod() is {
         this.entier := 5;
    }

}


class UseOfUndefinedVariable() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def UseOfUndefinedVariable() is {
        this.entier := variableQuiExistePas; // Erreur : on utilise une variable qui n'existe pas
    }

}


class UseOfUndefinedMethod() is { // L'erreur sera dans le main

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def UseOfUndefinedMethod() is {
        this.entier := 12;
    }

}


class TypeOfVariableInexistant() is {

    var variableBidon : ClasseQuiExistePas; // Erreur : la classe ClasseQuiExistePas est inconnue

    def TypeOfVariableInexistant() is {}

}


class TypeMatchingErrorMethod() is {

    var entier : Integer;
    var chaineDeCaracteres : String;

    def getEntier() : Integer := this.entier
    def getChaineDeCaracteres() : String := this.entier // Erreur : la methode doit retourner un String mais elle retourne un Integer

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def setChaineDeCaracteres(s : String) is {
        this.chaineDeCaracteres;
    }

    def TypeMatchingErrorMethod() is {
        this.entier := 12;
        this.chaineDeCaracteres := "Coucou";
    }

}


class TwoDefinitionsOfTheSameMethod() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier
    def getEntier() : Integer := this.entier + 22 // Erreur : on defini deux fois la meme methode

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def TwoDefinitionsOfTheSameMethod() is {
        this.entier := 12;
    }

}


class TwoDefinitionsOfTheSameClass() is {
    def TwoDefinitionsOfTheSameClass() is {}
}
class TwoDefinitionsOfTheSameClass() is {
    def TwoDefinitionsOfTheSameClass() is {}
}
/* Erreur : deux classes portent le meme nom */


class ProblemOfVisibilityVariable() is { // L'erreur sera dans le main

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ProblemOfVisibilityVariable() is {
        this.entier := 12;
    }

}


class OverrideWithDifferentSignature() extends ClasseModeleReutilisable is {

    var entier : Integer;

    def override getEntier(x : Integer) : Integer := this.entier + x // Erreur : on override une methode mais on change la signature

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def OverrideWithDifferentSignature() : ClasseModeleReutilisable() is {}

}


class OverrideInexistantMethod() extends ClasseModeleReutilisable is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def override methodeInexistante() : Integer := this.entier + 78 // Erreur : on override une methode qui n'existe pas

    def OverrideInexistantMethod() : ClasseModeleReutilisable() is {}

}


class ImpossibleCast() is { // L'erreur sera dans le main

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ImpossibleCast() is {
         this.entier := 5;
    }

}


class TwoDeclarationsOfTheSameVariable() is {

    var entier : Integer;
    var entier : String; // Erreur : on a deux variables - attributs qui portent le meme nom

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def TwoDeclarationsOfTheSameVariable() is {
         this.entier := 5;
    }

}


class ConditionalButNoInteger() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ConditionalButNoInteger() is {
         if "Je suis un String mec !" then // Erreur : on fait une conditionnelle mais pas avec un Integer
              this.entier := 5;
         else
              this.entier := 10;
    }

}


class ClassWithTwoConstructor() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ClassWithTwoConstructor() is {
        this.entier := 1;
    }

    def ClassWithTwoConstructor() is {
        this.entier := 2;
    }

}


class OverrideWithoutExtends() is {

    var entier : Integer;

    def override getEntier() : Integer := this.entier + 22 // Erreur : on fait un override sans heritage

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def OverrideWithoutExtends() is {
         this.entier := 5;
    }

}


class CircularDepedencyA() extends CircularDepedencyB is {
    def CircularDepedencyA() : CircularDepedencyB() is {}
}
class CircularDepedencyB() extends CircularDepedencyA is {
    def CircularDepedencyB() : CircularDepedencyA() is {}
}
/* Erreur : dependence circulaire */


class MissingConstructorSuperClass() extends ClasseModeleReutilisable is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def MissingConstructorSuperClass() is { // Erreur : pas d'appel au constructeur de la SuperClasse
         this.entier := 22;
    }

}


class ResultNotInstanced() is {

    var entier : Integer;
    var clone : ResultNotInstanced;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ResultNotInstanced() is {
         this.entier := 5;
    }

    def clone() : ResultNotInstanced is {
        this.clone := new ResultNotInstanced();
        this.clone.setEntier(22);
        /* Erreur : le result est pas instancie */
    }

}


class ResultWrongInstanced() is {

    var entier : Integer;
    var clone : ResultWrongInstanced;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def ResultWrongInstanced() is {
         this.entier := 5;
    }

    def clone() : ResultWrongInstanced is {
        this.clone := new ResultWrongInstanced();
        this.clone.setEntier(22);
        result := 58; // Erreur : le result n'a pas le bon type
    }

}


class VariableNotSupposedToExist() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def VariableNotSupposedToExist() is {
         if 1 then{
              x : Integer := 55;
              is
              this.entier := x;
         }
         else
              this.entier := x; // Erreur : on a declare x dans le IF, il est pas cense etre visible dans le ELSE
    }

}


class SuperInexistantAttribute() extends ClasseModeleReutilisable is {

    var texte : String;

    def getTexte() : String := this.texte

    def setTexte(x : String) is {
        this.texte := x;
    }

    def donneMoiPrenom() : Integer := super.prenom // Erreur, dans la super-classe, aucun attribut ne s'appelle prenom

    def SuperInexistantAttribute() : ClasseModeleReutilisable() is {
        this.texte := "Hola";
    }

}


class ResultVoidReturn() is {

    var entier : Integer;

    def getEntier() : Integer := this.entier

    def setEntier(x : Integer) is {
        this.entier := x;
    }

    def jeNeRenvoieRien() is {
        x : Integer := 22;
        is
        result := x; // Erreur, la methode n'est pas censee renvoyer qqch, or on a return
    }

    def ResultVoidReturn() is {
         this.entier := 5;
    }

}


class NonMatchingConstructorAndClassProfile(var humeur : Integer) is {

    var nom :String;
    var prenom :String;
    var age: Integer;

    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : Integer := this.age
    def getHumeur() : Integer := this.humeur

    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    def NonMatchingConstructorAndClassProfile() is { // Erreur : Le constructeur n'a pas le meme profil que la classe (nombre d'arguments)
        this.nom := "Jean";
        this.prenom := "Michel";
        this.age := 34;
    }

}


class NonMatchingConstructorAndClassProfileBis(var humeur : Integer) is {

    var nom :String;
    var prenom :String;
    var age: Integer;

    def getNom() : String := this.nom
    def getPrenom() : String := this.prenom
    def getAge() : Integer := this.age
    def getHumeur() : Integer := this.humeur

    def setNom(name : String) is {
        this.nom := name;
    }

    def setPrenom(firstName : String) is {
        this.prenom := firstName;
    }

    def setAge(n : Integer) is {
        this.age := n;
    }

    def NonMatchingConstructorAndClassProfileBis(var humeur : String) is { // Erreur : Le constructeur n'a pas le meme profil que la classe (type des arguments)
        this.nom := "Jean";
        this.prenom := "Michel";
        this.age := 34;
    }

}


class StaticMethodTryToAccesNotStaticAttribute() is {

    var static attributStatique : Integer;
    var attributPasStatique : Integer;

    def static methodeStatique() : Integer := this.attributPasStatique // Erreur : la methode est statique mais elle accede a un attribut non statique

    def StaticMethodTryToAccesNotStaticAttribute() is {
        this.attributPasStatique := this.attributPasStatique + 1;
        StaticMethodTryToAccesNotStaticAttribute.attributPasStatique := StaticMethodTryToAccesNotStaticAttribute.attributPasStatique + 1;
    }

}


class Integer() is { // Erreur : Definition de la classe Integer, c'est interdit
    def Integer() is {

    }
}


class ExtendsInteger() extends Integer is { // Erreur : On etends la classe Integer

    def ExtendsInteger() : Integer() is {}

}

class ExtendsString() extends String is { // Erreur : On etends la classe String

    def ExtendsString() : String() is {}

}


{

    wrongTypeUsingMethod : WrongTypeUsingMethod := new WrongTypeUsingMethod();
    wrongNumberParametersMethod : WrongNumberParametersMethod := new WrongNumberParametersMethod();
    useOfUndefinedMethod : UseOfUndefinedMethod := new UseOfUndefinedMethod();
    problemOfVisibilityVariable : ProblemOfVisibilityVariable := new ProblemOfVisibilityVariable();
    impossibleCast : ImpossibleCast := new ImpossibleCast();
    x : Integer := (as ClasseModeleReutilisable : impossibleCast).getEntier(); // Erreur : on essaye de caster mais c'est impossible

    wrongNumberParametersMethod.setEntier(1); // Erreur : On manipule une variable avant le IS

    is

    wrongTypeUsingMethod.setEntier("Je suis un string mec !"); // Erreur : la methode doit prendre un Integer en entree, pas un String
    wrongNumberParametersMethod.setEntier(1, 2); // Erreur : la methode attend un argument, pas deux
    useOfUndefinedMethod.methodeQuiExistePas(); // Erreur : on tente d'executer une methode qui n'existe pas
    problemOfVisibilityVariable.entier := 54; // Erreur : on est pas censes pouvoir acceder a cet attribut sans passer par le getter
    ProblemOfVisibilityVariable.entier.toString().println(); // Erreur : on essaie d'acceder statiquement a un attribut
    identifiantInconnu := problemOfVisibilityVariable.getEntier(); // Erreur : identifiant inconnu lors d'une affectation
    wrongNumberParametersMethod := wrongTypeUsingMethod; // Erreur : affectation mais types qui concordent pas

    jeDeclareUneVariableApresLeIs : ClasseModeleReutilisable := new ClasseModeleReutilisable(); // Erreur : declaration d'une variable apres le is

    result := 42;
}
