# PCL «Projet de Compilation des Langages»

## Sommaire
- [Bref résumé du projet](#Bref-résumé-du-projet)
- [Membres du groupe](#Membres-du-groupe)
- [Récupérer le projet](#Récupérer-le-projet)
- [Exécution du projet avec Gradle](#Exécution-du-projet-avec-Gradle)
- [Composition du répertoire](#Composition-du-répertoire)


## Bref résumé du projet

> "Ce projet a été créé par l'Université de Paris-Saclay. Son objectif est d'écrire un compilateur d'un mini langage orienté objet. Le compilateur produit crée, en sortie, du code assembleur microPIUP/ASM à partir d'un code en langage Blood."

Vous trouverez la description détailée du projet dans le fichier [sujet-Projet-2020-21.pdf](./sujet-Projet-2020-21.pdf).


## Membres du groupe

- Dumitru Bulgaru <<dumitru.bulgaru@telecomnancy.eu>>
- Florent Caspar <<florent.caspar@telecomnancy.eu>>
- Yann Colomb <<yann.colomb@telecomnancy.eu>>
- Maxime Lucas <<maxime.lucas@telecomnancy.eu>>
- Benjamin Sepe <<benjamin.sepe@telecomnancy.eu>>


## Récupérer le projet

Pour récupérer le projet, il vous suffit de faire :
- Par HTTPS :
```bash
$ git clone https://gitlab.telecomnancy.univ-lorraine.fr/Yann.Colomb/pcl-colomb3u.git
```
- Par SSH :
```bash
$ git clone git@gitlab.telecomnancy.univ-lorraine.fr:Yann.Colomb/pcl-colomb3u.git
```

## Exécution du projet avec Gradle

- Exécution :
```bash
$ ./gradlew run --args='<arguments> fichier'
```

- Compilation en un binaire distribuable :
```bash
$ ./gradlew build
```
Le binaire compilé peut alors être récupéré dans le répertoire `./build/distributions`.

- Exécution des tests :
```bash
$ ./gradlew test
```

### Utilisation

```
usage: BloodCompiler [-h] [-d] [-s] [-a] file

Compile a Blood file into microPIUP assembly.

positional arguments:
  file                   Blood source file

named arguments:
  -h, --help             show this help message and exit
  -d, --debug            Enables the display of debug messages
  -s, --symbolTables     Enables the display of the symbol tables
  -a, --ast              Enables printing the text AST
```

## Composition du répertoire

### Grammaire

Le dossier [grammaire](./grammaire) contient la grammaire du langage Blood écrite pour le framework [ANTLR](https://www.antlr.org/).


### Tests

Le dossier [exemples](./exemples) contient des fichiers de code Blood permettant de tester la grammaire créée auparavant.	

### ANTLR

Le dossier [jar](./jar) contient les fichiers jar pour l'utilisation des frameworks Antlr et AntlrWorks.
Pour exécuter ces fichiers, il faut faire :
```bash
$ java -jar antlr-3.5.2-complete.jar
```
et
```bash
$ java -jar antlrworks-1.5.1.jar
```

### Code source

Le dossier [src](./src) contient tout le code source Java du projet.
